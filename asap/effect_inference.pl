% ---- effect_inference.pl - Safe Parallelism Inference -*- Prolog -*----%
%                                                                        %
% This file is distributed under the University of Illinois Open Source  %
% License. See LICENSE.TXT for details.                                  %
%                                                                        %
% ===----------------------------------------------------------------=== %
%                                                                        %
% This module contains code performing effect inference on partially     %
% instantiated effect inclusion constraints for the ASaP annotation      %
% inference.                                                             %
%                                                                        %
% ===----------------------------------------------------------------=== %

:-module(effect_inference, [esi_collect/4,
                            infer_es_vars/2,
                            headof_CG_cycle/1,
                            finalize_partial_es_all/0
                           ]).

:-use_module(util, [
                    has_value/2,
                    has_effect_summary/2,
                    partially_inferred_es/2,
                    partially_inferred_es2/2
                   ]).

:-use_module(rpl, [apply_sub/3,
                   has_expanding_sub/4,
                   substitute_rpl_vars/2
                  ]).

:-use_module(effect, [get_effect_summary/2,
                      make_effect_summary/2,
                      is_resolved/1,
                      assertz_es/3,
                      assertz_pi_es/2,
                      assertz_pi_es2/2
                     ]).

:- dynamic headof_CG_cycle/1.

%% esi_collect(+EV:effect_variable, +Name:method_name, +ESin:effect_l , -ESout:effect_summary)
% Traverses the call-graph and computes partially infered summaries (without applying
% substitutions or simplifying expanding cycles). This pass is done without having to
% instantiate RPL variables, and the results are stored in the partially_inferred_es
% predicate
% @param ESin are the effects of the statements of the body of the method
% which may include invokes effects.
% @param ESout is the inferred effect summary.
esi_collect(_EV, _Name, [], ES) :- !, make_efffect_summary([], ES).
esi_collect(EV, Name, LHS, ES) :-
    make_effect_summary(LHS, ESin),
    esi_collect1(EV, [Name], ESin, ES),
    !,
    assertz_pi_es(EV, ES).

infer_es_vars([H|T], EVL) :-
    infer_es_var(H, EVLH),
    infer_es_vars(T, EVLT),
    ord_union(EVLH, EVLT, EVL).
infer_es_vars([], []).

infer_es_var(EV, []) :-
    has_value(EV, _ES), !.
infer_es_var(EV, EVL) :-
    resolve_es(EV, _, EVL).

%% resolve_es(+EVar:effect_var, -ES:effect_summary, -EVL:effect_var_ord_set) :-
resolve_es(EVar, ES, EVL) :-
    has_effect_summary(FunName, effect_var(EVar)), % get the name of the function from the effect var
    partially_inferred_es(EVar, ESp),
    substitute_rpl_vars(ESp, ES1),
    esi_resolve(EVar, [FunName], ES1, ES2, EVL),
    !,
    ES2 = effect_summary(_SE2, []),
    ES = ES2.


%% esi_resolve(+EV:effect_var, +Names:string_list, +ESin:effect_summary,
%%             -ESout:effect_summary, -EVL:effect_var_ord_set)
%  Resolve the effect summary of the function which may include
%  recursion (in this function or one of the callees).
%  This is the second phase of the infer_es/2 predicate.
%  Its "output" is to populate the has_value/2 predicate.
%  @param EVL constains the list of effect_vars that were instantiated
esi_resolve(EV, _Names, ES, ESSorted, [EV]) :-
    is_resolved(ES), !,
    assertz_es(EV, ES, ESSorted).
esi_resolve(EV, Names, ESin, ESoutSorted, EVL) :-
    Names = [Self|_Others],
    ESin = effect_summary(SE, CE),
    ( headof_CG_cycle(Self)
      ->
      ( % Here we will resolve the substitutions along the CallGraph cycle
        % and update the (partially) inferred effect summary for 'Self' before
        % proceeding to esi_resolve_children, since some of the children may
        % call (possibly indirectly) 'Self', if the CallGraph cycle has length
        % greater than one.
        summarize_rec_subs(Self, CE, CErest, RecL),
        % apply each RecL independently to CEout and merge with effect_summary(SE,CEout)
        RecSubS = subst_set(RecL),
        apply_rec_sub(SE, [RecSubS], SE2),
        apply_rec_sub(CErest, [RecSubS], CE2),
        % update database
        assertz_es(EV, effect_summary(SE2, CE2), _ESSorted)
      )
      ; ( CE2 = CE, SE2 = SE )
    ),
    % resolve childen
    esi_resolve_children(Names, CE2, EVLch),
    % Once children are resolved, resolve remaining invocations.
    esi_resolve_self(Self, SE2, CE2, ESoutRandom),
    % update database
    assertz_es(EV, ESoutRandom, ESoutSorted),
    ord_union([EV], EVLch, EVL).

%% esi_resolve_self(+Self:string, +SE:effect_list, +CE:effect_list, -ES:effect_summary)
%  Fetch the effects of the now resolved invocations from CE, merge them
%  with the effects in SE and return them in ES.
% @param SE simple effects
% @param CE complex effects
esi_resolve_self(_Self, SE, [], effect_summary(SE, [])) :- !.
esi_resolve_self(Self, SE, [invokes(Callee, Subs)|T], ES) :-
    get_effect_summary(Callee, ESc),
    apply_sub(ESc, Subs, ESc2),
    ESc2 = effect_summary(SEc2, CEc2),
    add_to_el(SE, SEc2, SEnew),
    ( (Self = Callee ; headof_CG_cycle(Callee))
      -> CEnew = T
      ;  add_to_el(T, CEc2, CEnew)
    ),
    esi_resolve_self(Self, SEnew, CEnew, ES).

%esi_resolve_self(Self, SE, [invokes(Callee, Subs)|T], ES) :-
%    get_effect_summary(Callee, ESc),
%    apply_sub(ESc, Subs, ESc2),
%    ESc2 = effect_summary(SEc2, CEc2),
%    add_to_el(SE, SEc2, SEnew),
%    ( Self = Callee
%      -> CEnew = T
%      ;  add_to_el(T, CEc2, CEnew)
%    ),
%    esi_resolve_self(Self, SEnew, CEnew, ES).

%% esi_resolve_children(+Names:string_list, +CEL:compound_effect_list, -EVL:effect_var_ord_set)
%  @param EVL constains the list of effect_vars that were instantiated
esi_resolve_children(_Names, [], []) :- !. % nop
esi_resolve_children(Names, [invokes(Callee,_Subs)|T], EVL) :-
    % resolve 1st child
    has_effect_summary(Callee, effect_var(EVC)),
    ((memberchk(Callee, Names),! ;  has_value(EVC, _))
    -> (% skip CallGraph-loops and already resolved functions
        EVLH = [])
    ;  (%get_effect_summary(Callee, ES), % if resolved, get the ES, otherwise get the partially inferred ES
        partially_inferred_es(EVC, ES),
        substitute_rpl_vars(ES, ESsub),
        esi_resolve(EVC, [Callee|Names], ESsub, _ES2, EVLH) )
    ),
    esi_resolve_children(Names, T, EVLT), % resolve rest of children ("tail" recursion)
    ord_union(EVLH, EVLT, EVL).


%% select_CG_loops(+Self:string, +CEin:effect_l, -CESelf:effect_l, -CERest:effect_l, -SVS:set_of_substitution_vectors)
%  @param CESelf will contain the invokes effects that *are* recursive
%  @param CERest will contain the invokes effects that are *not* recursive
%  @param SVS will contain a set of substitution vectors built from the self-loop edges in the reduced call-graph
select_CG_loops(Self, CEin, [invokes(Self, Subs)|CESelf], CERest, [Subs|SVSrest]) :-
    select(invokes(Self, Subs), CEin, CEinRest),
    !,
    select_CG_loops(Self, CEinRest, CESelf, CERest, SVSrest).
select_CG_loops(Self, CE, [], CE, []) :-
    \+ memberchk(invokes(Self, _Subs), CE).

%% summarize_rec_subs(+Self, +CEin, -CEout, -RecL)
summarize_rec_subs(Self, CEin, CEout, RecL) :-
    select_CG_loops(Self, CEin, _CESelf, _CERest, SVS),
    collect_params_svs(SVS, Pars),
    find_params_w_expanding_substitution_cycles(SVS, Pars, CyclePars),
    CEout = CEin,
    mk_simplified_subs(CyclePars, RecL),
    write('summarize_rec_subs returned RecL='), write(RecL), write(' for call to '), write(Self), nl,
    true.

%% mk_simplified_subs(+CycleParams:parameter_l, -Subs:substitution_l).
mk_simplified_subs([], []).
mk_simplified_subs([H|T], [Ho|To]) :-
    Ho = param_sub(H, rpl([H,rSTAR], [])),
    mk_simplified_subs(T, To).

%% collect_params_svs(+SVS:set_of_substitution_set_l, -Params:param_ord_set).
collect_params_svs([], []) :- !.
collect_params_svs([H|T], Prms) :-
    collect_params_sv(H, Hprm),
    collect_params_svs(T, Tprm),
    ord_union(Hprm, Tprm, Prms).

%% collect_params_sv(+SV:substitution_set_l, -Params:param_ord_set).
collect_params_sv([], []).
collect_params_sv([H|_T], Pord) :-
    H = subst_set(S),
    collect_params(S, Prms),
    list_to_ord_set(Prms, Pord).
    % ignore the params of the non-head vector elements _T

%% collect_params(+SubS:substitution_l, -ParamL:parameter_l)
% collects the rgn params at the head of substitutions in input list
collect_params([H|T], [P|Tp]) :-
    H = param_sub(P, _Rpl), % ignore the params in subs of the RPL
    rgn_param(P),
    collect_params(T, Tp). % no merge needed: substitution sets should contain substitutions with disjoint (head) params
collect_params([], []).

%% find_params_w_expanding_substitution_cycles(+SVS:set_of_substitution_set_l, +ParsIn:params_l, -ParsCycle:params_ord_set)
%  @param ParsCycle returns the parameters that contribute to an expanding substitution
find_params_w_expanding_substitution_cycles(_SVS, [], []) :- !.
find_params_w_expanding_substitution_cycles(SVS, [Hp|Tp], ParsCycle) :-
    extract_expanding_cycle(SVS, Hp, _Cycle, Pstars)
    -> ( % found one or more expanding cycles starting from Hp
         findall(P ,(memberchk(P, Tp), \+memberchk(P, Pstars)) , ParsRest),
         find_params_w_expanding_substitution_cycles(SVS, ParsRest, PC),
         list_to_ord_set(Pstars, PstarsOrd),
         ord_union(PstarsOrd, PC, ParsCycle)
       )
    ;  ( find_params_w_expanding_substitution_cycles(SVS, Tp, ParsCycle)).


%% extract_expanding_cycle(+SVS:set_of_substitution_set_l, +P:param, -Cycle:substitution_l, -Params:param_l
extract_expanding_cycle(SVS, P, Cycle, [P|ParamsOut]) :-
    select(Chain, SVS, SVSrest),
    extract_chain_(Chain, [], SVSrest, P, P, Cycle, ParamsOut),
    has_expanding_sub(Cycle, _, _, _), !.

%% extract_chain_(+ChainIn, +ChainRest, +SVSrest, +Ph, +Pt, -CycleOut, -ParamsOut) non-deterministic
extract_chain_([H|[]], ChainRest, OtherChains, Ph, Pt, SubsOut, ParamsOut) :-
    !,
    H = subst_set(S),
    Sub = param_sub(Ph, rpl([P2|Ts], Subs)),
    select(Sub, S, SminusS), !,
    Sub2= param_sub(Ph, rpl([P2|Ts], [])),
    get_chain_last_param(Subs, P2, Pz, SubsZ),
    append(ChainRest, [subst_set(SminusS)], ChainNext),
    ( Pz = Pt
    -> ( % found chain
         SubsOut = [Sub2|SubsZ],
         ParamsOut = []
       )
    ;  ( % didn't find chain
         select(Chain, [ChainNext|OtherChains], OCRest),
         extract_chain_(Chain, [], OCRest, Pz, Pt, SubsOut2, ParamsOut2),
         append(SubsZ, SubsOut2, SubsOut3),
         SubsOut = [Sub2|SubsOut3],
         ParamsOut = [P2|ParamsOut2]
       )
    ).

extract_chain_([H|T], ChainRest, OtherChains, Ph, Pt, SubsOut, ParamsOut) :-
    (H = subst_set(S),
    Sub = param_sub(Ph, rpl([P2|Ts], Subs)),
    select(Sub, S, SminusS))
    -> (% Found substitution with required parameter -> proceed
        Sub2= param_sub(Ph, rpl([P2|Ts], [])),
        get_chain_last_param(Subs, P2, Pz, SubsZ),
        append(ChainRest, [subst_set(SminusS)], ChainNext),
        extract_chain_(T, ChainNext, OtherChains, Pz, Pt, SubsOut2, ParamsOut),
        append(SubsZ, SubsOut2, SubsOut3),
        SubsOut = [Sub2|SubsOut3])
    ; (% Didn't find substitution with required parameter -> skip this substitution-set
        append(ChainRest, [H], ChainNext),
        extract_chain_(T, ChainNext, OtherChains, Ph, Pt, SubsOut, ParamsOut)
    ).

%% get_chain_last_param(+Subs:substitution_set_l, +Pin:rgn_param, -Pout:rgn_param, -SubsOut:substitution_l)
get_chain_last_param([], Ph, Ph, []) :- !.

get_chain_last_param([H|T], Ph, Pout, SubsOut) :-
    H = subst_set(S),
    Sub = param_sub(Ph, rpl([P2|Ts], Subs)),
    memberchk(Sub, S),
    Sub2= param_sub(Ph, rpl([P2|Ts], [])),
    get_chain_last_param(Subs, P2, Pz, SubsZ),
    get_chain_last_param(T, Pz, Pout, SubsZ2),
    append(SubsZ, SubsZ2, SubsZ3),
    SubsOut = [Sub2|SubsZ3].
get_chain_last_param(_Subs, Ph, Ph, []). % no substitutions found


%% esi_collect1(+EV:effect_var,+Names:string_list,
%%              +ESin:effect_summary,-ESout:effect_summary)
%  Firts pass of effect inference: traverses the call-graph in
%  depth-first order breaking recursive cycles and "pulls-up"
%  all effects encountered along that spanning-tree.
%  Also, asserts partially resolved effect summaries which
%  later queried and updated by subsequent passes of the inference.
esi_collect1(_EV, _Names, effect_summary(SE,[]), effect_summary(SE,[])) :- !.
esi_collect1(EV, Names, effect_summary(SE,CE), ES) :-
    SE \= [],
    !,
    esi_collect1(EV, Names, effect_summary([],CE), ESc),
    add_to_es(effect_summary(SE,[]), ESc, ES).
esi_collect1(EV, Names, effect_summary([], [H|T]), ES) :-
    H = invokes(Callee, Sub),
    \+ memberchk(Callee, Names),
    !,
    ( headof_CG_cycle(Callee)
    -> ESC = effect_summary([], [H])
    ; ( get_effect_summary(Callee, ESCtry)
      ->(
          apply_sub(ESCtry, Sub, ESC)
        )
      ; ( has_effect_summary(Callee, effect_var(EVC)),
          esi_constraint(_, LHS, effect_var(EVC)),
          make_effect_summary(LHS, ESCcons),
          esi_collect1(EVC, [Callee|Names], ESCcons, ESCinf),
          assertz_pi_es(EVC, ESCinf), % this might be wrong to assert here...
          ( headof_CG_cycle(Callee)
          -> ESC = effect_summary([], [H])
          ; apply_sub(ESCinf, Sub, ESC)
          )
        )
      )
    ),
    esi_collect1(EV, Names, effect_summary([], T), ES_T),
    add_to_es(ESC, ES_T, ES).

esi_collect1(EV, Names, effect_summary([],[H|T]), ES) :-
    H = invokes(Callee, _Sub),
    memberchk(Callee, Names),
    !,
    (headof_CG_cycle(Callee),! ; assertz(headof_CG_cycle(Callee)), write('HeadOfCG_Path:'), write(Names), write(', Asserting headof for '), write(Callee), nl),
    esi_collect1(EV, Names, effect_summary([], T), ES_T),
    add_to_es(effect_summary([], [H]), ES_T, ES).

contains_headof_CG_cycle([H|_T]) :-
    headof_CG_cycle(H), !.
contains_headof_CG_cycle([_H|T]) :-
    contains_headof_CG_cycle(T).


finalize_partial_es_all :-
    findall(C, headof_CG_cycle(C), HeadsL),
    finalize_partial_es_l(HeadsL),
    update_pi_es_l(HeadsL).

finalize_partial_es_l([H|T]) :-
    finalize_partial_es(H),
    finalize_partial_es_l(T).
finalize_partial_es_l([]).

finalize_partial_es(H) :-
    has_effect_summary(H, effect_var(EVar)), % get the name of effect var from the function
    partially_inferred_es(EVar, PES),
    finalize_partial_es_(H, PES, PESfin),
    assertz_pi_es2(EVar, PESfin).

%% finalize_partial_es_(+Self:decl_id, +ES:effect_summary, -ES:effect_summary)
finalize_partial_es_(Self, effect_summary(SEin, [H|T]), ESout) :-
    % 1. finalize H
    H = invokes(Callee, Subs),
    ( (headof_CG_cycle(Callee) , Self \= Callee)
    -> (get_effect_summary(Callee, ESH),
        filter_self_invokes(Self, ESH, ESHself),
        apply_sub(ESHself, Subs, ESHsub),
        add_to_es(effect_summary(SEin, [H]), ESHsub, ESoutH))
    ;  (ESoutH = effect_summary(SEin, [H]))
                    % called on those functions X that are headof_CG_cycle(X)
    ),
    % 2. finalize T
    finalize_partial_es_(Self, effect_summary([], T), ESoutT),
    % 3. combine
    add_to_es(ESoutH, ESoutT, ESout).

%finalize_partial_es_(Self, effect_summary(SEin, [H|T]), ESout) :-
%    % 1. finalize H
%    H = invokes(Callee, Subs),
%    ( Self = Callee % headof_CG_cycle(Self) is true because finalize is only
%                    % called on those functions X that are headof_CG_cycle(X)
%    -> (ESoutH = effect_summary(SEin, [H]))
%    ;  (get_effect_summary(Callee, ESH),
%        filter_self_invokes(Self, ESH, ESHself),
%        apply_sub(ESHself, Subs, ESHsub),
%        add_to_es(effect_summary(SEin, [H]), ESHsub, ESoutH))
%    ),
%    % 2. finalize T
%    finalize_partial_es_(Self, effect_summary([], T), ESoutT),
%    % 3. combine
%    add_to_es(ESoutH, ESoutT, ESout).

finalize_partial_es_(_Self, effect_summary(SEin, []), effect_summary(SEin, [])).


%%filter_self_invokes(+Self:decl_id, +ESin:effect_summary, -ESout:effect_summary)
% Takes an effect summary ESin and returns an effect summary ESout with only the invokations to Self.
filter_self_invokes(Self, effect_summary(_SE, [invokes(Self, Sub)|T]), effect_summary([], [invokes(Self, Sub)| Tout])) :- !,
    filter_self_invokes(Self, effect_summary([], T), effect_summary([], Tout)).
filter_self_invokes(Self, effect_summary(_SE, [_H|T]), ESTout) :- !,
    filter_self_invokes(Self, effect_summary([], T), ESTout).
filter_self_invokes(_Self, effect_summary(_SE, []), effect_summary([], [])).

update_pi_es_l([H|T]) :-
    update_pi_es(H),
    update_pi_es_l(T).
update_pi_es_l([]).

update_pi_es(H) :-
    has_effect_summary(H, effect_var(EVar)), % get the name of effect var from the function
    partially_inferred_es2(EVar, PES),
    retract(partially_inferred_es2(EVar, PES)),
    assertz_pi_es(EVar, PES).


