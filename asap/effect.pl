% ---- effect.pl - Safe Parallelism Inference ----*- Prolog -*-----------%
%                                                                        %
% This file is distributed under the University of Illinois Open Source  %
% License. See LICENSE.TXT for details.                                  %
%                                                                        %
% ===----------------------------------------------------------------=== %
%                                                                        %
% This module contains code for manipulating effects and effect          %
% summaries for the ASaP annotation inference.                           %
%                                                                        %
% ===----------------------------------------------------------------=== %

:-module(effect, [get_subeffect_kind_rpls/4,
                  is_non_interfering_effect_kind/2,
                  get_effect_rpl/2,
                  is_non_interfering/2,
                  covers/2,
                  is_resolved/1,
                  get_effect_summary/2,
                  make_effect_summary/2,
                  compute_el/2,
                  compute_es/2,
                  unroll_es/2,
                  gather_es_vars/3,
                  merge_es/3,
                  add_to_es/3,
                  merge_el/3,
                  add_to_el/3,
                  assertz_es/3,
                  assertz_pi_es/2,
                  assertz_pi_es2/2
                 ]).

:-use_module(rpl, [is_disjoint/2
                  ]).

:-use_module(util, [has_value/2, partially_inferred_es/2, partially_inferred_es2/2]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       EFFECTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pure.
reads(rpl(_Rpl,_Sub)).
writes(rpl(_Rpl,_Sub)).
invokes(_Method,_Sub).

%% simple_effect(?E:simple_effect)
%  True iff E is a simple effect
simple_effect(pure).
simple_effect(reads(_Rpl)).
simple_effect(writes(_Rpl)).

%% compound_effect(?E:compound_effect)
%  True iff E is a compound effect
compound_effect(invokes(_Method, _Sub)).

%%  effect(+E:effect) is det
%
%   True iff @param E is an effect.
effect(E) :- simple_effect(E),!.
effect(E) :- compound_effect(E).

%% get_subeffect_kind_rpls(+E1:effect, +E2:effect, -Rpl1:rpl, -Rpl2:rpl)
%
% returns the RPLs of input effects if the effect kind of E1 is a subeffect
% kind of that of E2.
get_subeffect_kind_rpls(writes(Rpl1), writes(Rpl2), Rpl1, Rpl2) :- !.
get_subeffect_kind_rpls(reads(Rpl1), writes(Rpl2), Rpl1, Rpl2) :- !.
get_subeffect_kind_rpls(reads(Rpl1), reads(Rpl2), Rpl1, Rpl2) :- !.

is_non_interfering_effect_kind(pure, _E) :- !.
is_non_interfering_effect_kind(_E, pure) :- !.
is_non_interfering_effect_kind(reads(_Rpl1), reads(_Rpl2)) :- !.

%% get_effect_rpl(+E:effect, -R:rpl)
% Return the rpl of an effect. Succeeds only for effects that have
% an Rpl argument; currently reads and writes.
get_effect_rpl(reads(Rpl), Rpl) :- !.
get_effect_rpl(writes(Rpl), Rpl) :- !.

%% is_non_interfering(+E1:effect, E2:effect) is det
is_non_interfering(pure, _EL) :- !. % short-cut/optimization
is_non_interfering(_EL, pure) :- !. % short-cut/optimization
is_non_interfering(_E, []) :- !.
is_non_interfering([], _E) :- !.

is_non_interfering(E, effect_summary(SE, CE)) :- !,
    is_non_interfering(E, SE),
    is_non_interfering(E, CE).

is_non_interfering(effect_summary(SE, CE), E) :- !,
    is_non_interfering(E, SE),
    is_non_interfering(E, CE).

is_non_interfering(E, [H|T]) :- !, % this cut is to prevent backtracking across overloaded predicates
    is_non_interfering(E, H),
    is_non_interfering(E, T).

is_non_interfering([H|T], E) :- !,
    is_non_interfering(H, E),
    is_non_interfering(T, E).

is_non_interfering(E1, E2) :-
    is_non_interfering_effect_kind(E1, E2), !.

is_non_interfering(E1, E2) :-
    get_effect_rpl(E1, R1),
    get_effect_rpl(E2, R2), !,
    is_disjoint(R1, R2), !.

%% covers(+EL1:effect_l, +EL2:effect_l) is det
%  True iff EL1 covers EL2
covers(_EL, []) :- !.
covers(EL, [H|T]) :- !,
    covers(EL, H),
    covers(EL, T).

%  covers(+EL1:effect_l, +E2:effect) is det
covers(_EL, pure) :- !.
covers([H|T], E) :-
    ( covers(H, E), !)
    ; covers(T, E).

%covers(E, effect_summary(SE, CE)) :- !,
%    covers(E, SE),
%    covers(E, CE).

%covers(E, invokes(M, Subs)) :- !,
%    compute_invoke_effects(M, Subs, ES),
%    covers(E, ES).

%% covers(+E1:effect, +E2:effect) is det
%% True iff E1 covers E2
%covers(_E1, pure) :- !.

%covers(effect_var(EV), E) :- !,
%    has_value(EV, ES),
%    covers(ES, E).

%covers(effect_summary(SE, CE), E) :- !,
%    covers(SE, E), ! ;
%    covers(CE, E).

%covers(invokes(M, Subs), E) :- !,
%    compute_invoke_effects(M, Subs, ES),
%    covers(ES, E).

%  covers(+E1:effect, +E2:effect) is det
covers(E, E) :- !.
covers(E1, E2) :-
    get_subeffect_kind_rpls(E2, E1, Rpl2, Rpl1),
    is_included(Rpl2, Rpl1), !.

%% covering(+E1:effect,+E2:effect,-E:effect) is Det
%% E is E1 if E1 covers E2, E2 if E2 covers E1, and fails otherwise
covering(E1,E2,E1) :-
    covers(E1,E2), !.
covering(E1,E2,E2) :-
    covers(E2,E1), !.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       EFFECT SUMMARIES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% effect_summary(SE_l:simple_effect_list, CE_l:compound_effect_list).

%%  get_effect_summary(+Name:string, -ES:effect_l)
%   Retrieve the effect summary of method @param Name if it is known.
get_effect_summary(Name, ESout) :-
    has_effect_summary(Name, ES),
    ( ES = effect_summary(_, _)
    -> ESout = ES
    ; ( ES = effect_var(EV),
        ( (has_value(EV, ESout), !)
        ; partially_inferred_es(EV, ESout)))
    ).


%% is_resolved(ES:effect_summary)
%  True iff all the invocation effects have been resolved (by replacing
%  them by their effects.
is_resolved(effect_summary(_SE,[])).

%% make_effect_summary(+E:effect_list, -ES:effect_summary)
%  create an effect_summary structure from an effect list
make_effect_summary(ESin, ESout) :-
    make_effect_summary_(ESin, EStmp),
    sort_es(EStmp, ESout).
make_effect_summary_([], effect_summary([],[])).
make_effect_summary_([H|T], effect_summary([H|SE],CE)) :-
    simple_effect(H),
    !,
    make_effect_summary_(T, effect_summary(SE,CE)).
make_effect_summary_([H|T], effect_summary(SE,[H|CE])) :-
    compound_effect(H),
    make_effect_summary_(T, effect_summary(SE,CE)).

%% compute_el(+EL:effect_l, -EL:effect_l)
compute_el([], []) :- !.
compute_el([H|T], EL) :-
    % deal with H
    (H = invokes(M, Subs)
    -> ( compute_invoke_effects(M, Subs, effect_summary(SE, CE)),
         CE = [],
         ELH = SE )
    ; ELH = [H]
    ),
    compute_el(T, ELT),
    add_to_el(ELH, ELT, EL).

compute_el(effect_var(EV), SE) :-
    has_value(EV, effect_summary(SE, CE)),
    CE = [].

%% compute_es(+ES:effect_summary, -ESo:effect_summary)
% compute the effect summary by computing the invocation effects
compute_es(effect_summary(SE, []), effect_summary(SE, [])) :- !. % nothing to do
compute_es(effect_summary(SE, [H|T]), ES) :- !,
    H = invokes(M, Subs),
    compute_invoke_effects(M, Subs, ES1),
    compute_es(effect_summary(SE, T), ES2),
    add_to_es(ES1, ES2, ES).

% select bewteen unrolling once or twice
unroll_es(Ei, Eo) :- unroll2_es(Ei, Eo).

%% unroll1_es(+ES:effect_summary, -ESur:effect_summary)
unroll1_es(effect_summary(SE, []), effect_summary(SE, [])) :- !.
unroll1_es(effect_summary(SEi, CEi), effect_summary(SEo, [])) :-
    compute_invoke_effects_l(CEi, effect_summary(SE2, _CE2)),
    add_to_el(SEi, SE2, SEo).

%% unroll2_es(+ES:effect_summary, -ESur:effect_summary)
unroll2_es(effect_summary(SE, []), effect_summary(SE, [])) :- !.
unroll2_es(effect_summary(SEi, CEi), effect_summary(SEo, [])) :-
    compute_invoke_effects_l(CEi, effect_summary(SE2, CE2)),
    compute_invoke_effects_l(CE2, effect_summary(SE3, _CE3)),
    add_to_el(SEi, SE2, SEtmp),
    add_to_el(SEtmp, SE3, SEo).

%% compute_invoke_effects_l(+CE:effect_l, -ES:effect_summary)
compute_invoke_effects_l([], effect_summary([], [])) :- !.
compute_invoke_effects_l([H|T], ESo) :-
    H = invokes(M, Subs),
    compute_invoke_effects(M, Subs, ESH),
    compute_invoke_effects_l(T, EST),
    add_to_es(ESH, EST, ESo).

%% compute_invoke_effects(+M:method_name, +Subs:substitution_l, -ES:effect_summary)
compute_invoke_effects(M, Subs, ES) :-
    get_effect_summary(M, ESi),
    substitute_rpl_vars(Subs, Subs0),
    substitute_rpl_vars(ESi, ES0),
    apply_sub(ES0, Subs0, ES), !.

%% gather_es_vars(+Exp:?, +Names:MethodName_l, -EVL:es_var_ord_set)
% @param Names is a list of method calls followed so far.
% Its purpose it so break recursive cycles.
gather_es_vars([H|T], Names, ESL) :-
    gather_es_vars(H, Names, ESH),
    gather_es_vars(T, Names, EST),
    ord_union(ESH, EST, ESL).
gather_es_vars([], _Names, []).
gather_es_vars(effect_summary(_SE, CE), Names, ESL) :-
    gather_es_vars(CE, Names, ESL).
gather_es_vars(invokes(Callee, _Subs), Names, ESL) :- !,
    ( memberchk(Callee, Names)
    -> ESL = []
    ; (has_effect_summary(Callee, ES),
       gather_es_vars(ES, [Callee|Names], ESL))).
gather_es_vars(pure, _Names, []) :- !.
gather_es_vars(reads(_R), _Names, []) :- !.
gather_es_vars(writes(_R), _Names, []) :- !.
gather_es_vars(effect_var(EV), Names, ESL) :- !,
    esi_constraint(_C, LHS, effect_var(EV)),
    gather_es_vars(LHS, Names, ESLHS),
    ord_union([EV], ESLHS, ESL).

gather_es_vars(C, _Names, ESL) :-
    esi_constraint(C, _L, _R), !,
    has_es_vars(C, ESL).
gather_es_vars(C, _Names, ESL) :-
    eni_constraint(C, _L, _R), !,
    has_es_vars(C, ESL).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Merging Effect Summaries and Lists
%% merge_es(+ES1:effect_summary, +ES2:effect_summary, -ESout:effect_summary)
merge_es(effect_summary(SE,CE), effect_summary([],[]), effect_summary(SE,CE)) :- !.
merge_es(effect_summary([],[]), effect_summary(SE,CE), effect_summary(SE,CE)) :- !.
merge_es(effect_summary(SE1,CE1), effect_summary(SE2,CE2), effect_summary(SE,CE)) :-
    merge_el(SE1,SE2,SE),
    merge_el(CE1,CE2,CE).

%% add_to_es(+ES1:effect_summary, +ES2:effect_summary, -ESout:effect_summary)
add_to_es(effect_summary(SE,CE), effect_summary([],[]), effect_summary(SE,CE)) :- !.
add_to_es(effect_summary([],[]), effect_summary(SE,CE), effect_summary(SE,CE)) :- !.
add_to_es(effect_summary(SE1,CE1), effect_summary(SE2,CE2), effect_summary(SE,CE)) :-
    add_to_el(SE1,SE2,SE),
    add_to_el(CE1,CE2,CE).

%% add_to_el(+EL1:effect_list, +EL2:effect_list, -ELout:effect_list)
add_to_el(ELin1, ELin2, ELout) :-
    add_to_el_(ELin1, ELin2, ELtmp),
    sort(ELtmp, ELout).

add_to_el_(EL,[],EL) :- !.
add_to_el_([H|T],EL2,EL) :-
    add_e_to_el_(H, EL2, ELo),
    add_to_el_(T, ELo, EL).
add_to_el_([],EL,EL).

add_e_to_el_(E, [H|T], EL) :-
    (covering(E, H, Eo)
    -> ( EL = [Eo|T] )
    ;  (add_e_to_el_(E, T, To), EL = [H|To])
    ).
add_e_to_el_(E, [], [E]).

%% merge_el(+EL1:effect_list, +EL2:effect_list, -ELout:effect_list)
merge_el(EL1, EL2, EL) :-
    append(EL1, EL2, ELtmp),
    simplify_el(ELtmp, ELsimp),
    sort(ELsimp, EL).

%% simplify_el(+ELin:effect_list, -ELout:effect_list) is det
%
%  Produces a simplified effect list by removing effects
%  that are covered by other effects.
simplify_el([], []).
simplify_el(ELin, ELout) :-
    simplify_el_(ELin, [], ELout).

%% simplify_el(+ELin:effect_list, ELtmp:effect_list, -ELout:effect_list)
%  Helper function to simplify effect lists.
%  Moves the head of ELin to ELtmp if it is not covered by the rest of ELin
%  or by ELtmp. N*N*covers complexity
simplify_el_([], EL, EL).
simplify_el_([H|T], ELtmp, ELout) :-
    ((covers(T, H) ; covers(ELtmp, H))
    -> (simplify_el_(T, ELtmp, ELout))
    ; ( simplify_el_(T, [H|ELtmp], ELout))).

sort_es(effect_summary(SEin, CEin), effect_summary(SEout, CEout)) :-
    sort(SEin, SEout),
    sort(CEin, CEout).

%% assertz_es(+EV:effect_var, +ESin:effect_summary, -ESSorted:effect_summary) :-
assertz_es(EV, ES, ESSorted) :-
    ignore(retract(has_value(EV, _))),
    substitute_rpl_vars(ES, ESSub),
    sort_es(ESSub, ESSorted),
    assertz(has_value(EV, ESSorted)).

%% assertz_pi_es(+EV:effect_var, +ES:effect_summary)
% Assert partially infered effect summary to the database.
% Sort before asserting for easily reading debug output.
assertz_pi_es(EV, ES) :-
    ignore(retract(partially_inferred_es(EV, _))),
    sort_es(ES, ESSorted),
    assertz(partially_inferred_es(EV, ESSorted)).

%% assertz_pi_es(+EV:effect_var, +ES:effect_summary)
% Assert partially infered effect summary to the database.
% Sort before asserting for easily reading debug output.
assertz_pi_es2(EV, ES) :-
    ignore(retract(partially_inferred_es2(EV, _))),
    sort_es(ES, ESSorted),
    assertz(partially_inferred_es2(EV, ESSorted)).

