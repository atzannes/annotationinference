% ---- rpl.pl - Safe Parallelism Inference ----*- Prolog -*--------------%
%                                                                        %
% This file is distributed under the University of Illinois Open Source  %
% License. See LICENSE.TXT for details.                                  %
%                                                                        %
% ===----------------------------------------------------------------=== %
%                                                                        %
% This module contains code for manipulating RPLs for the ASaP           %
% annotation inference.                                                  %
%                                                                        %
% ===----------------------------------------------------------------=== %

:-module(rpl, [compute_dom/1,
               intersect_dom/2,
               rpl_var/2,
               simplify_rpl/2,
               is_included/2,
               is_disjoint/2,
               apply_sub/3,
               has_expanding_sub/4,
               compute_arity/2,
               substitute_rpl_vars/2,
               instantiate_rpl_var/2,
               instantiate_rpl_vars/2
              ]).

:- use_module(util, [ intersect_l/3,

                      trying/0,
                      assert_try_l/1,
                      assert_try/1,

                      has_value/2, rgn_name/1, rgn_param/1,
                      head_rpl_var/2, tail_rpl_var/2,
                      rpl_domain/4, has_dom/3,
                      esi_constraint/3, ri_constraint/3, eni_constraint/3,
                      has_effect_summary/2
                    ]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  RPL Domain manipulation

%% gather_dom(+Env:rpl_domain, -PL:param_l, -NL:rgn_name_l
% compute the parameter list and region name list available
% to the input RplDomain
gather_dom(null_dom, [], [rGLOBAL]) :- !.
gather_dom(RplDom, PL, NL) :-
    rpl_domain(RplDom, PL1, NL1, EnvP), !,
    gather_dom(EnvP, PL2, NL2),
    append(PL1,PL2, PL),
    append(NL1,NL2, NL).

%% compute_dom(+RplVar:rpl_variable)
compute_dom(RplVar) :-
    rpl_var(RplVar, Dom),
    gather_dom(Dom, PL, NL),
    assertz(has_dom(RplVar, PL, NL)).

% intersect_dom(+RplVTo:rpl_var, +RplFrom:rpl_var)
intersect_dom(RplVTo, RplVFrom) :-
    has_dom(RplVTo, Pto, Nto),
    has_dom(RplVFrom, Pfrom, Nfrom),
    intersect_l(Pto, Pfrom, P),
    intersect_l(Nto, Nfrom, N),
    write('Intersecting domain '), print(has_dom(RplVTo, Pto, Nto)), nl,
    write('        with domain '), print(has_dom(RplVFrom, Pfrom, Nfrom)), nl,
    write('            Result: '), print(has_dom(RplVTo, P, N)), nl,
    retract(has_dom(RplVTo, Pto, Nto)),
    assertz(has_dom(RplVTo, P, N)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       RPL
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  headRplElt(+X:rpl_elt) is det
%
%   True for Head RPL Elements, i.e., elements that can appear at the head of an RPL
head_rpl_elmt(rSTAR).
head_rpl_elmt(rROOT).
head_rpl_elmt(rLOCAL).
head_rpl_elmt(X) :- rpl_var(X, _).
head_rpl_elmt(X) :- rgn_param(X).
head_rpl_elmt(X) :- rgn_name(X).

rpl_var(X, Dom) :- head_rpl_var(X, Dom), !.
rpl_var(X, Dom) :- tail_rpl_var(X, Dom).


% Tail RPL Elements, i.e., elements that can appear at non head spots
%nonHeadRplElt(qmark).
tail_rpl_elmt(rSTAR).
tail_rpl_elmt(X) :- rgn_name(X).

% rpl(RplElmts:rpl_elmt_l, Subs:sub_l)
rpl([],[]).

% rpl_elmt_l true when the rpl is well formed
rpl_elmt_l([H|T]) :-
    head_rpl_elmt(H),
    tail_rpl_elmt_l(T).

tail_rpl_elmt_l([]).
tail_rpl_elmt_l([H|T]) :-
    tail_rpl_elmt(H),
    tail_rpl_elmt_l(T).

%% substitute_rpl_vars(+Rin:rpl_elmt_l, -Rout:rpl_elmt_l)
substitute_rpl_vars([], []) :- !.
substitute_rpl_vars([H|T], Rout) :- !,
    substitute_rpl_vars(H, R1),
    substitute_rpl_vars(T, R2),
    ( is_list(R1)
      -> append(R1, R2, Rout)
      ;  Rout = [R1|R2]).

substitute_rpl_vars(effect_summary(SE, CE), effect_summary(SEo, CEo)) :- !,
    substitute_rpl_vars(SE, SEo),
    substitute_rpl_vars(CE, CEo).

substitute_rpl_vars(reads(Rpl), reads(RplOut)) :- !,
    substitute_rpl_vars(Rpl, RplOut).

substitute_rpl_vars(writes(Rpl), writes(RplOut)) :- !,
    substitute_rpl_vars(Rpl, RplOut).

substitute_rpl_vars(invokes(M, Subs), invokes(M, SubsOut)) :- !,
    substitute_rpl_vars(Subs, SubsOut).

substitute_rpl_vars(subst_set(S), subst_set(So)) :- !,
    substitute_rpl_vars(S, So).

substitute_rpl_vars(param_sub(P, R), param_sub(P, Ro)) :- !,
    substitute_rpl_vars(R, Ro).

% STRUCTURE AND VALUE DRIVEN CHECKING
substitute_rpl_vars(rpl(Rin, []), rpl(Rout, [])) :- !,
    substitute_rpl_vars(Rin, Rout).
substitute_rpl_vars(rpl(Rin, Sin), RplOut) :- !,
    substitute_rpl_vars(Rin, Rout),
    (Rin\=Rout
    -> (apply_sub(rpl(Rout, []), Sin, RplTmp),
        substitute_rpl_vars(RplTmp, RplOut))
    ;  (substitute_rpl_vars(Sin, Sout),
        apply_sub(rpl(Rout, []), Sout, RplOut))).

% NAIVE CHECKING
%substitute_rpl_vars(rpl(Rin, Sin), RplOut) :- !,
%    substitute_rpl_vars(Rin, Rout),
%    substitute_rpl_vars(Sin, Sout),
%    apply_sub(rpl(Rout, []), Sout, RplOut).

substitute_rpl_vars(Elmt, Rout) :-
    rpl_var(Elmt, _Dom), !,
    %instantiate_rpl_var(Elmt),
    (has_value(Elmt, RVal)
     -> substitute_rpl_vars(RVal, Rout)
     ; (trying
        -> (assert_try(Elmt), instantiate_rpl_var(Elmt, RVL),
            assert_try_l(RVL),
            has_value(Elmt, RVal2), substitute_rpl_vars(RVal2, Rout))
        ; Rout = Elmt)
    ).

substitute_rpl_vars(Elmt, Elmt).

%% strip_root(+RPLin:rpl/rpl_l, -RPLout:rpl/rpl_l)
%  Substitutes RPL vars with their values (all RPL var must have values) and
%  removes any explicit Root region from the head of RPLs
strip_root(Rin, Rout) :-
    substitute_rpl_vars(Rin, Rtmp),
    strip_root_(Rtmp, Rout).

strip_root_(rpl([rROOT|T], Subs), rpl([T], Subs)) :- !.
strip_root_(rpl(Elmts, Subs), rpl(Elmts, Subs)).

strip_root_([rROOT|T], [T]) :- !.
strip_root_([H|T], [H|T]) :- !.
strip_root_([], []) :- !.

%%  addToRpl(+Rpl1:rpl, +Last:rpl_elt, -Rpl2:rpl) is det
%%  addToRpl(-Rpl1:rpl, -Last:rpl_elt, +Rpl2:rpl) is det
%
%   Rpl2 = Rpl1 : Last
add_to_rpl(rpl(R1, Subs1), RplEl, rpl(R2, Subs1)) :-
    append(R1, [RplEl], R2).
add_to_rpl(Rpl1, E, Rpl2) :-
    (Rpl1 = [_H1|_T1] ; Rpl1 = []),
    Rpl2 = [_H2|_T2],
    append(Rpl1, [E], Rpl2).

%% simplify_rpl(+RPLin:rpl, -RPLout:rpl)
%
%  Simplify an RPL with multiple stars to contain a single star
simplify_rpl([], []).
simplify_rpl(Rin, Rout) :-
    fully_spec_prefix(Rin, Rpref, Rrest),
    (Rrest = []
    -> (Rout = Rpref)
    ;  (fully_spec_postfix(Rrest,[], Rtail),
        append(Rpref, [rSTAR|Rtail], Rout))
    ).

%% fully_spec_prefix_(+Rin:rpl_elmts, -Rpref:rpl_elmts, -Rrest:rpl_elmts)
%
% Given an RPL it produces a star-free prefix and the rest of the RPL.
fully_spec_prefix([], [], []) :- !.
fully_spec_prefix([rSTAR|T], [], [rSTAR|T]) :- !.
fully_spec_prefix([H|T], [H|T2], Rrest) :-
    fully_spec_prefix(T, T2, Rrest).

%% fully_spec_postfix_(+Rin:rpl_elmts, -Rpref:rpl_elmts, -Rrest:rpl_elmts)
%
% Given an RPL, it produces a star-free postfix and the rest of the RPL.
fully_spec_postfix([], Mem, Rev) :- reverse(Mem, Rev).
fully_spec_postfix([rSTAR|T], _Mem, T2) :-
    fully_spec_postfix(T, [], T2),!.
fully_spec_postfix([H|T], Mem, T2) :-
    fully_spec_postfix(T, [H|Mem], T2).

%% is_under(+RPL1:rpl,+RPL2:rpl) is det
%% True iff RPL1 is under RPL2
is_under(Rpl1, Rpl2) :-
    strip_root(Rpl1, SansRoot1),
    strip_root(Rpl2, SansRoot2),
    is_under_(SansRoot1, SansRoot2).

is_under_(_Any, rpl([rROOT], [])) :- !.   % [Under Root]
is_under_(Rpl, Rpl) :- !.                 % [Reflexive]
is_under_(Rpl1, Rpl2) :-                  % [Under Name&Index&Star]
    add_to_rpl(Rpl3, _, Rpl1),            % Rpl1 = Rpl3 : Tail
    is_under_(Rpl3, Rpl2).
is_under_(Rpl1, Rpl2) :-                 % [Under-Included]
    is_included_(Rpl1, Rpl2),!.

%%  is_ncluded(+Rpl1:rpl, +Rpl2:rpl) is det
%   True if Rpl1 and Rpl2 are well formed and Rpl1 C= Rpl2 (\subseteq)
is_included(Rpl1, Rpl2) :-
    strip_root(Rpl1, SansRoot1),
    strip_root(Rpl2, SansRoot2),
    is_included_(SansRoot1, SansRoot2),!.

is_included_(Rpl1, Rpl1) :- !.          % [Reflexive]

%isIncluded_(Rpl1, Rpl3) :-         % [Transitive] hopefully not needed for computation
%   isIncluded_(Rpl1, Rpl2),
%   isIncluded_(Rpl2, Rpl3).

is_included_(Rpl1, Rpl2) :-         % [Include Name] & [Include Index]
    add_to_rpl(Rpl3, Tail, Rpl1),       % Rpl1 = Rpl3 : Tail
    add_to_rpl(Rpl4, Tail, Rpl2),       % Rpl2 = Rpl4 : Tail
    is_included_(Rpl3, Rpl4),!.

is_included_(Rpl1, Rpl2) :-         % [Include Star]
    add_to_rpl(Rpl3, rSTAR, Rpl2),      % Rpl2 = Rpl3 : *
    is_under_(Rpl1, Rpl3),!.

% TODO
%isIncluded_(Rpl1, Rpl2) :-         % [Include Full] this creates an infinite
                                    % loop, is it even needed? (isn't it
                                    % captured by the reflexivity?
%   isFullySpecifiedRpl(Rpl1),
%   isIncluded_(Rpl2, Rpl1).

% TODO [Include Param]

%% is_disjoint(+Rpl1:rpl, +Rpl2:rpl) is det
is_disjoint(Rpl1, Rpl2) :-
    strip_root(Rpl1, SansRoot1),
    strip_root(Rpl2, SansRoot2),
    is_disjoint_(SansRoot1, SansRoot2), !.

is_disjoint_(Rpl1, _Rpl2) :-
    is_under_(Rpl1, rpl([rLOCAL], [])), !. % exception
is_disjoint_(_Rpl1, Rpl2) :-
    is_under_(Rpl2, rpl([rLOCAL], [])), !. % exception
is_disjoint_(Rpl, Rpl) :- !, fail. % short-cut
is_disjoint_(rpl(Rpl1, Subs), rpl(Rpl2, Subs)) :-
    ( is_distinct_left(Rpl1, Rpl2), !
    ; is_distinct_right(Rpl1, Rpl2), !
    ; param_star_rule(Rpl1, Rpl2), ! ).

not_star(X) :- X \= rSTAR.

is_distinct_left([H|_T], []) :- not_star(H).
is_distinct_left([], [H|_T]) :- not_star(H).
is_distinct_left([H1|_T1], [H2|_T2]) :-
    not_star(H1),
    not_star(H2),
    H1 \== H2,
    ((rgn_name(H1), rgn_name(H2))
    ;(rgn_param(H1), rgn_param(H2))).

is_distinct_left([H|T1], [H|T2]) :-
   not_star(H),
   is_distinct_left(T1, T2).

is_distinct_right([H|_T] ,[]) :- not_star(H).
is_distinct_right([], [H|_T]) :- not_star(H).
is_distinct_right(Rpl1, Rpl2) :-
    add_to_rpl(_RplH1, LastElt1, Rpl1),
    add_to_rpl(_RplH2, LastElt2, Rpl2),
    not_star(LastElt1),
    not_star(LastElt2),
    LastElt1 \== LastElt2,
    rgn_name(LastElt1), rgn_name(LastElt2).
is_distinct_right(Rpl1, Rpl2) :-
    add_to_rpl(RplH1, LastElt, Rpl1),
    add_to_rpl(RplH2, LastElt, Rpl2),
    not_star(LastElt),
    is_distinct_right(RplH1, RplH2).

param_star_rule(Rpl1, Rpl2) :-
    is_static_rpl(Rpl1), !,
    param_star_rule_(Rpl1, Rpl2).
param_star_rule(Rpl1, Rpl2) :-
    is_static_rpl(Rpl2),
    param_star_rule_(Rpl2, Rpl1).

param_star_rule_(StaticRpl, [H|T]) :-
    rgn_param(H),
    fully_spec_prefix(T, Tpref, _Trest),
    \+sublist(Tpref, StaticRpl).

is_static_rpl([H|T]) :-
    rgn_name(H),
    is_static_rpl(T).
is_static_rpl([]).

sublist( [], _ ).
sublist( [X|XS], [X|XSS] ) :- sublist( XS, XSS ).
sublist( [X|XS], [_|XSS] ) :- sublist( [X|XS], XSS ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       apply_sub
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% apply_sub(+Ein:effect_summary, +S:substitution_l, -Eout:effect_summary)
%  Applies the substitutions S to the effect summary Ein to produce Eout.
apply_sub(rpl([H|T], Subs), Sub, RplOut) :-
    rpl_var(H, _), !,
    ( has_value(H, Rpl) ->
      ( append(Rpl, T, Rtmp),
        apply_sub(rpl(Rtmp, Subs), Sub, RplOut))
      ; ( is_list(Sub) ->
          ( append(Subs, Sub, SubOut))
          ; ( append(Subs, [Sub], SubOut))),
      RplOut = rpl([H|T], SubOut)) .

apply_sub(rpl(Rin, Subs), S, RplOut) :-
    Subs \= [], !,
    (is_list(S)
     -> append(Subs, S, AllSubs)
     ;  append(Subs, [S], AllSubs)),
    apply_sub(rpl(Rin, []), AllSubs, RplOut).

apply_sub(X, [], X) :- !.
apply_sub(X, [subst_set([])], X) :- !.
apply_sub(X, subst_set([]), X) :- !.
apply_sub([], _SubL, []) :- !.

apply_sub(effect_summary(SE, CE), SubL, effect_summary(SEout, CEout)) :- !, % overload cut
    apply_sub(SE, SubL, SEout),
    apply_sub(CE, SubL, CEout).

% apply_sub(+Ein:effect_l, +S:substitution_l, -Eout:effect_l)
apply_sub(EffLin, [H|T], EffLout) :- !, % overload cut
    apply_sub(EffLin, H, EffLout1),
    apply_sub(EffLout1, T, EffLout).

% apply_sub(+ELin:effect_l, +S:substitution, -ELout:effect_l)
apply_sub([H|T], Sub, [Hout|Tout]) :- !,
    \+ is_list(Sub), % make sure Sub is not a list
    apply_sub(H, Sub, Hout),
    apply_sub(T, Sub, Tout).

% apply_sub(+Ein:effect, +S:substitution, -Eout:effect)
apply_sub(pure, _, pure).
apply_sub(reads(X), Sub, reads(Y)) :-
    X = rpl(_, _),
    apply_sub(X, Sub, Y).
apply_sub(writes(X), Sub, writes(Y)) :-
    X = rpl(_, _),
    apply_sub(X, Sub, Y).
apply_sub(invokes(M,S), Sub, invokes(M,SubLout)) :-
    is_list(Sub), append(S, Sub, SubLout).
apply_sub(invokes(M,S), Sub, invokes(M,SubLout)) :- !,
    \+ is_list(Sub), append(S, [Sub], SubLout).

% apply_sub on RPLs
% apply_sub(rpl(Rpl,SubLin), Sub, rpl(Rpl,SubLout)) :-
%    Rpl = [H|T],
%    rpl_var(H),
%    append(SubLin, [Sub], SubLout),
%    !.  % TODO: we may be able to apply this sub
%        % to the last sub in the SubLin vector.
apply_sub(rpl(R1, []), subst_set([H|T]), rpl(Rout, Sout)) :-
    (try_apply_sub(rpl(R1, []), H, rpl(Rout, Sout)), !)
  ; (apply_sub(rpl(R1, []), subst_set(T), rpl(Rout, Sout)), !).

apply_sub(rpl([H|T], []), param_sub(P, RplSub), RplOut) :-
    %\+ rpl_var(H, _),
    H = P ->
    ( RplSub = rpl(Elmts, Subs),
      append(Elmts, T, Rtmp),
      simplify_rpl(Rtmp, Rout),
      apply_sub(rpl(Rout, []), Subs, RplOut)
    )
    ;  RplOut = rpl([H|T], []). % i.e., RplOut = RplIn

% like apply_sub but succeeds only if substitution was applicable
try_apply_sub(rpl([P|T], []), param_sub(P, RplSub), RplOut) :-
    RplSub = rpl(Elmts, Subs),
    append(Elmts, T, Rtmp),
    simplify_rpl(Rtmp, Rout),
    apply_sub(rpl(Rout, []), Subs, RplOut).

%% has_expanding_sub(Subs:substitution_l, +Ph:region_param, +Pt:region_param, -Srest:substitution_l)
%  True iff at least one of the substitutions in the list is expanding from Ph to Pt.
%  Returns the remaining substitutions.
has_expanding_sub([H|T], Ph, Pt, T) :-
    is_expanding_sub(H, Ph, Pt), !.
has_expanding_sub([H|T], Ph, Pt, [H|Trest]) :-
    has_expanding_sub(T, Ph, Pt, Trest).

%% is_preserving_sub(+Sub:substitution, Ph:region_param, +Pt:region_param)
is_preserving_sub(Sub, Ph, Pt) :-
    Sub = param_sub(Ph, rpl([Pt|_Ts], [])),
    rgn_param(Ph), rgn_param(Pt).

%% is_expanding_sub(+Sub:substitution, +Ph:region_param, +Pt:region_param)
is_expanding_sub(Sub, Ph, Pt) :-
    Sub = param_sub(Ph, rpl([Pt|Ts], [])),
    rgn_param(Ph), rgn_param(Pt),
    Ts \= [].


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       RplVar Instantiation & Arity
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rpl_var_arity(RplV, 1) :-
    has_value(RplV, _), !.
rpl_var_arity(RplV, Arity) :-
    has_dom(RplV, P, N),
    length(P, Pl),
    length(N, Nl),
    (must_have_param(RplV)
     -> (cannot_have_star(RplV)
         -> Arity is Pl*Nl + Pl
         ;  Arity is Pl*Nl + 2*Pl + 1)
     ;  (cannot_have_star(RplV)
         -> Arity is Pl*Nl + Pl + Nl
         ;  Arity is Pl*Nl + 2*Pl + Nl + 1)).

compute_arity([], 1).
compute_arity([H|T], A) :-
    rpl_var_arity(H, Ah),
    %write('RplVar arity:'), print(rpl_var_arity(H, Ah)), nl,
    compute_arity(T, At),
    A is Ah * At.

%% instantiate_rpl_vars(+RplVarList:rpl_var_l,  -RplVarListOut:rpl_var_ord_set)
% RplVarListOut keeps track of which RPL vars were actually instantiated
% For each uninstantiated RplVar in the list,
% instantiate the var. Retrying this goal results
% in the necessary backtracking to exhaustively search
% the search-space. Instantiating an RplVar means adding
% a has_value(RplVar, Value) to the list of prolog facts.
% Backtracking removes the previous such fact and adds
% a new one if successful.
instantiate_rpl_vars([], []) :-!.
instantiate_rpl_vars([H|T], RVLinit) :-
    instantiate_rpl_var(H, Hinit),
    %write('.'),
    instantiate_rpl_vars(T, Tinit),
    ord_union(Hinit, Tinit, RVLinit).

%% instantiate_rpl_var(+RplVar:rpl_var, +PL:param_l, +NL:rgn_name_l, -RplOut:rpl_elmt_l)
instantiate_rpl_var(RplVar, RVLinit) :-
    has_value(RplVar, RplOut), !,
    % maybe RplOut contains RPL vars (e.g., because of RI constraint simplification)
    gather_instantiate_rpl_vars(RplOut, RVLinit).

instantiate_rpl_var(RplVar, [RplVar]) :-
    has_dom(RplVar, PL, NL),
    instantiate_rpl_var_(RplVar, PL, NL, _RplOut),
    (trying
     -> true
     ; (
      % write('Instantiated '), write(RplVar), write(' to '), write(RplOut), nl,
        try_check_involved_constraints([RplVar])) % induce early backtracking
    ).

instantiate_rpl_var_(RplVar, PL, NL, RplOut) :-
    %( may_not_have_param(RplVar), gen_names(N, Out);
    (
      \+ must_be_expanding(RplVar), gen_params(PL, RplOut);
      gen_params_names(PL, NL, RplOut);
      \+ must_have_param(RplVar), \+ must_be_expanding(RplVar), gen_names(NL, RplOut);
      \+ cannot_have_star(RplVar), \+ must_be_expanding(RplVar), gen_params_star(PL, RplOut);
      %may_not_have_param(RplVar), Out = [star]).
      \+ must_have_param(RplVar), \+ must_be_expanding(RplVar), \+cannot_have_star(RplVar), RplOut = [rSTAR]
    ),
    ignore(retract(has_value(RplVar, _))),
    assertz(has_value(RplVar, RplOut)).
instantiate_rpl_var_(RplVar, _PL, _NL, []) :-
    ignore(retract(has_value(RplVar, _))),
    false.

%% gen_names(+NL:rgn_name_l, -N:rgn_name)
%  Produces, one at a time, all the elements of the list.
%  Very similar to the select/3 predicate.
gen_names([H|T], Out):-
    Out = [H];
    gen_names(T, Out).
%gen_names(NL, [Out]):-
%   member(Out, NL).

%% gen_params(+PL:param_l, -P:param)
%  Produces, one at a time, all the elements of the list.
%  Very similar to the select/3 predicate.
gen_params([H|T], Out):-
    Out = [H];
    gen_params(T, Out).
%gen_params(PL, [Out]):-
%   member(Out, PL).

%% gen_params_names(+PL:param_l, +NL:rgn_name_l, -RplElmts:rpl_elmt_l
%gen_params_names([H|T], NL, Out):-
%    gen_names(NL, OutN), Out = [H|OutN];
%    gen_params_names(T, NL, Out).
gen_params_names(PL, [H|T], Out):-
    gen_params(PL, OutP), append(OutP, [H], Out);
    gen_params_names(PL, T, Out).

%gen_params_names(PL, NL, [P, N]):-
%   member(P, PL),
%   member(N, NL).

%% gen_params_star(+PL:param_l, -RplElmts:rpl_elmt_l)
%
gen_params_star([H|T], Out):-
    Out = [H, rSTAR];
    gen_params_star(T, Out).
%gen_params_star(PL, [P, rSTAR]):-
%   member(P, PL).
