% ---- util.pl - Safe Parallelism Inference ----*- Prolog -*-------------%
%                                                                        %
% This file is distributed under the University of Illinois Open Source  %
% License. See LICENSE.TXT for details.                                  %
%                                                                        %
% ===----------------------------------------------------------------=== %
%                                                                        %
% This module contains common utility code for the ASaP annotation       %
% inference.                                                             %
%                                                                        %
% ===----------------------------------------------------------------=== %
:-module(util, [
                minus_sets/3,
                intersect_minus_sets/4,
                intersect_l/3,

                trying/0,
                push_trying/0,
                pop_trying/0,
                check_no_try/0,
                clear_try/0,
                assert_try/1,
                assert_try_l/1,
                retract_tries/1,

                has_value/2,
                rgn_name/1,
                rgn_param/1,
                head_rpl_var/2,
                tail_rpl_var/2,
                rpl_domain/4,
                has_dom/3,
                esi_constraint/3,
                ri_constraint/3,
                eni_constraint/3,
                has_effect_summary/2,
                partially_inferred_es/2,
                partially_inferred_es2/2
               ]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% dynamic predicates provided by Clang
:- dynamic rgn_name/1.
:- dynamic rgn_param/1.
:- dynamic head_rpl_var/2.
:- dynamic tail_rpl_var/2.

:- dynamic has_dom/3.
:- dynamic rpl_domain/4.

:- dynamic esi_constraint/3.
:- dynamic ri_constraint/3.
:- dynamic eni_constraint/3.

:- dynamic has_effect_summary/2.
:- dynamic partially_inferred_es/2.
:- dynamic partially_inferred_es2/2.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% dynamic predicates computed by solver
:- dynamic has_value/2.
:- dynamic has_dom/3.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% trying
% present if currently trying to see if a constraint is still satisfiable
:- dynamic trying/0.

is_trying :- trying.

push_trying :- assertz(trying).
pop_trying :- retract(trying).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% try

% try(X:rpl_var) this RPL var has been instantiated on a trial basis
:- dynamic try/1.

%% assert_try(+V:rpl_var)
assert_try_l([H|T]) :-
    assert_try(H),
    assert_try_l(T).
assert_try_l([]).
assert_try(V) :-
    try(V)
    -> true
    ; assertz(try(V)).

%% check_no_try
check_no_try :-
    \+ try(_).

clear_try :-
    findall(X, try(X), TryL),
    retract_tries(TryL).

retract_tries([]) :- !.
retract_tries([H|T]) :-
    ignore(retract(has_value(H, _))),
    retract(try(H)),
    retract_tries(T).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set operations I should replace with built-ins

%% minus_sets(+S1:list, +S2:list, -Sout:list)
% Sout = S1 / S2
minus_sets(S1, [], S1) :- !.
minus_sets(S1, [H|T], Sout) :-
    (select(H, S1, S1mH), !)
    -> minus_sets(S1mH, T, Sout)
    ;  minus_sets(S1, T, Sout).

%% intersect_minus_sets(+S1:list, +S2:list, -Intersect:list, -S2mS1: list)
intersect_minus_sets([], S2, [], S2) :- !.
intersect_minus_sets(_S1, [], [], []) :- !.
intersect_minus_sets(S1, [H|T], [H|IntersectT], S2mS1T) :-
    memberchk(H, S1), !,
    intersect_minus_sets(S1, T, IntersectT, S2mS1T).
intersect_minus_sets(S1, [H|T], IntersectT, [H|S2mS1T]) :-
%   \+ memberchk(H, S1), !, % implicit because of previous rule
    intersect_minus_sets(S1, T, IntersectT, S2mS1T).


%% intersect_l(+L1:list, +L2:list, -L3:list)
% L3 is made up of the elements that are members of both L1 and L2
intersect_l([], _L2, []) :- !.
intersect_l(_L1, [], []) :- !.
intersect_l([H|T], L2, L) :-
    intersect_l(T, L2, Lt),
    ( memberchk(H, L2)
      -> L = [H|Lt]
      ; L = Lt).

