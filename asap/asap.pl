% ---- asap.pl - Safe Parallelism Inference ----*- Prolog -*-------------%
%                                                                        %
% This file is distributed under the University of Illinois Open Source  %
% License. See LICENSE.TXT for details.                                  %
%                                                                        %
% ===----------------------------------------------------------------=== %
%                                                                        %
% This file contains the core constraint solving code for the ASaP       %
% annotation inference.                                                  %
%                                                                        %
% ===----------------------------------------------------------------=== %

:- use_module(util, [ minus_sets/3, intersect_minus_sets/4,

                      trying/0, push_trying/0, pop_trying/0,
                      check_no_try/0, clear_try/0,
                      assert_try/1, retract_tries/1,

                      has_value/2, rgn_name/1, rgn_param/1,
                      head_rpl_var/2, tail_rpl_var/2,
                      rpl_domain/4, has_dom/3,
                      esi_constraint/3, ri_constraint/3, eni_constraint/3,
                      has_effect_summary/2, partially_inferred_es/2
                    ]).

:- use_module(rpl).
:- use_module(effect).
:- use_module(effect_inference).

:- dynamic must_have_param/1.
:- dynamic must_be_expanding/1.
:- dynamic cannot_have_star/1.

% has_rpl_vars(C:constraint, RVL:rpl_ord_set)
:- dynamic has_rpl_vars/2.
% is_in_constraint(RV:rpl_var, CL:constraint_ord_set)
:- dynamic is_in_constraint/2.

%% intersect_constraints(C1:eni_constraint, C2:ri_constraint, RplVarIntersection, RplVarIntersectionLength, S2\S1, S2\S1Lenght)
:- dynamic intersect_constraints/6.
:- dynamic has_es_vars/2.

%% has_subconstraints(C:eni_constraint, Cww:ww_eni_subc_l, Crw:rw_eni_subc_l)
:- dynamic has_subconstraints/3.
:- dynamic eni_sub_constraint/4.

%% unrolled_eni_constraint(C:eni_constraint, LHS_R:LHS_read_effect_l, LHS_W: LHS_write_effect_l,
%                                            RHS_R:RHS_read_effect_l, RHS_W: RHS_write_effect_l)
:- dynamic unrolled_eni_constraint/5.
% is_in_subconstraint(RV:rpl_var, CL:pair_l<constraint, ord_quadset>)
:- dynamic is_in_subconstraint/2.

%% Flags to control which simplifications are performed
:- dynamic simplify_basic/0.
:- dynamic simplify_disjoint_writes/0.
:- dynamic simplify_recursive_writes/0.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       ENTRY POINT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
solve_all :-
    preprocess_phase,
    !,
    % Phase 3: constraint simplification
    simplify_phase,
    !,
    % Phase 4: Rpl Instantiation and Checking
    instantiate_and_check_phase,

    % Wrap-up
    % Instantiate the remaining RPL vars (which are not used but Clang will query for their values)
    findall(RV, head_rpl_var(RV, _D), StartingRplVars), % find all the RPL Vars
    instantiate_rpl_vars(StartingRplVars, _RVList),
    % RPL vars and effect summaries may have values in terms of rpl variables, substitute them.
    evaluate_all,
    write('==============='), nl,
    write('INFERENCE DONE!'), nl,
    write('==============='), nl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       PHASE 1: PREPROCESS PHASE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
preprocess_phase :-
    % preprocessing
    % 1. compute domains
    findall(RV, head_rpl_var(RV, _D), StartingRplVars), % find all the RPL Vars
    foreach(member(RVx, StartingRplVars), compute_dom(RVx)),

    % 2. Compute which RPL vars are reachable for each constraint
    findall(C1, ri_constraint(C1, _, _), RIL),
    findall(C2, esi_constraint(C2, _, _), ESIL),
    findall(C3, eni_constraint(C3, _, _), NIL),
    foreach(member(Cri, RIL),  compute_ri_rpl_vars(Cri)),
    foreach(member(Cesi, ESIL), compute_esi_vars(Cesi)),
    foreach(member(Ceni, NIL),  compute_eni_vars(Ceni)),

    % 3. Relevant RPL Vars: are those that are reachable form the NI constraints
    calculate_arity(A1),
    write('Initial Space Size = '), print(A1), nl,

    %sort_esi_constraints(ESIL),
    preprocess_esi_constraints(ESIL),
    finalize_partial_es_all,

    gen_eni_subc_cl(NIL). % foreach(member(Ceni2, NIL),  gen_eni_subc(Ceni2)).


%% sort_esi_constraints(+ESI:esi_constraint_l)
sort_esi_constraints([H|T]) :-
    sort_esi_constraint(H),
    sort_esi_constraints(T).
sort_esi_constraints([]).

sort_esi_constraint(C) :-
    esi_constraint(C, LHS, EV),
    sort(LHS, LHSsorted),
    retract(esi_constraint(C, LHS, EV)),
    assertz(esi_constraint(C, LHSsorted, EV)).

%% preprocess_esi_constraints(+ESI:esi_constraint_l)
% Call esi_collect on all constraints to break call-graph cycles
% and partially infer the effect summaries
preprocess_esi_constraints([H|T]) :-
    preprocess_esi_constraint(H),
    preprocess_esi_constraints(T).
preprocess_esi_constraints([]).

preprocess_esi_constraint(C) :-
    esi_constraint(C, LHS, effect_var(EV)),
    ( partially_inferred_es(EV, _ES)
    ->  true
    ; ( has_effect_summary(FunName, effect_var(EV)),
        esi_collect(EV, FunName, LHS, _))).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       PHASE 2: SIMPLIFY PHASE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
simplify_phase :-
    findall(C3, eni_constraint(C3, _, _), NIL),

    (simplify_basic
    -> (findall(Ci0, ri_constraint(Ci0, _, _), RIL),
        simplify_ri_constraints(RIL), !,
        % Here we really need a fixpoint but for now just run it 4 times
        findall(Ci1, ri_constraint(Ci1, _, _), RIL1),
        preprocess_ri_constraints(RIL1), !,
        findall(Ci2, ri_constraint(Ci2, _, _), RIL2),
        preprocess_ri_constraints(RIL2), !,
        findall(Ci3, ri_constraint(Ci3, _, _), RIL3),
        preprocess_ri_constraints(RIL3), !,
        findall(Ci4, ri_constraint(Ci4, _, _), RIL4),
        preprocess_ri_constraints(RIL4), !,
        calculate_arity(A2),
        write('Simplified Space Size (After RPL Inclusion Constraint Simplification) = '), print(A2), nl)
    ; true),

    (simplify_disjoint_writes
    -> (% Application of Theorem 1
        preprocess_eni_constraints(NIL), !,
        calculate_arity(A3),
        write('Simplified Space Size (After Theorem 1 Simplification) = '), print(A3), nl)
    ; true),

    (simplify_recursive_writes
    -> (% Application of Theorem 2: simplify_recursive_writes
        foreach(headof_CG_cycle(X), simplify_recursive_writes(X)), !,
        calculate_arity(A4),
        write('Simplified Space Size (After Theorem 2 Simplification) = '), print(A4), nl)
    ; true).


%% simplify_ri_constraints(+L:ri_constraint_l)
%  Applies substitutions and removes RI constraints that are tautologies
simplify_ri_constraints([]) :- !.
simplify_ri_constraints([H|T]) :-
    simplify_ri_constraint(H),
    simplify_ri_constraints(T).

simplify_ri_constraint(C) :-
    ri_constraint(C, L, R),
    apply_sub(L, [], L1),
    apply_sub(R, [], R1),
    once(retract(ri_constraint(C, L, R))),
    ( \+ is_included(L1, R1)
     -> assertz(ri_constraint(C, L1, R1))
     ;  write('retracted '), print(ri_constraint(C, L, R)), nl).

%% preprocess_ri_constraints(+L:ri_constraint_l)
%  Finds trivial chains of RI constraints and makes RPLVars equal
%  (i.e., unifies them)
preprocess_ri_constraints([]) :- !.
preprocess_ri_constraints(L) :-
    preprocess_ri_constraints_(L, []).

%% preprocess_ri_constraints_(+L:ri_constraint_id_l, +Rest:ri_constraint_id_l)
% Sometimes we get a constraint: RPL \Incl RPL_Var. If RPL_Var does not appear
% in the other RI constraints or in RPL, it is complete to set RPL_Var = RPL.
preprocess_ri_constraints_([], _R) :- !.
preprocess_ri_constraints_([H|T], Rest) :-
    ri_constraint(H, L, R),
    L = rpl([LrH|LrT], []),
    R = rpl([RrH|[]], []),!,
    gather_rpl_vars(T, [], TRVs),
    gather_rpl_vars(Rest, [], RestRVs),
    gather_rpl_vars(L, [], LRVs),
    ((head_rpl_var(RrH, _DR),
      \+memberchk(RrH, TRVs),
      \+memberchk(RrH, RestRVs),
      \+memberchk(RrH, LRVs)
     ) -> (assertz(has_value(RrH, [LrH|LrT])),  % RrH <- LrH:LrT
           once(retract(ri_constraint(H, L, R))),
           write('retracted '), print(ri_constraint(H, L, R)), nl,
           (has_dom(LrH, _Pl, _Nl) -> intersect_dom(LrH, RrH) ; true),
           ((head_rpl_var(LrH, _DL) -> update_is_in_constraint(LrH, RrH) ; true)),
           preprocess_ri_constraints_(T, Rest))
     ; preprocess_ri_constraints_(T, [H|Rest])
    ).
% no-op
preprocess_ri_constraints_([H|T], Rest) :-
    preprocess_ri_constraints_(T, [H|Rest]).

%% update_is_in_constraint(+NewRV:rpl_var, +OldRV:rpl_var)
% OldRV was set to NewRV, therefore OldRV is now also effectively appearing
% in the constraints where NewRV was appearing.
update_is_in_constraint(NewRV, OldRV) :-
    (is_in_constraint(OldRV, OldCL)
    ->(assert_is_in_constraint(NewRV, OldCL),
       add_has_rpl_var(NewRV, OldCL))
    ;  true),
    (is_in_subconstraint(OldRV, OldQCL)
    ->(add_rpl_var_to_subconstraints(NewRV, OldQCL))
    ;  true).

%% preprocess_eni_constraints : Theorem 1
%% 2. rv[P <- R1] # rv[P <- R2] ==> rv = P : rv', R1 # R2
%% assertz(must_have_param(rv, P))
%% assertz(rpl_tail(rv', dom_rv))
%% assertz(rni_constraint(C, R1, R2))
preprocess_eni_constraints([]) :- !.
preprocess_eni_constraints([H|T]) :-
    preprocess_eni_constraint(H),
    preprocess_eni_constraints(T).

preprocess_eni_constraint(C) :-
    eni_constraint(C, L, R),
    compute_es(L, effect_summary(SL1, _CL1)),
    compute_es(R, effect_summary(SR1, _CR1)),
    preprocess_eni_ll(SL1, SR1).

preprocess_eni_ll([], _SR1).
preprocess_eni_ll([H|T], SR1) :-
    preprocess_eni_el(H, SR1),
    preprocess_eni_ll(T, SR1).

preprocess_eni_el(_E, []).
preprocess_eni_el(E, [H|T]):-
    preprocess_eni_ee(E, H),
    preprocess_eni_el(E, T).

preprocess_eni_ee(E1, E2) :-
    is_non_interfering_effect_kind(E1, E2), !.
preprocess_eni_ee(E1, E2) :-
    get_effect_rpl(E1, R1),
    get_effect_rpl(E2, R2),
    preprocess_eni_rr(R1, R2).

preprocess_eni_rr(rpl(X, S1), rpl(X, S2)) :-
    X = [H|_T],
    head_rpl_var(H, _Dom), !,
    assert_must_have_param(H),
    preprocess_eni_ss(S1, S2).

preprocess_eni_rr(_, _).

preprocess_eni_ss(_, _). % TODO

%% simplify_recursive_writes(+Name:function_str)
%  Theorem 2.
simplify_recursive_writes(Name) :-
    has_effect_summary(Name, effect_var(EV)),
    partially_inferred_es(EV, effect_summary(SE, CE)),
    findall(I, (member(I, CE), I = invokes(Name, _Subs)), LooptyDoops),
    foreach(member(writes(RPL), SE), simplify_recursive_write(RPL, LooptyDoops)).

%% simplify_recursive_write(+WritesRpl:rpl, +CG_Cycles:invokes_effect_l)
simplify_recursive_write(rpl([H|_T], S), LooptyDoops) :-
    rgn_param(H), !,
    assert_must_have_params(S),
    foreach(member(I, LooptyDoops), assert_expanding_cycles([H], I)).

simplify_recursive_write(rpl([H|_T], S), LooptyDoops) :-
    rpl_var(H, _), !,
    has_dom(H, P, _N),
    assert_must_have_param(H),
    %assert_must_be_expanding(H),
    assert_must_have_params(S),
    foreach(member(I, LooptyDoops), assert_expanding_cycles(P, I)).

simplify_recursive_write(rpl(_R, _S), _LooptyDoops). % no-op

%assert_expanding_cycles([], invokes(_Name, _Subs)) :- !.
assert_expanding_cycles(_Params, invokes(_Name, [])) :- !.
assert_expanding_cycles(_Params, invokes(_Name, [H|[]])) :- !,
    assert_must_have_params(H),
    assert_cannot_have_star(H).

assert_expanding_cycles(Params, invokes(Name, [H|T])) :- !,
    assert_must_have_params(H),
    assert_cannot_have_star(H),
    assert_expanding_cycles(Params, invokes(Name, T)).

assert_must_have_param(X) :-
    head_rpl_var(X, _)
    -> ((must_have_param(X), !;
         assertz(must_have_param(X)),
         write('Asserted '), print(must_have_param(X)), nl))
        ,
        ((has_value(X, [Y]), head_rpl_var(Y, _))
         -> assert_must_have_param(Y)
         ; true
        )
     ;  true.

assert_must_be_expanding(X) :-
    head_rpl_var(X, _)
    -> ((must_be_expanding(X), !;
         assertz(must_be_expanding(X)),
         write('Asserted '), print(must_be_expanding(X)), nl))
        ,
        ((has_value(X, [Y]), head_rpl_var(Y, _))
         -> assert_must_be_expanding(Y)
         ; true
        )
     ;  true.

assert_must_have_params([]) :- !.
assert_must_have_params([H|T]) :- !,
    assert_must_have_params(H),
    assert_must_have_params(T).
assert_must_have_params(subst_set([S])) :- !,
    assert_must_have_params(S).
assert_must_have_params(subst_set(_S)) :- !,
    % Only one of the chains must have a param. Because we cannot yet express
    % this OR constraint, we just don't assert anything
    %assert_must_have_params(S).
    true.
assert_must_have_params(param_sub(_P, rpl([H|_T], S))) :-
    assert_must_have_param(H),
    assert_must_have_params(S).

assert_cannot_have_star([]) :- !.
assert_cannot_have_star([H|T]) :- !,
    assert_cannot_have_star(H),
    assert_cannot_have_star(T).
assert_cannot_have_star(subst_set(S)) :- !,
    assert_cannot_have_star(S).
assert_cannot_have_star(param_sub(_P, rpl([H|_T], []))) :- !,
    assert_cannot_have_star(H).
assert_cannot_have_star(param_sub(_P, rpl(_R, S))) :- !,
    assert_cannot_start_with_star(S).

assert_cannot_have_star(X) :-
    rpl_var(X, _)
    -> (( cannot_have_star(X), !;
         assertz(cannot_have_star(X)),
         write('Asserted '), print(cannot_have_star(X)), nl, !)
        ,
        ((has_value(X, [Y]), head_rpl_var(Y, _))
         -> assert_cannot_have_star(Y)
         ; true
        )
        %,
        %(must_be_expanding(X), !;
        % assertz(must_be_expanding(X)),
        % write('Asserted '), print(must_be_expanding(X)), nl)
       )
     ; true.

assert_cannot_start_with_star([]) :- !.
assert_cannot_start_with_star([H|[]]) :- !,
    assert_cannot_have_star(H).
assert_cannot_start_with_star([_H|T]) :-
    assert_cannot_start_with_star(T).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       SUBSTITUTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% apply_rec_sub(+ELin:effect_list,
%%               +RecSubs:list_of_lists_of_substitutions,
%%               -ELout:effect_list)
% apply each recursive (summarized) substitution vectors (lists) to  ELin and
% merge the results with ELin to procuce ELout
apply_rec_sub(EL, [], EL) :- !.
apply_rec_sub(ELin, [H|T], ELout) :-
    apply_sub(ELin, H, EL1),
    apply_rec_sub(ELin, T, EL2),
    merge_el(EL1, EL2, ELout).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       DEBUG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
print_rvs :-
    foreach((has_value(RV, Val), head_rpl_var(RV, _D)),
            (print(has_value(RV, Val)), nl)).

print_rvs2 :-
    foreach((head_rpl_var(RV, _D), has_value(RV, Val)),
            (print(has_value(RV, Val)), nl)).

print_rvs(C) :-
    gather_rpl_vars(C, [], RplVL),
    foreach((member(RV, RplVL), has_value(RV, Val)),
            (print(has_value(RV, Val)), nl)).
print_doms :-
    foreach((head_rpl_var(RV, _D), has_dom(RV, P, N)),
            (print(has_dom(RV, P, N)), nl)).

print_evs_p :-
    foreach((partially_inferred_es(EV, ES)),
            (print(partially_inferred_es(EV, ES)), nl)).

print_evs :-
    foreach((esi_constraint(_C, _LHS, effect_var(EV)), has_value(EV, ES)),
            (print(has_value(EV, ES)), nl)).

print_has_rvs :-
    foreach(has_rpl_vars(C, Vars),
            (print(has_rpl_vars(C, Vars)), nl)).

print_has_val :-
    foreach(has_value(X, Y),
            (print(has_value(X, Y)), nl)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       CONSTRAINTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

instantiate_and_check_phase :-
    findall(C1, ri_constraint(C1, _, _), RIL),
    findall(C2, esi_constraint(C2, _, _), ESIL),
    findall(C3, eni_constraint(C3, _, _), NIL),
    %findall(RV, head_rpl_var(RV, _D), StartingRplVars), % find all the RPL Vars
    %gather_rpl_vars(RIL, [], RILVars),

    %minus_sets(StartingRplVars, RILVars, RestVars),
    %instantiate_rpl_vars(RestVars),
    %reverse_list(ESIL, ESILR),
    %ESILR = ESIL,

    % sort NIL by increasing number of RPL vars
    map_list_to_pairs(var_num, NIL, NILwLen),
    keysort(NILwLen, SortedNILwLen),
    pairs_values(SortedNILwLen, SortedNIL),
    check_eni_constraints(SortedNIL, RIL, RILRest),
    check_constraints(RILRest),
    %check_constraints(NIL),
    check_constraints(ESIL),
    %print_has_val,
    true.

%% var_num(+C:constraint, -N:int)
% N is the number of rpl variables involved in constraint C
var_num(C, N) :-
    has_rpl_vars(C, RVL),
    length(RVL, N).

%%add_rpl_var_to_subconstraints(+RV:rpl_var, +CLadd:pair_l<constraint_id, quad_ord_set>
add_rpl_var_to_subconstraints(RV, CLadd):-
    is_in_subconstraint(RV, CLold), !,
    ord_pcqs_union(CLold, CLadd, CLnew),
    retract(is_in_subconstraint(RV, CLold)),
    asserta(is_in_subconstraint(RV, CLnew)).
add_rpl_var_to_subconstraints(RV, CLadd):-
    % \+is_in_subconstraint(RV, CLold), !,
    asserta(is_in_subconstraint(RV, CLadd)).

%% add_rpl_vars_to_subconstraint(+RVars:rpl_var_l, +C:unrolled_eni_constraint_id, +QS:quad_ord_set),
add_rpl_vars_to_subconstraint([], _C, _QS) :- !.
add_rpl_vars_to_subconstraint([H|T], C, QS) :-
    add_rpl_var_to_subconstraint(H, C, QS),
    add_rpl_vars_to_subconstraint(T, C, QS).

%% add_rpl_var_to_subconstraint(+RV:rpl_var, +C:constraint_id, +QS:quad_ord_set) :-
add_rpl_var_to_subconstraint(RV, C, QS) :-
    is_in_subconstraint(RV, CL), !,
    %(qsp_contains(CL, C, CP, CLrest)
    % -> (pairs_keys_values([CP], [C], [CV]),
    %     ord_qs_union(QS, CV, CVnew),
    %     pairs_keys_values(CPnew, [C], [CVnew]))
    % ; (CLrest = CL, pairs_keys_values(CPnew, [C], [QS]))
    %),
    %append(CPnew, CLrest, CLnew),
    P = C-QS,
    add_p_qsp(P, CL, CLnew),
    retract(is_in_subconstraint(RV, CL)),
    asserta(is_in_subconstraint(RV, CLnew)).

add_rpl_var_to_subconstraint(RV, C, QS) :-
    % \+ is_in_subconstraint(RV, CL)
    pairs_keys_values(CP, [C], [QS]),
    assertz(is_in_subconstraint(RV, CP)).

%% add_p_qsp(+P:pair<constraint_id, ord_quad_set>, +S:pair_l<...>, -Sout:pair_l<...>)
add_p_qsp(P, S, Sout) :-
    P = C-QS,
    (qsp_contains(S, C, CP, Srest)
     -> (pairs_keys_values([CP], [C], [CV]),
         ord_qs_union(QS, CV, CVnew),
         pairs_keys_values(CPnew, [C], [CVnew]))
     ; (Srest = S, CPnew = [P])
    ),
    append(CPnew, Srest, Sout).

%% qsp_contains(+CL:pair_l<constraint_id, quad_ord_set>, +C:constraint_id, -CP:pair<constraint_id, quad_ord_set>, -CLrest:pair_l<constraint_id, quad_ord_set>)
qsp_contains([H|T], C, H, T) :-
    pairs_keys_values([H], [C], [_V]).
qsp_contains([H|T], C, CP, [H|T2]) :-
    qsp_contains(T, C, CP, T2).

%% make_ord_qs(+Q:int, +S:ord_set, -QS:quad_ord_set) :-
make_ord_qs(Q, S, QS) :-
    is_ordset(S),
    make_ord_qs_(Q, S, QS).

make_ord_qs_(0, S, [S, [], [], []]).
make_ord_qs_(1, S, [[], S, [], []]).
make_ord_qs_(2, S, [[], [], S, []]).
make_ord_qs_(3, S, [[], [], [], S]).

is_ord_qs([X, Y, Z, W]) :-
    is_ordset(X),
    is_ordset(Y),
    is_ordset(Z),
    is_ordset(W).

%% ord_qs_union(+S1, +S2, -S3)
ord_qs_union([X1, X2, X3, X4], [Y1,Y2, Y3, Y4], [Z1, Z2, Z3, Z4]) :-
    ord_union(X1, Y1, Z1),
    ord_union(X2, Y2, Z2),
    ord_union(X3, Y3, Z3),
    ord_union(X4, Y4, Z4).

%% ord_pcqs_union(+S1:pair_l<constraint_id, quad_ord_set>, +S2:pair_l<...>, +So:pair_l<...>)
ord_pcqs_union([], S, S) :- !.
ord_pcqs_union(S, [], S) :- !.
ord_pcqs_union([H|T], S, Sout) :-
    add_p_qsp(H, S, S1),
    ord_pcqs_union(T, S1, Sout).

%% assert_is_in_constraint(+V:rpl_var_l/rpl_var, +C:constraint/constraint_ord_set)
assert_is_in_constraint([], _C) :- !.
assert_is_in_constraint([H|T], C) :- !,
    assert_is_in_constraint(H, C),
    assert_is_in_constraint(T, C).

assert_is_in_constraint(V, C) :-
    is_in_constraint(V, CL)
    -> ( (is_list(C) -> (list_to_ord_set(C, Cord), ord_union(Cord, CL, CLnew))
                      ; ord_union([C], CL, CLnew)),
         retract(is_in_constraint(V, CL)),
         assertz(is_in_constraint(V, CLnew)),
         true)
%	     write('Asserted '), print(is_in_constraint(V, CLnew)), nl)
    ;  ( (is_list(C) -> (list_to_ord_set(C, Cord), assertz(is_in_constraint(V, Cord)))
                      ;  assertz(is_in_constraint(V, [C]))),
         true).
%	     write('Asserted '), print(is_in_constraint(V, [C])), nl).


%% add_has_rpl_var(+RV:rpl_var, +CL:constraint_l)
add_has_rpl_var(_V, []) :- !.
add_has_rpl_var(V, [H|T]) :- !,
    add_has_rpl_var(V, H),
    add_has_rpl_var(V, T).
add_has_rpl_var(V, C) :-
    \+is_list(V), % just making sure V is not a list
    has_rpl_vars(C, RVL)
    -> (ord_union([V], RVL, RVLnew),
        retract(has_rpl_vars(C, RVL)),
        assertz(has_rpl_vars(C, RVLnew)))
    ; (assertz(has_rpl_vars(C, [V]))).

clear_all :-
    foreach(has_value(X, Y), retract(has_value(X, Y))).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       Constraint Checking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% gather_instantiate_rpl_vars(+ExpIn:? -RVLinited:rpl_var_l)
%  List of RplVars that were actually initialized
gather_instantiate_rpl_vars(Exp, RVLinited) :-
    gather_rpl_vars(Exp, [], RVL), !,
    %compute_arity(RVL, A1),
    %write('Gather_Instantiate Space Size = '), print(A1), nl,
    %asserta(has_value(space_size, A1)),
    instantiate_rpl_vars(RVL, RVLinited).
    %(has_value(space_size, X)
    %-> ( Y is X - 1,
    %     once(retract(has_value(space_size, X))),
    %     asserta(has_value(space_size, Y)),
    %     print(has_value(space_size, Y)), nl
    %   )
    %; true),
    % true.

%% gather_infer_es_vars(+Exp:?)
gather_infer_es_vars(Exp) :-
    gather_es_vars(Exp, [], EVL), !,
    infer_es_vars(EVL, EVLinferred), !,
    backtrack_retract_es_vars(EVLinferred).

backtrack_retract_es_vars([H|T]) :-
    backtrack_retract_es_var(H),
    backtrack_retract_es_vars(T).
backtrack_retract_es_vars([]).

backtrack_retract_es_var(_EV).
backtrack_retract_es_var(EV) :-
    ignore(retract(has_value(EV, _))),
    fail.

%% gather_rpl_vars(+Exp:?, +Names:string_l -RVL:rpl_var_ord_set)
% @param Names is a list of method calls followed so far.
% Its purpose it so break recursive cycles.
gather_rpl_vars([], _Names, []) :- !.

gather_rpl_vars([H|T], Names, RVL) :- !,
    gather_rpl_vars(H, Names, RVLH),
    gather_rpl_vars(T, Names, RVLT),
    ord_union(RVLH, RVLT, RVL).

gather_rpl_vars(subst_set([]), _Names, []) :- !.
gather_rpl_vars(subst_set(S), Names, RVL) :- !,
    gather_rpl_vars(S, Names, RVL).

gather_rpl_vars(rpl(R, S), Names, RVL) :- !,
    gather_rpl_vars(R, Names, RVLR),
    gather_rpl_vars(S, Names, RVLS),
    ord_union(RVLR, RVLS, RVL).

gather_rpl_vars(param_sub(_P, RplSub), Names, RVL) :- !,
    gather_rpl_vars(RplSub, Names, RVL).

gather_rpl_vars(pure, _Names, []) :- !.

gather_rpl_vars(invokes(Method, Subs), Names, RVL) :- !,
    gather_rpl_vars(Subs, Names, RVLS),
    has_effect_summary(Method, ES),
    ( memberchk(Method, Names)
      -> RVLES = []
      ;  gather_rpl_vars(ES, [Method|Names], RVLES)
    ),
    ord_union(RVLS, RVLES, RVL).

gather_rpl_vars(effect_summary(SE, CE), Names, RVL) :- !,
    gather_rpl_vars(SE, Names, RVL1),
    gather_rpl_vars(CE, Names, RVL2),
    ord_union(RVL1, RVL2, RVL).

gather_rpl_vars(effect_var(EV), Names, RVL) :- !,
    esi_constraint(C, LHS, effect_var(EV)),
    ( has_rpl_vars(C, Vars)
    -> RVL = Vars
    ; ( has_value(EV, ES)
      -> gather_rpl_vars(ES, Names, RVL)
      ;  gather_rpl_vars(LHS, Names, RVL)
      )
    ).

gather_rpl_vars(E, Names, RVL) :-
    get_effect_rpl(E, Rpl), !,
    gather_rpl_vars(Rpl, Names, RVL).

gather_rpl_vars(C, _Names, RVL) :-
    ri_constraint(C, _LHS, _RHS), !,
    has_rpl_vars(C, RVL).

gather_rpl_vars(C, _Names, RVL) :-
    esi_constraint(C, _LHS, _RHS), !,
    has_rpl_vars(C, RVL).

gather_rpl_vars(C, _Names, RVL) :-
    eni_constraint(C, _LHS, _RHS), !,
    has_rpl_vars(C, RVL).

gather_rpl_vars(C, _Names, RVL) :-
    eni_sub_constraint(C, _L, _R, _Cp), !,
    has_rpl_vars(C, RVL).

gather_rpl_vars(RV, _Names, RVars) :-
    rpl_var(RV, _)
    -> RVars = [RV]
    ;  RVars = [].

%% calculate_arity(-A:int)
%  @param N: size of the instantiation space reachable from the NIConstraints
calculate_arity(A) :-
    reachable_rpl_vars(RplVars1),
    compute_arity(RplVars1, A).

%% reachable_rpl_vars(-RPLsOut:rpl_var_l)
%  entry point for computing reachable rpl_vars
reachable_rpl_vars(RPLsOut) :-
    findall(C, eni_constraint(C, _, _), NIL),
    reachable_rpl_vars_(NIL, NIL, Cout),
    gather_rpl_vars(Cout, [], RPLsOut).

%% reachable_rpl_vars_(+Call:constraint_l, +Cdiff:constraint_l, -Cout:constraint_l)
%  implements the "while-loop" of the reachability fixpoint computation
%  @param Cin:  reachable constraints at the entry of the iteration
%  @param Cdiff:additional reachable constraints at the entry of the iteration since last iteration
%  @param Cout: reachable constraints at the exit of the iteration
%  The while-loop terminates when Cin == Cout.
%  Invatiant at entry: Cdiff \subseteq Cin
%  Invatiant at exit:  Cin   \subseteq Cout
reachable_rpl_vars_(Cin, Cdiff, Cout) :-
    gather_rpl_vars(Cdiff, [], RPLs),
    gather_involved_constraints(RPLs, Cs),
    ord_intersection(Cin, Cs, _Cintersection, Cmore),
    (ord_empty(Cmore)
    -> Cout = Cin % end of while loop
    ;  ( ord_union(Cin, Cmore, CinNext),
         gather_rpl_vars(CinNext, Cmore, Cout))
    ).

%% check_instantiated_constraints(+CL:constraint_l, -CLRest:constraint_l)
%  Return true if each fully instantiated constraint in CL is satisfied
check_instantiated_constraints([], []) :- !.
check_instantiated_constraints([H|T], Rest) :-
    is_instantiated_constraint(H), !,
    check_constraint(H), !,
    check_instantiated_constraints(T, Rest).
check_instantiated_constraints([H|T], [H|Rest]) :-
%   \+ is_instantiated(H), !,
    check_instantiated_constraints(T, Rest).

%% try_check_constraints(+CL:constraint_l)
try_check_constraints([]) :- !.
try_check_constraints([H|T]) :-
    try_check_constraint(H),
    try_check_constraints(T).

%% check_constraint(+C:constraint)
try_check_constraint(C) :-
    ri_constraint(C, LHS, RHS), !,
    push_trying,
    check_no_try,
    (is_included(LHS, RHS)
    -> (clear_try, pop_trying)
    ; (clear_try, pop_trying, fail)).

%try_check_constraint(C) :-
%    eni_sub_constraint(C, L, R, _Cp), !,
%    push_trying,
%    check_no_try,
%    (is_non_interfering(L, R)
%    -> (clear_try, pop_trying)
%    ; (clear_try, pop_trying, fail)).

try_check_constraint(_C). % ignore ESI and ENI constraints (ESI always succeed, ENI too complex)

%% try_check_subconstraints(+CQSPL:pair_l<constraint, quad_ord_set>).
try_check_subconstraints([]) :- !.
try_check_subconstraints([H|T]) :- !,
    try_check_subconstraints_p(H),
    try_check_subconstraints(T).

%try_check_subconstraints_p(+P:pair<constraint_id, quad_ord_set>)
try_check_subconstraints_p(P) :- !,
    P = C-[Q0, Q1, Q2, Q3],
    unrolled_eni_constraint(C, LHS_R, LHS_W, RHS_R, RHS_W),
    try_check_sub_eni(Q1, Q3, LHS_W, RHS_W),
    try_check_sub_eni(Q0, Q3, LHS_R, RHS_W),
    try_check_sub_eni(Q1, Q2, LHS_W, RHS_R).

%% try_check_sub_eni(+Q1:int_l, +Q2:int_l, +EL1:effect_l, +EL2:effect_l)
try_check_sub_eni(Q1, Q2, EL1, EL2) :- !, try_check_sub_eni_(0, Q1, Q2, EL1, EL2).

%% check_sub_eni_(+I1:int, +I2:int, +Q1:int_l, +Q2:int_l, +EL1:effect_l, +EL2:effect_l)
%  if idx1 in Q1 then check all of EL2,
%  else check only EL2 filtered by Q2
try_check_sub_eni_(_Idx, Q1, Q2, LHS_L, RHS_L) :-
    forall(nth0(N, LHS_L, LHS), (ord_memberchk(N, Q1)
                                 -> try_check_all_eni(LHS, RHS_L)
                                 ;  try_check_filtered_eni(LHS, 0, Q2, RHS_L))).

%try_check_sub_eni_(_, _, _, [], _) :- !.
%try_check_sub_eni_(_, _, _, _, []) :- !.
%try_check_sub_eni_(_, [], [], _, _) :- !.
%try_check_sub_eni_(Idx, [Idx|Ti], Q2, [He|Te], EL2) :-
%    !,
%    try_check_all_eni(He, EL2),
%    Inext is Idx+1,
%    try_check_sub_eni_(Inext, Ti, Q2, Te, EL2).
%try_check_sub_eni_(Idx, Q1, Q2, [He|Te], EL2) :- !,
%    try_check_filtered_eni(He, 0, Q2, EL2),
%    Inext is Idx+1,
%    try_check_sub_eni_(Inext, Q1, Q2, Te, EL2).

% TODO: use a foreach loop instead of recursion and test performance
%% try_check_all_eni(+E:effect, +EL:effect_l)
try_check_all_eni(E, EL) :-
    maplist(try_check_eni(E), EL).

%try_check_all_eni(E, EL) :-
%    forall(member(X, EL), try_check_eni(E, X)).

%try_check_all_eni(E, [H|T]) :-
%    try_check_eni(E, H),
%    try_check_all_eni(E, T).
%try_check_all_eni(_E, []).

%% try_check_filtered_eni(+E:effect, +Idx:int, +Q:int_l, +EL:effect_l)
try_check_filtered_eni(E, _Idx, IdxL, EL) :-
    forall((member(Idx, IdxL), nth0(Idx, EL, X)), try_check_eni(E, X)).

%try_check_filtered_eni(_E, _Idx, [], _EL) :- !.
%try_check_filtered_eni(E, Idx, [Idx|Ti], [H|T]) :- !,
%    try_check_eni(E, H),
%    Inext is Idx+1,
%    try_check_filtered_eni(E, Inext, Ti, T).
%try_check_filtered_eni(E, Idx, Q, [_H|T]) :-
%    Inext is Idx+1,
%    try_check_filtered_eni(E, Inext, Q, T).
%try_check_filtered_eni(_E, _Idx, _Q, []).

%% try_check_eni(+E1:effect, +E2:effect)
try_check_eni(E1, E2) :-
    push_trying,
    check_no_try,
    ( is_non_interfering(E1, E2)
    -> (clear_try, pop_trying)
    ; (clear_try, pop_trying, fail))
    , !.


%% check_constraints(+CL:constraint_l)
check_constraints([]) :- !.
check_constraints([H|T]) :-
    check_constraint(H),
    check_constraints(T).

%% check_constraint(+C:constraint)
check_constraint(C) :-
    ri_constraint(C, LHS, RHS), !,
    check_ri_constraint(C, LHS, RHS).

% NAIVE CHECKING
%check_constraint(C) :-
%    ri_constraint(C, LHS, RHS), !,
%    gather_instantiate_rpl_vars(C, _RVLinited),
%    is_included(LHS, RHS).

check_constraint(C) :-
    esi_constraint(C, LHS, RHS), !,
    gather_instantiate_rpl_vars(C, _RVLinited),
    gather_infer_es_vars(RHS),
    compute_el(LHS, LEL),
    compute_el(RHS, REL),
    covers(REL, LEL).

check_constraint(C) :-
    eni_sub_constraint(C, L, R, _Cp), !,
    gather_instantiate_rpl_vars(C, _RVLinited),
    is_non_interfering(L, R).

% STRUCTURE & VALUE DRIVEN CHECKING
%%check_ri_constraint(C, LHS, RHS) :-
check_ri_constraint(_C, LHS, RHS) :-
    instantiate_rpl(LHS, _LHS_RVs),
    instantiate_rpl(RHS, _RHS_RVs),
    is_included(LHS, RHS).

%%instantiate_effect(+E:effect, -RVL:rpl_var_l)
instantiate_effect(E, RVL) :-
    get_effect_rpl(E, R),
    instantiate_rpl(R, RVL).

%%instantiate_rpl(+R:rpl, -RVL:rpl_var_l)
instantiate_rpl(rpl(R, []), RVL) :- !,
    instantiate_l(R, RVL).
instantiate_rpl(rpl(R, S), RVL) :-
    % S \= []
    instantiate_l(R, RVL_R),
    apply_sub(rpl(R, []), S, Ro),
    instantiate_rpl(Ro, RVL_S),
    ord_union(RVL_R, RVL_S, RVL).

%% instantiate_l(+L:rpl_elmt_l, -RVL:rpl_var_l)
instantiate_l([H|_T], RVL) :-
    rpl_var(H, _Dom)
    -> instantiate_rpl_var(H, RVL)
    ;  RVL = [].
instantiate_l([], []).

%% check_eni_constraints(+ENIL:eni_constraint_l, +RIL:ri_constraint_l, -RILRest:ri_constraint_l)
check_eni_constraints([], RIL, RIL) :- !.
check_eni_constraints([H|T], RIL, RILRest) :-
    check_eni_constraint(H, RIL, RILRest1),
    check_eni_constraints(T, RILRest1, RILRest).

check_eni_constraint_old(C, RIL, RILRest) :-
    eni_constraint(C, LHS, RHS), !,
    % phase 1: check related RILs and sub-eni-constraints
    has_subconstraints(C, Cww, Crw),
    find_ris_w_common_rvs(C, RIL, RILwAllCom, RILwSomeCom, RILRest),
    %sort_by_size(RILwAllCom, RILAllComIncr),

    % OPTION 1
    check_ris_of_eni(RILwAllCom, Cww, Crw, CwwO1, CrwO1),
    check_ris_of_eni(RILwSomeCom, CwwO1, CrwO1, CwwO2, CrwO2), %% TODO: convert to try_check where only vars in common w. ENI constraint are instantiated
    check_constraints(CwwO2),
    check_constraints(CrwO2),

    % OPTION 2
    %check_constraints(Cww),
    %check_constraints(Crw),
    %check_constraints(RILwAllCom),
    %check_constraints(RILwSomeCom),

    % phase 2: check eni-constraint by inferring effect summaries.
    gather_instantiate_rpl_vars(C, _RVLinited),
    gather_infer_es_vars(C),
    % check the non-interference constraint
    compute_es(LHS, ESL),
    compute_es(RHS, ESR),
    is_non_interfering(ESL, ESR).

check_eni_constraint(C, RIL, RILRest) :-
    eni_constraint(C, LHS, RHS), !,
    % phase 1: check related RILs and sub-eni-constraints
    find_ris_w_common_rvs(C, RIL, RILwAllCom, RILwSomeCom, RILRest),
    %sort_by_size(RILwAllCom, RILAllComIncr),

    % OPTION 2
    check_constraints(RILwAllCom),
    check_constraints(RILwSomeCom),
    check_subconstraints(C),

    % phase 2: check eni-constraint by inferring effect summaries.
    gather_instantiate_rpl_vars(C, _RVLinited),
    gather_infer_es_vars(C),
    % check the non-interference constraint
    compute_es(LHS, ESL),
    compute_es(RHS, ESR),
    is_non_interfering(ESL, ESR).

check_subconstraints(C) :-
    unrolled_eni_constraint(C, LHS_R, LHS_W, RHS_R, RHS_W), !,
    check_eni_sub(LHS_W, RHS_W),
    check_eni_sub(LHS_W, RHS_R),
    check_eni_sub(LHS_R, RHS_W).

 %% check_eni_sub(+EL1:effect_l, +EL2:effect_l)
 check_eni_sub([H|T], EL) :-
    instantiate_effect(H, _),           % STRUCTURE & VALUE DRIVEN CHECKING
    % gather_instantiate_rpl_vars(H, _) % NAIVE CHECKING
    check_eni_sub_el(H, EL),
    check_eni_sub(T, EL).
check_eni_sub([], _EL).

check_eni_sub_el(E, [H|T]) :-
    check_eni_sub_ee(E, H),
    check_eni_sub_el(E, T).
check_eni_sub_el(_E, []).

check_eni_sub_ee(E1, E2) :-
    %gather_instantiate_rpl_vars(E1) : This is already done in check_eni_sub
    instantiate_effect(E2, _),           % STRUCTURE & VALUE DRIVEN CHECKING
    % gather_instantiate_rpl_vars(E2, _) % NAIVE CHECKING
    is_non_interfering(E1, E2).

%% check_ris_of_eni(+RIs:ri_constraint_l, +Cww:ww_eni_subconstraint_l, +Crw:rw_eni_subconsraint_l, -Cww:ww_eni_subconstraint_l, -Crw:rw_eni_subconstraint_l)
%  Checks the RI constraints and at the same time any of the Cww and Crw constraints that become fully instantiated.
%  If a fully instantiated constraint is successfully checked it is removed from the constraints to check (CwwO and CrwO)
check_ris_of_eni([], Cww, Crw, Cww, Crw) :- !.
check_ris_of_eni(RIL, Cww, Crw, CwwO, CrwO) :-
    %TODO: select_heuristic(RIL, RIC, RILRest), !,
    select(RIC, RIL, RILRest), !,
    check_ri_of_eni(RIC, Cww, Crw, CwwO1, CrwO1),
    check_ris_of_eni(RILRest, CwwO1, CrwO1, CwwO, CrwO).

check_ri_of_eni(RIC, Cww, Crw, Cww, Crw) :-
    check_constraint(RIC).

%% find_ris_w_common_rvs(+C:constraint , +RIL:ri_constraint_l , -RILwAllCom:ri_constraint_l , -RILwSomeCom:ri_constraint_l , -RILRest:ri_constraint_l)
%  Returns a list of ri_constraints that have all their RPL vars in common with the eni constraint C,
%          a list of ri_constraints that has some of their RPL vars in common with the eni constraint C,
%          and the rest of the ri_constraints (which have no RPL vars in common with the eni constraint C).
find_ris_w_common_rvs(_C, [], [], [], []) :- !.
find_ris_w_common_rvs(C, [H|T], [H|RILwAllComT], RILwSomeComT, RILRestT) :-
    have_all_common_rvs(C, H), !,
    find_ris_w_common_rvs(C, T, RILwAllComT, RILwSomeComT, RILRestT).
find_ris_w_common_rvs(C, [H|T], RILwAllComT, [H|RILwSomeComT], RILRestT) :-
    have_some_common_rvs(C, H), !,
    find_ris_w_common_rvs(C, T, RILwAllComT, RILwSomeComT, RILRestT).
find_ris_w_common_rvs(C, [H|T], RILwAllComT, RILwSomeComT, [H|RILRestT]) :-
%    \+ have_common_rvs(C, H), !,
    find_ris_w_common_rvs(C, T, RILwAllComT, RILwSomeComT, RILRestT).

have_all_common_rvs(C1, C2) :-
    get_intersect_constraints(C1, C2, Intersect, _IntLen, S2mS1, _S2mS1Len),
    Intersect \= [],
    S2mS1 = [].

have_some_common_rvs(C1, C2) :-
    get_intersect_constraints(C1, C2, Intersect, _IntLen, S2mS1, _S2mS1Len),
    Intersect \= [],
    S2mS1 \= [].

get_intersect_constraints(C1, C2, Intersect, IntLen, S2mS1, S2mS1Len) :-
    intersect_constraints(C1, C2, Intersect, IntLen, S2mS1, S2mS1Len), !.
get_intersect_constraints(C1, C2, Intersect, IntLen, S2mS1, S2mS1Len) :-
    compute_intersect_constraints(C1, C2, Intersect, IntLen, S2mS1, S2mS1Len).

compute_intersect_constraints(C1, C2, Intersect, IntLen, S2mS1, S2mS1Len) :-
    has_rpl_vars(C1, RVs1),
    has_rpl_vars(C2, RVs2),
    intersect_minus_sets(RVs1, RVs2, Intersect, S2mS1),
    length(Intersect, IntLen),
    length(S2mS1, S2mS1Len),
    assertz(intersect_constraints(C1, C2, Intersect, IntLen, S2mS1, S2mS1Len)).

is_instantiated_constraint(C) :-
    has_rpl_vars(C, RVL),
    is_instantiated_rv_list(RVL).

is_instantiated_rv_list([]) :- !.
is_instantiated_rv_list([H|T]) :-
    is_instantiated_rv(H),
    is_instantiated_rv_list(T).

is_instantiated_rv(RV) :-
    has_value(RV, V),
    (([RV2] = V, head_rpl_var(RV2, _D))
     -> is_instantiated_rv(V)
     ; true).

%% try_check_involved_constraints(+RVL:rpl_var_l).
%  Given the already instantiated rpl_vars, check all the constraints in which
%  the provided list of RPL variables is involved
try_check_involved_constraints([]) :- !.
try_check_involved_constraints(RVL) :-
    gather_involved_constraints(RVL, CL),
    try_check_constraints(CL),
    gather_involved_subconstraints(RVL, CQSPL),
    try_check_subconstraints(CQSPL).

%% gather_involved_subconstraints(+RVL:rpl_var_l, -CPL:pair_l<constraint, quad_ord_set>)
gather_involved_subconstraints([], []) :- !.
gather_involved_subconstraints([H|T], CL) :-
    (is_in_subconstraint(H, CLH)
    -> true
    ; CLH = []),
    gather_involved_subconstraints(T, CLT),
    ord_pcqs_union(CLH, CLT, CL).

%% gather_involved_constraints(+RVL:rpl_var_l, -CL:constraint_ord_set).
gather_involved_constraints([], []) :- !.
gather_involved_constraints([H|T], CL) :-
    gather_involved_constraints(T, T_CL),
    ( is_in_constraint(H, H_CL)
      -> true
      ; H_CL = []),
    ord_union(H_CL, T_CL, CL).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PHASE 3: Simplify!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% generate sub-constraints for a list of ENI Constraints CL
gen_eni_subc_cl([H|T]) :-
    gen_eni_subc(H),
    gen_eni_subc_cl(T).
gen_eni_subc_cl([]).
%%%%%%%%%%%%%%%%%%
%% gen_eni_subc(+C:eni_constraint)
% generate sub-constraints for ENI Constraint C
gen_eni_subc(C) :-
    eni_constraint(C, L, R),
    % 1st phase: recursion unrolling
    unroll_es(L, effect_summary(LHS_SE, [])),
    unroll_es(R, effect_summary(RHS_SE, [])),

    findall(R1, (member(R1, LHS_SE), R1 = reads(_)), LHS_R),  % gather LHS read effects
    findall(W1, (member(W1, LHS_SE), W1 = writes(_)), LHS_W), % gather LHS write effects
    findall(R2, (member(R2, RHS_SE), R2 = reads(_)), RHS_R),  % gather RHS read effects
    findall(W2, (member(W2, RHS_SE), W2 = writes(_)), RHS_W), % gather RHS write effects

    % START DEBUG
    length(LHS_R, LHS_R_L),
    length(LHS_W, LHS_W_L),
    length(RHS_R, RHS_R_L),
    length(RHS_W, RHS_W_L),
    write('LHS_R = '), print(LHS_R_L), nl,
    write('LHS_W = '), print(LHS_W_L), nl,
    write('RHS_R = '), print(RHS_R_L), nl,
    write('RHS_W = '), print(RHS_W_L), nl, nl,
    % END DEBUG

    assertz(unrolled_eni_constraint(C, LHS_R, LHS_W, RHS_R, RHS_W)),
    compute_unrolled_eni_vars(C).

    %gen_eni_subc_se(LHS_W, RHS_W, C, Cww),
    %gen_eni_subc_se(LHS_W, RHS_R, C, Cwr),
    %gen_eni_subc_se(RHS_W, LHS_R, C, Crw),
    %append(Cwr, Crw, CRW),
    %assertz(has_subconstraints(C, Cww, CRW)).

%% gen_eni_subc_se(+L:simple_effect_l, +R:simple_effect_l, +C:constraint, -Cgen:constraint)
%  Cgen is the list with the generated sub-constraints
gen_eni_subc_se([], _R, _C, []) :- !.
gen_eni_subc_se(_L, [], _C, []) :- !.
gen_eni_subc_se([H|T], R, C, Cgen) :-
    gen_eni_subc_ese(H, R, C, CgenH),
    gen_eni_subc_se(T, R, C, CgenT),
    append(CgenH, CgenT, Cgen). % CgenH could be empty

%% gen_eni_subc_ese(+E:effect, +L:simple_effect_l, +C:constraint, -Cgen:constraints_l)
% Cgen is the list of generated sub constraints
gen_eni_subc_ese(_E, [], _C, []) :- !.
gen_eni_subc_ese(E, [H|T], C, Cgen) :-
    gen_eni_subc_ee(E, H, C, CgenH),
    gen_eni_subc_ese(E, T, C, CgenT),
    append(CgenH, CgenT, Cgen). % CgenH could be empty

%% gen_eni_subc_ee(+E1:effect, +E2:effect, +C:constraint, -Cgen:constraint)
%  Cgen is the generated sub constraint
%  TODO: check that sub_eni is not trivially SAT or trivially UNSAT
gen_eni_subc_ee(E1, E2, C, [C0]) :-
    gen_next_eni_subc_id(C0),
    assertz(eni_sub_constraint(C0, E1, E2, C)),
    compute_subeni_vars(C0).

%% gen_next_eni_subc_id(-C:constraint_id)
gen_next_eni_subc_id(C) :-
    gensym(subconstraint_, C).

%%%%%%%%%%%%%%%%%%
compute_ri_rpl_vars(C) :-
    ri_constraint(C, L, R),
    gather_rpl_vars(L, [], LVars),
    gather_rpl_vars(R, [], RVars),
    ord_union(LVars, RVars, All),
    assertz(has_rpl_vars(C, All)),
    assert_is_in_constraint(All, C).

compute_esi_vars(C) :-
    esi_constraint(C, L, R),
    gather_rpl_vars(L, [], LVars),
    assertz(has_rpl_vars(C, LVars)),
    assert_is_in_constraint(LVars, C),
    has_effect_summary(Name, R),
    gather_es_vars(R, [Name], ESL),
    assertz(has_es_vars(C, ESL)).

compute_eni_vars(C) :-
    eni_constraint(C, L, R),
    gather_rpl_vars(L, [], LVars),
    gather_rpl_vars(R, [], RVars),
    ord_union(LVars, RVars, All),
    assertz(has_rpl_vars(C, All)),
    assert_is_in_constraint(All, C),
    gather_es_vars(L, [], ESLVars),
    gather_es_vars(R, [], ESRVars),
    ord_union(ESLVars, ESRVars, ESVars),
    assertz(has_es_vars(C, ESVars)).

compute_subeni_vars(C) :-
    eni_sub_constraint(C, L, R, _Cp),
    gather_rpl_vars(L, [], LVars),
    gather_rpl_vars(R, [], RVars),
    ord_union(LVars, RVars, All),
    assertz(has_rpl_vars(C, All)),
    assert_is_in_constraint(All, C).

%% compute_unrolled_eni_vars(+C:unrolled_eni_constraint_id) :-
compute_unrolled_eni_vars(C) :-
    unrolled_eni_constraint(C, LHS_R, LHS_W, RHS_R, RHS_W),
    compute_unrolled_eni_vars_quadrant(C, 0, LHS_R, 0),
    compute_unrolled_eni_vars_quadrant(C, 1, LHS_W, 0),
    compute_unrolled_eni_vars_quadrant(C, 2, RHS_R, 0),
    compute_unrolled_eni_vars_quadrant(C, 3, RHS_W, 0).

%% compute_unrolled_eni_vars_quadrant(+C:unrolled_eni_constraint_id, +Quad:int, +EL:effect_l, +Idx:int)
compute_unrolled_eni_vars_quadrant(_, _, [], _) :- !.
compute_unrolled_eni_vars_quadrant(C, Q, [H|T], Idx) :-
    gather_rpl_vars(H, [], RVars),
    make_ord_qs(Q, [Idx], QS),
    add_rpl_vars_to_subconstraint(RVars, C, QS),
    NextIdx is Idx+1,
    compute_unrolled_eni_vars_quadrant(C, Q, T, NextIdx).

% For each var in the list, add constraint C to its is_in_constraint list
evaluate_all :-
     findall(Var, has_value(Var, _), Vars),
     evaluate_vars(Vars).

evaluate_vars([]).
evaluate_vars([H|T]) :-
    evaluate_var(H),
    evaluate_vars(T).

evaluate_var(V)	:-
    has_value(V, Val),
    substitute_rpl_vars(Val, ValZ),
    ( Val = ValZ
    -> true
    ; (retract(has_value(V, Val)),
       assertz(has_value(V, ValZ))
      )
    ).

