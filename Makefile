INSTALL_DIR = /opt/lib
PROLOG_SRCS = asap/asap.pl\
              asap/util.pl\
              asap/rpl.pl\
              asap/effect.pl\
              asap/effect_inference.pl\
              asap/load-asap.pl
MD = mkdir -p

INSTALL_DIR:
	$(MD) $(INSTALL_DIR)

.PHONY:install
install:asap/asap.pl INSTALL_DIR
	install -v --backup=numbered  -t $(INSTALL_DIR)  $(PROLOG_SRCS)	
