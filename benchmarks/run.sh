#!/bin/bash

# USAGE: All arguments are needed, see below
# ARG1: Number of executions
# ARG2: Log File Name
# ARG3: Source file
# ARG4: Optimization level (one of 0, 1, 2, 3) 
NExec=5
LOGFILE="runs.log"

NExec=${1}
LOGFILE=${2}
SRC=${3}
OPT=${4}

rm -f ${LOGFILE}
for i in `seq 1 ${NExec}`; do 
   clang++ --analyze -Xclang -analyzer-checker=alpha.SafeParallelismChecker -Xclang -analyzer-config -Xclang -asap-default-scheme=inference -Xclang -verify   -std=c++11 -stdlib=libc++  -DASAP_INFER_EFFECTS -DASAP_INFER_ARGS  ${SRC} -Xclang -analyzer-config -Xclang -asap-simplify-level=${OPT} 1>>${LOGFILE} 2>&1
done

