#line 2 "mt19937ar.h"
/* =============================================================================
 *
 * mt19937ar.h
 * -- MT19937 random number generator
 *
 * =============================================================================
 *
 * A C-program for MT19937, with initialization improved 2002/1/26.
 * Coded by Takuji Nishimura and Makoto Matsumoto.
 *
 * Before using, initialize the state by using init_genrand(seed)
 * or init_by_array(init_key, key_length).
 *
 * Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *  3. The names of its contributors may not be used to endorse or promote
 *     products derived from this software without specific prior written
 *     permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * Any feedback is very welcome.
 * http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
 * email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
 *
 * =============================================================================
 *
 * Modified February 2006 by Chi Cao Minh
 *
 * - Changed functions to take 'mt' and 'mti' as arguments so can have
 *   private per-thread random number generators.
 *
 * =============================================================================
 *
 * For the license of bayes/sort.h and bayes/sort.c, please see the header
 * of the files.
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of kmeans, please see kmeans/LICENSE.kmeans
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of ssca2, please see ssca2/COPYRIGHT
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/mt19937ar.c and lib/mt19937ar.h, please see the
 * header of the files.
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/rbtree.h and lib/rbtree.c, please see
 * lib/LEGALNOTICE.rbtree and lib/LICENSE.rbtree
 * 
 * ------------------------------------------------------------------------
 * 
 * Unless otherwise noted, the following license applies to STAMP files:
 * 
 * Copyright (c) 2007, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 * 
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 * 
 *     * Neither the name of Stanford University nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY STANFORD UNIVERSITY ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 * =============================================================================
 */


#ifndef MT19937AR_H
#define MT19937AR_H 1

#include "asap.h"
#include <stdio.h>
#line 125


/* Period parameters */
#define N_MT19937AR 624
#define M_MT19937AR 397
#define MATRIX_A 0x9908b0dfUL   /* constant vector a */
#define UPPER_MASK 0x80000000UL /* most significant w-r bits */
#define LOWER_MASK 0x7fffffffUL /* least significant r bits */

#if 0
static unsigned long mt[N_MT19937AR]; /* the array for the state vector  */
static long mti=N_MT19937AR+1; /* mti==N+1 means mt[N_MT19937AR] is not initialized */
#endif

/* initializes mt[N] with a seed */
void init_genrand WRITES(Global) (unsigned long mt[], unsigned long * mtiPtr, unsigned long s); // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[rGLOBAL]}} expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[rSTAR]}}

/* initialize by an array with array-length */
/* init_key is the array for initializing keys */
/* key_length is its length */
/* slight change for C++, 2004/2/26 */
void init_by_array WRITES(Global) (unsigned long mt[], unsigned long * mtiPtr, unsigned long init_key[], long key_length); // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[r282_mt]}} // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[r283_mtiPtr]}}  expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[r284_init_key]}}

/* generates a random number on [0,0xffffffff]-interval */
unsigned long genrand_int32 WRITES(Global) (unsigned long mt[], unsigned long * mtiPtr); // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[rSTAR]}} // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[rSTAR]}}

/* generates a random number on [0,0x7fffffff]-interval */
long genrand_int31 WRITES(Global) (unsigned long mt[], unsigned long * mtiPtr); // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[r287_mt]}} // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[r288_mtiPtr]}}

/* generates a random number on [0,1]-real-interval */
double genrand_real1 WRITES(Global) (unsigned long mt[], unsigned long * mtiPtr); // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[r289_mt]}} // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[r290_mtiPtr]}}

/* generates a random number on [0,1)-real-interval */
double genrand_real2 WRITES(Global) (unsigned long mt[], unsigned long * mtiPtr); // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[r291_mt]}} // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[r292_mtiPtr]}}
/* generates a random number on (0,1)-real-interval */
double genrand_real3 WRITES(Global) (unsigned long mt[], unsigned long * mtiPtr); // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[r293_mt]}} // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[r294_mtiPtr]}}

/* generates a random number on [0,1) with 53-bit resolution*/
double genrand_res53 WRITES(Global) (unsigned long mt[], unsigned long * mtiPtr); // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[r295_mt]}} // expected-warning{{Inferred region arguments: unsigned long *, IN:Local, ArgV:[r296_mtiPtr]}}


#endif /* MT19937AR_H */


/* =============================================================================
 *
 * End of mt19937ar.h
 *
 * =============================================================================
 */
