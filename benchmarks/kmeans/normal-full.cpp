#line 2 "normal.cpp"
/* =============================================================================
 *
 * normal.c
 * -- Implementation of normal k-means clustering algorithm
 *
 * =============================================================================
 *
 * Author:
 *
 * Wei-keng Liao
 * ECE Department, Northwestern University
 * email: wkliao@ece.northwestern.edu
 *
 *
 * Edited by:
 *
 * Jay Pisharath
 * Northwestern University.
 *
 * Chi Cao Minh
 * Stanford University
 *
 * =============================================================================
 *
 * For the license of bayes/sort.h and bayes/sort.c, please see the header
 * of the files.
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of kmeans, please see kmeans/LICENSE.kmeans
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of ssca2, please see ssca2/COPYRIGHT
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/mt19937ar.c and lib/mt19937ar.h, please see the
 * header of the files.
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/rbtree.h and lib/rbtree.c, please see
 * lib/LEGALNOTICE.rbtree and lib/LICENSE.rbtree
 * 
 * ------------------------------------------------------------------------
 * 
 * Unless otherwise noted, the following license applies to STAMP files:
 * 
 * Copyright (c) 2007, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 * 
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 * 
 *     * Neither the name of Stanford University nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY STANFORD UNIVERSITY ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 * =============================================================================
 */

#include "../include/tbb.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include "common-full.h"
#include "normal-full.h"
#include "random-full.h"
#include "timer.h"
#include "tm.h"
#include "util.h"
#line 99

double global_time = 0.0;

//float global_delta;
long global_i; /* index into task queue */

#define CHUNK 3


/* =============================================================================
 * work
 * =============================================================================
 */
class Work {
    float** const feature; // expected-warning{{Inferred region arguments: float **const, IN:[r1645_feature], ArgV:[rGLOBAL], [rGLOBAL]}}
    int     const nfeatures; // expected-warning{{Inferred region arguments: const int, IN:[r1648_nfeatures], ArgV:}}
    int     const npoints; // expected-warning{{Inferred region arguments: const int, IN:[r1649_npoints], ArgV:}}
    int     const nclusters; // expected-warning{{Inferred region arguments: const int, IN:[r1650_nclusters], ArgV:}}
//  int*    const membership;
    float** const clusters; // expected-warning{{Inferred region arguments: float **const, IN:[r1651_clusters], ArgV:[rGLOBAL], [rGLOBAL]}}
    int**   const new_centers_len ARG(Global, Global, Shared); // expected-warning{{Inferred region arguments: int **const, IN:[r1654_new_centers_len], ArgV:[rGLOBAL], [rGLOBAL]}}
    float** const new_centers ARG(Global, Global, Shared); // expected-warning{{Inferred region arguments: float **const, IN:[r1657_new_centers], ArgV:[rGLOBAL], [rGLOBAL]}}
    tbb::spin_mutex* const mutexes; // expected-warning{{Inferred region arguments: tbb::spin_mutex *const, IN:[r1660_mutexes], ArgV:[rGLOBAL]}}

public:
    void operator() READS(Global) WRITES(Shared) (
    				const tbb::blocked_range<int>& r) const // expected-warning{{Inferred region arguments: const tbb::blocked_range<int> &, IN:<empty>, ArgV:[r1662_r]}}
    {  // expected-warning{{Inferred Effect Summary for operator(): [reads(rpl([r1645_feature],[])),reads(rpl([r1648_nfeatures],[])),reads(rpl([r1650_nclusters],[])),reads(rpl([r1651_clusters],[])),reads(rpl([r893_pt1],[])),reads(rpl([r894_pt2],[])),reads(rpl([r896_pts],[])),reads(rpl([rGLOBAL],[])),writes(rpl([rLOCAL],[]))]}} 

//      float delta = 0.0;
        
        for(int i = r.begin(); i != r.end(); ++i) {

#ifndef ASAP_IGNORE_LOCKED_BLOCKS
            int index = common_findNearestPoint(feature[i],
                                                nfeatures,
                                                clusters,
                                                nclusters);
#else
            common_findNearestPoint(feature[i],
                                    nfeatures,
                                    clusters,
                                    nclusters);
#endif
//          /*
//           * If membership changes, increase delta by 1.
//           * membership[i] cannot be changed by other threads
//           */
//          if (membership[i] != index) {
//              delta += 1.0;
//          }
//
//          /* Assign the membership to object i */
//          /* membership[i] can't be changed by other thread */
//          membership[i] = index;

            /* Update new cluster centers : sum of objects located within */
#ifndef ASAP_IGNORE_LOCKED_BLOCKS
            {
                tbb::spin_mutex::scoped_lock myLock(mutexes[index]);
                *new_centers_len[index] = *new_centers_len[index] + 1;
                for (int j = 0; j < nfeatures; j++) {
                    new_centers[index][j] = new_centers[index][j] + feature[i][j];
                }
            }
#endif
        }
        
//      TM_BEGIN();
//      TM_SHARED_WRITE_F(global_delta, TM_SHARED_READ_F(global_delta) + delta);
//      TM_END();
    }
    
    Work (float** const feature, // expected-warning{{Inferred region arguments: float **const, IN:Local, ArgV:[rGLOBAL], [rGLOBAL]}}
         int     const nfeatures,
         int     const npoints,
         int     const nclusters,
//       int*    const membership,
         float** const clusters, // expected-warning{{Inferred region arguments: float **const, IN:Local, ArgV:[rGLOBAL], [rGLOBAL]}}
         int**   const new_centers_len ARG(Local, Global, Shared), // expected-warning{{Inferred region arguments: int **const, IN:Local, ArgV:[rGLOBAL], [rGLOBAL]}}
         float** const new_centers ARG(Local, Global, Shared), // expected-warning{{Inferred region arguments: float **const, IN:Local, ArgV:[rGLOBAL], [rGLOBAL]}}
         tbb::spin_mutex* const mutexes) : // expected-warning{{Inferred region arguments: tbb::spin_mutex *const, IN:Local, ArgV:[rGLOBAL]}}
    feature(feature),
    nfeatures(nfeatures),
    npoints(npoints),
    nclusters(nclusters),
//  membership(membership),
    clusters(clusters),
    new_centers_len(new_centers_len),
    new_centers(new_centers),
    mutexes(mutexes)
    {}  // expected-warning{{Inferred Effect Summary for Work: [reads(rpl([rLOCAL],[]))]}}
    
    // Copy constructor must be defined explicitly so that we can put an effect annotation on it
    READS(Global) Work (const Work& other) :  // expected-warning{{Inferred region arguments: const class Work &, IN:<empty>, ArgV:[r1672_other]}}
    feature(other.feature),
    nfeatures(other.nfeatures),
    npoints(other.npoints),
    nclusters(other.nclusters),
//  membership(other.membership),
    clusters(other.clusters),
    new_centers_len(other.new_centers_len),
    new_centers(other.new_centers),
    mutexes(other.mutexes)
    {}  // expected-warning{{Inferred Effect Summary for Work: [reads(rpl([r1645_feature],[])),reads(rpl([r1648_nfeatures],[])),reads(rpl([r1649_npoints],[])),reads(rpl([r1650_nclusters],[])),reads(rpl([r1651_clusters],[])),reads(rpl([r1654_new_centers_len],[])),reads(rpl([r1657_new_centers],[])),reads(rpl([r1660_mutexes],[]))]}} 
};



/* =============================================================================
 * normal_exec
 * =============================================================================
 */
float**
normal_exec WRITES(Global, Shared) // expected-warning{{Inferred region arguments: float **(int, float **, int, int, int, float, random_t *), IN:<empty>, ArgV:[r1673_normal_exec], [rGLOBAL], [rGLOBAL]}}
            (int       nthreads,
             float**   feature,    /* in: [npoints][nfeatures] */ // expected-warning{{Inferred region arguments: float **, IN:Local, ArgV:[rGLOBAL], [rGLOBAL]}}
             int       nfeatures,
             int       npoints,
             int       nclusters,
             float     threshold,
//           int*      membership,
             random_t* randomPtr) /* out: [npoints] */ // expected-warning{{Inferred region arguments: random_t *, IN:Local, ArgV:[r871_extractMoments]}}
{  // expected-warning{{Inferred Effect Summary for normal_exec: [reads(rpl([r1645_feature],[])),reads(rpl([r1648_nfeatures],[])),reads(rpl([r1650_nclusters],[])),reads(rpl([r1651_clusters],[])),reads(rpl([r1704_mag01],[])),reads(rpl([r324_tv_sec],[])),reads(rpl([r325_tv_usec],[])),reads(rpl([r893_pt1],[])),reads(rpl([r894_pt2],[])),reads(rpl([r896_pts],[])),reads(rpl([rGLOBAL],[])),writes(rpl([r1679_new_centers_len],[])),writes(rpl([r1682_clusters],[])),writes(rpl([r1685_new_centers],[])),writes(rpl([r1688_mutexes],[])),writes(rpl([r1690_new_centers_lengths],[])),writes(rpl([r1692_new_centers_data],[])),writes(rpl([r1697_mt],[])),writes(rpl([r1698_mtiPtr],[])),writes(rpl([r1702_mt],[])),writes(rpl([rGLOBAL],[])),writes(rpl([rLOCAL],[]))]}} //Inferred Effect Summary for normal_exec: [writes(rpl([r0_Shared],[])),writes(rpl([rGLOBAL],[])),writes(rpl([rLOCAL],[]))]}}
    int i;
    int j;
    int loop = 0;
    int** new_centers_len ARG(Local, Global, Shared); /* [nclusters]: no. of points in each cluster */ // expected-warning{{Inferred region arguments: int **, IN:[r1679_new_centers_len], ArgV:[rGLOBAL], [rGLOBAL]}}
//  float delta;
    float** clusters;      /* out: [nclusters][nfeatures] */ // expected-warning{{Inferred region arguments: float **, IN:[r1682_clusters], ArgV:[rGLOBAL], [rGLOBAL]}}
    float** new_centers ARG(Local, Global, Shared);   /* [nclusters][nfeatures] */ // expected-warning{{Inferred region arguments: float **, IN:[r1685_new_centers], ArgV:[rGLOBAL], [rGLOBAL]}}
    tbb::spin_mutex *mutexes; // expected-warning{{Inferred region arguments: tbb::spin_mutex *, IN:[r1688_mutexes], ArgV:[rGLOBAL]}}
//  void* alloc_memory = NULL;
    int *new_centers_lengths ARG(Local, Shared); // expected-warning{{Inferred region arguments: int *, IN:[r1690_new_centers_lengths], ArgV:[rGLOBAL]}}
    float *new_centers_data ARG(Local, Shared); // expected-warning{{Inferred region arguments: float *, IN:[r1692_new_centers_data], ArgV:[rGLOBAL]}}
    TIMER_T start; // expected-warning{{Inferred region arguments: struct timeval, IN:<empty>, ArgV:[r1694_start]}}
    TIMER_T stop; // expected-warning{{Inferred region arguments: struct timeval, IN:<empty>, ArgV:[r1695_stop]}}

    /* Allocate space for returning variable clusters[] */
    clusters = (float**)malloc(nclusters * sizeof(float*));
    assert(clusters);
    clusters[0] = (float*)malloc(nclusters * nfeatures * sizeof(float));
    assert(clusters[0]);
    for (i = 1; i < nclusters; i++) {
        clusters[i] = clusters[i-1] + nfeatures;
    }

    /* Randomly pick cluster centers */
    for (i = 0; i < nclusters; i++) {
        int n = (int)(random_generate(randomPtr) % npoints);
        for (j = 0; j < nfeatures; j++) {
            clusters[i][j] = feature[n][j];
        }
    }

//  for (i = 0; i < npoints; i++) {
//      membership[i] = -1;
//  }

    /*
     * Need to initialize new_centers_len and new_centers[0] to all 0.
     * Allocate clusters on different cache lines to reduce false sharing.
     */
    {
//      int cluster_size = sizeof(int) + sizeof(float) * nfeatures;
//      const int cacheLineSize = 32;
//      cluster_size += (cacheLineSize-1) - ((cluster_size-1) % cacheLineSize);
//      alloc_memory = calloc(nclusters, cluster_size);
        new_centers_lengths = (int*)calloc(nclusters, sizeof(int));
        new_centers_data = (float*)calloc(nclusters, sizeof(float) * nfeatures);
        new_centers_len = (int**) malloc(nclusters * sizeof(int*));
        new_centers = (float**) malloc(nclusters * sizeof(float*));
        mutexes = new tbb::spin_mutex[nclusters];
        assert(new_centers_lengths && new_centers_data);
        assert(/*alloc_memory &&*/ new_centers && new_centers_len);
        for (i = 0; i < nclusters; i++) {
//          new_centers_len[i] = (int*)((char*)alloc_memory + cluster_size * i);
//          new_centers[i] = (float*)((char*)alloc_memory + cluster_size * i + sizeof(int));
            new_centers_len[i] = new_centers_lengths + i;
            new_centers[i] = new_centers_data + i * nclusters;
        }
    }

    TIMER_READ(start);

    GOTO_SIM();

    do {
//      delta = 0.0;

        global_i = nthreads * CHUNK;
//      global_delta = delta;

        Work work(feature, // expected-warning{{Inferred region arguments: class Work, IN:<empty>, ArgV:[rGLOBAL]}}
                  nfeatures,
                  npoints,
                  nclusters,
//                membership,
                  clusters,
                  new_centers_len,
                  new_centers,
                  mutexes);
        tbb::blocked_range<int> range(0,npoints);
        tbb::parallel_for(range, work);

//      delta = global_delta;

        /* Replace old cluster centers with new_centers */
        for (i = 0; i < nclusters; i++) {
            for (j = 0; j < nfeatures; j++) {
                if (new_centers_len[i] > 0) {
                    clusters[i][j] = new_centers[i][j] / *new_centers_len[i];
                }
                new_centers[i][j] = 0.0;   /* set back to 0 */
            }
            *new_centers_len[i] = 0;   /* set back to 0 */  // expected-warning{{Dereference of null pointer}} :: spurious warning from Clang
        }

//      delta /= npoints;

    } while (loop++ < 10 /*(delta > threshold) && (loop++ < 500)*/);

    GOTO_REAL();

    TIMER_READ(stop);
    global_time += TIMER_DIFF_SECONDS(start, stop);

//  free(alloc_memory);
    free(new_centers_lengths);
    free(new_centers_data);
    free(new_centers);
    free(new_centers_len);
    delete[] mutexes;

    return clusters;
}


/* =============================================================================
 *
 * End of normal.c
 *
 * =============================================================================
 */
