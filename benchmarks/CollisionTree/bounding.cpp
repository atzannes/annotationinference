#include <cmath>
#include "bounding.h"
#line 4 "bounding.cpp"

// XXX If this appears before the call to it in CollisionTree::intersect 
// in the single-file version, it will cause spurious checker errors.
BoundingBox &BoundingBox::transform_r ARG(Rstore) 
//PARAM(Rrotate, Rtranslate, Rscale, Rstore)
READS(R, Rrotate, Rtranslate, Rscale) WRITES(Rstore) (
		Quaternion &rotate ARG_(Rrotate), 
		Vector3f &translate ARG_(Rtranslate), 
		Vector3f &scale ARG_(Rscale), 
		BoundingBox &store ARG_(Rstore))
{  // expected-warning{{Inferred Effect Summary for transform_r: [reads(rpl([p51_R],[])),reads(rpl([p53_Rrotate],[])),reads(rpl([p54_Rtranslate],[])),reads(rpl([p55_Rscale],[])),writes(rpl([p56_Rstore],[])),writes(rpl([rLOCAL],[]))]}}
		//Matrix3f _compMat;
		Vector3f _compVect1;
		Vector3f _compVect2;

        BoundingBox &box ARG(Rstore) = store;
        BoundingBox &boxVol ARG(Rstore) = box;

        center.mult(scale, boxVol.center);
        rotate.mult(boxVol.center, boxVol.center);
        boxVol.center.addLocal(translate);

        Matrix3f transMatrix;
        transMatrix.set(rotate);
        // Make the rotation matrix all positive to get the maximum x/y/z extent
        transMatrix.m00 = fabs(transMatrix.m00);
        transMatrix.m01 = fabs(transMatrix.m01);
        transMatrix.m02 = fabs(transMatrix.m02);
        transMatrix.m10 = fabs(transMatrix.m10);
        transMatrix.m11 = fabs(transMatrix.m11);
        transMatrix.m12 = fabs(transMatrix.m12);
        transMatrix.m20 = fabs(transMatrix.m20);
        transMatrix.m21 = fabs(transMatrix.m21);
        transMatrix.m22 = fabs(transMatrix.m22);

        _compVect1.set(xExtent * scale.x, yExtent * scale.y, zExtent * scale.z);
        transMatrix.mult(_compVect1, _compVect2);
        // Assign the biggest rotations after scales.
        box.xExtent = fabs(_compVect2.x);
        box.yExtent = fabs(_compVect2.y);
        box.zExtent = fabs(_compVect2.z);

        return box;
}

bool BoundingBox::intersectsBoundingBox /*PARAM(Rbb)*/ 
READS(R, Rbb) (
		BoundingBox &bb ARG_(Rbb))
{  // expected-warning{{Inferred Effect Summary for intersectsBoundingBox: [reads(rpl([p51_R],[])),reads(rpl([p57_Rbb],[])),reads(rpl([rLOCAL],[]))]}}
		BoundingBox &bbVol ARG(Rbb) = bb;
        if (!Vector3f::isValidVector(center) || !Vector3f::isValidVector(bbVol.center)) return false;
    
        if (center.x + xExtent < bbVol.center.x - bb.xExtent
                || center.x - xExtent > bbVol.center.x + bb.xExtent) {
            return false;
        }
        else if (center.y + yExtent < bbVol.center.y - bb.yExtent
                || center.y - yExtent > bbVol.center.y + bb.yExtent) {
            return false;
        }
        else if (center.z + zExtent < bbVol.center.z - bb.zExtent
                || center.z - zExtent > bbVol.center.z + bb.zExtent) {
            return false;
        }
        else
            return true;
}
