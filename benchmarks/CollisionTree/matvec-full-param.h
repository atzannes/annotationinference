#ifndef MATVEC_H
#define MATVEC_H

#include <cmath>
#include "asap.h"
#line 7 "matvec-full.h"

class Vector2f;
class Vector3f;
class Quaternion;
class Matrix3f;


class Vector2f { // expected-warning{{Inferred region arguments: class Vector2f &&, IN:<empty>, ArgV:[p21_Vector2f]}} expected-warning{{Inferred region arguments: const class Vector2f &, IN:<empty>, ArgV:[p21_Vector2f]}}
public:
    float x ARG(R), y ARG(R); // expected-warning{{Inferred region arguments: float, IN:[p21_Vector2f], ArgV::}}   expected-warning{{Inferred region arguments: float, IN:[p21_Vector2f], ArgV::}}
    Vector2f() : x(0), y(0) {}
};


class Vector3f { // expected-warning{{Inferred region arguments: class Vector3f &(const class Vector3f &), IN:<empty>, ArgV:[p22_Vector3f]:}} expected-warning{{Inferred region arguments: const class Vector3f &, IN:<empty>, ArgV:[p22_Vector3f]:}}
public:
    float x ARG(R), y ARG(R), z ARG(R); // expected-warning{{Inferred region arguments: float, IN:[p22_Vector3f], ArgV:}}  expected-warning{{Inferred region arguments: float, IN:[p22_Vector3f], ArgV:}}  expected-warning{{Inferred region arguments: float, IN:[p22_Vector3f], ArgV:}}

	Vector3f (): x(0), y(0), z(0) {}
	
	PARAM(P) READS(P)
	Vector3f(const Vector3f &v ARG(P)) : x(v.x), y(v.y), z(v.z) {} // expected-warning{{Inferred Effect Summary for Vector3f: [reads(rpl([p23_P],[]))]}}
    
    Vector3f &multLocal ARG(R) PARAM(Rvec) // expected-warning{{Inferred region arguments: class Vector3f &(class Vector3f &), IN:<empty>, ArgV:[p22_Vector3f]:}}
    READS(Rvec) WRITES(R) (Vector3f &vec ARG(Rvec));

    Vector3f &addLocal ARG(R) PARAM(Rvec) //  expected-warning{{Inferred region arguments: class Vector3f &(class Vector3f &), IN:<empty>, ArgV:[p22_Vector3f]:}}
    READS(Rvec) WRITES(R) (Vector3f &vec ARG(Rvec));
    
    Vector3f &set ARG(R) PARAM(Rvect) //  expected-warning{{Inferred region arguments: class Vector3f &(const class Vector3f &), IN:<empty>, ArgV:[p22_Vector3f]:}}
    READS(Rvect) WRITES(R) (
    	const Vector3f &vect ARG(Rvect));
    
    Vector3f &set ARG(R) WRITES(R) (float x, float y, float z); // expected-warning{{Inferred region arguments: class Vector3f &(float, float, float), IN:<empty>, ArgV:[p22_Vector3f]:}}

	Vector3f &mult ARG(Rstore) PARAM(Rvec, Rstore) // expected-warning{{Inferred region arguments: class Vector3f &(class Vector3f &, class Vector3f &), IN:<empty>, ArgV:[p28_Rstore]:}}
	READS(R, Rvec) WRITES(Rstore) (
		Vector3f &vec ARG(Rvec), Vector3f &store ARG(Rstore));

    Vector3f &subtract ARG(Rresult) PARAM(Rvec, Rresult) // expected-warning{{Inferred region arguments: class Vector3f &(class Vector3f &, class Vector3f &), IN:<empty>, ArgV:[p30_Rresult]:}}
    READS(R, Rvec) WRITES(Rresult) (
    	Vector3f &vec ARG(Rvec), Vector3f &result ARG(Rresult));

	float dot PARAM(Rvec)
	READS(R, Rvec) (Vector3f &vec ARG(Rvec));

	Vector3f &cross ARG(Rresult) PARAM(Rresult)  // expected-warning{{Inferred region arguments: class Vector3f &(float, float, float, class Vector3f &), IN:<empty>, ArgV:[p32_Rresult]:}}
	READS(R) WRITES(Rresult) (
		float otherX, float otherY, float otherZ, Vector3f &result ARG(Rresult));

	Vector3f &cross ARG(Rresult) PARAM(Rv, Rresult) // expected-warning{{Inferred region arguments: class Vector3f &(class Vector3f &, class Vector3f &), IN:<empty>, ArgV:[p34_Rresult]:}}
	READS(R, Rv) WRITES(Rresult) (
		Vector3f &v ARG(Rv), Vector3f &result ARG(Rresult));

	static bool isValidVector PARAM(Rvector) 
	READS(Rvector) (Vector3f &vector ARG(Rvector))
	{ // expected-warning{{Inferred Effect Summary for isValidVector: [reads(rpl([p35_Rvector],[])),reads(rpl([rLOCAL],[]))]}}
		if (std::isnan(vector.x) ||
			std::isnan(vector.y) ||
			std::isnan(vector.z)) return false;
		if (std::isinf(vector.x) ||
			std::isinf(vector.y) ||
			std::isinf(vector.z)) return false;
		return true;
	}

	float *toArray ARG(Rfloats) PARAM(Rfloats) // expected-warning{{Inferred region arguments: float *(float *), IN:Local, ArgV:[p36_Rfloats]:}}
	READS(R) WRITES(Rfloats) (
		float *floats ARG(Rfloats));
}; // end class Vector3f


class Quaternion { // expected-warning{{Inferred region arguments: class Quaternion &(const class Quaternion &), IN:<empty>, ArgV:[p37_Quaternion]:}}  // expected-warning{{Inferred region arguments: const class Quaternion &, IN:<empty>, ArgV:[p37_Quaternion]:}} 
public:
	float x ARG(R); // expected-warning{{Inferred region arguments: float, IN:[p37_Quaternion], ArgV::}}
    float y ARG(R); // expected-warning{{Inferred region arguments: float, IN:[p37_Quaternion], ArgV::}}
    float z ARG(R); // expected-warning{{Inferred region arguments: float, IN:[p37_Quaternion], ArgV::}}
    float w ARG(R); // expected-warning{{Inferred region arguments: float, IN:[p37_Quaternion], ArgV::}}

	Quaternion() : x(0), y(0), z(0), w(0) {}
    PARAM(P)
	Quaternion(const Quaternion &q ARG(P)) : x(q.x), y(q.y), z(q.z), w(q.w) {}  // expected-warning{{Inferred Effect Summary for Quaternion: [reads(rpl([p38_P],[]))]:}}

	Vector3f &mult ARG(Rstore) PARAM(Rv, Rstore)  // expected-warning{{Inferred region arguments: class Vector3f &(class Vector3f &, class Vector3f &), IN:<empty>, ArgV:[p40_Rstore]:}}
	READS(R, Rv) WRITES(Rstore) (
		Vector3f &v ARG(Rv), Vector3f &store ARG(Rstore));
    
    Matrix3f &toRotationMatrix ARG(Rresult) PARAM(Rresult) // expected-warning{{Inferred region arguments: class Matrix3f &(class Matrix3f &), IN:<empty>, ArgV:[p41_Rresult]}}
    READS(R) WRITES(Rresult) (Matrix3f &result ARG(Rresult));
    
    float norm READS(R) ();
}; // end class Quaternion


class Matrix3f { // expected-warning{{Inferred region arguments: class Matrix3f &&, IN:<empty>, ArgV:[p42_Matrix3f]:}} // expected-warning{{Inferred region arguments: const class Matrix3f &, IN:<empty>, ArgV:[p42_Matrix3f]:}}
public:
	float m00 ARG(R), m01 ARG(R), m02 ARG(R); // expected-warning{{Inferred region arguments: float, IN:[p42_Matrix3f], ArgV::}} // expected-warning{{Inferred region arguments: float, IN:[p42_Matrix3f], ArgV::}} // expected-warning{{Inferred region arguments: float, IN:[p42_Matrix3f], ArgV::}}
    float m10 ARG(R), m11 ARG(R), m12 ARG(R); // expected-warning{{Inferred region arguments: float, IN:[p42_Matrix3f], ArgV::}} // expected-warning{{Inferred region arguments: float, IN:[p42_Matrix3f], ArgV::}} // expected-warning{{Inferred region arguments: float, IN:[p42_Matrix3f], ArgV::}}
    float m20 ARG(R), m21 ARG(R), m22 ARG(R); // expected-warning{{Inferred region arguments: float, IN:[p42_Matrix3f], ArgV::}} // expected-warning{{Inferred region arguments: float, IN:[p42_Matrix3f], ArgV::}} // expected-warning{{Inferred region arguments: float, IN:[p42_Matrix3f], ArgV::}}
    
    void set PARAM(Rquaternion)
	READS(Rquaternion) WRITES(R) (
		Quaternion &quaternion ARG(Rquaternion));
	
	Vector3f &mult ARG(Rproduct) PARAM(Rvec, Rproduct) // expected-warning{{Inferred region arguments: class Vector3f &(class Vector3f &, class Vector3f &), IN:<empty>, ArgV:[p45_Rproduct]:}}
	READS(R, Rvec) WRITES(Rproduct) (
		Vector3f &vec ARG(Rvec), Vector3f &product ARG(Rproduct));
};

#endif
