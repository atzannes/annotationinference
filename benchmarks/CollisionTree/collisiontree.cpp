#include "intersection.h"
#include "collisiontree.h"

namespace tbb {
template<typename Func0, typename Func1>
    void parallel_invoke [[asap::param("P0, P1, P2, P3, P4, P5, P6, P7, P8, P9")]]
                         (const Func0 &F0 [[asap::arg("P0, P1, P2, P3, P4")]],  
                          const Func1 &F1 [[asap::arg("P5, P6, P7, P8, P9")]]);
}

class PARAM(R, Rtree1, Rtree2, Rwb, Rresult) IntersectInvoker {
	CollisionTree *tree1 ARG(R, Rtree1);
	CollisionTree *tree2 ARG(R, Rtree2);
	BoundingBox *wb ARG(R, Rwb);
	int cutoff ARG(R);
	bool *resultPtr ARG(R, Rresult);

public:
	READS(R) IntersectInvoker (
			CollisionTree *tree1_v ARG(Rtree1),
			CollisionTree *tree2_v ARG(Rtree2),
			BoundingBox *wb_v ARG(Rwb),
			int cutoff_v, bool *resultPtr_v ARG(Rresult)) :
	tree1(tree1_v), tree2(tree2_v), wb(wb_v), cutoff(cutoff_v), resultPtr(resultPtr_v)
	{}  // expected-warning{{Inferred Effect Summary for IntersectInvoker: [reads(rpl([rLOCAL],[]))]}}

	void operator() 
	READS(R, Rtree1:*, Rtree2:*, Rwb, CollisionTree::RMeshes) 
	WRITES(Rresult) () const
	{ // expected-warning{{Inferred Effect Summary for operator(): [reads(rpl([p83_R],[])),reads(rpl([p84_Rtree1,rSTAR],[])),reads(rpl([p85_Rtree2,rSTAR],[])),reads(rpl([p86_Rwb],[])),reads(rpl([r2_RMeshes],[])),writes(rpl([p87_Rresult],[])),writes(rpl([rLOCAL],[]))]}}  :: writes Local:Left & Local:Right seem wrong
		*resultPtr = tree1->intersect(*tree2, *wb, cutoff);
	}
	
	
};

bool CollisionTree::intersect //PARAM(R_cT, Rwb)
//REGION(R_cTWB, RTemps)
READS(R:*, R_cT:*, Rwb, RMeshes) 
/*WRITES(RLists, RTemps:*)*/
(
			CollisionTree &collisionTree ARG_(R_cT),
            //ParallelArrayList<RLists> aList, ParallelArrayList<RLists> bList, 
            BoundingBox &myWorldBounds ARG_(Rwb), int cutoff)
{  // expected-warning{{Inferred Effect Summary for intersect: [reads(rpl([p60_R,rSTAR],[])),reads(rpl([p61_R_cT,rSTAR],[])),reads(rpl([p62_Rwb],[])),reads(rpl([r2_RMeshes],[])),writes(rpl([rLOCAL],[]))]}}
	BoundingBox ctWorldBounds ARG(Local);
	collisionTree.bounds->transform_r(
		collisionTree.mesh->worldRotation, 
		collisionTree.mesh->worldTranslation,
		collisionTree.mesh->worldScale,	
		ctWorldBounds);

	if (!myWorldBounds.intersectsBoundingBox(ctWorldBounds)) {
		return false;
	}

	// if our node is not a leaf send the children (both left and right) to
	// the test tree.
	if (left != nullptr) { // This is not a leaf
		if (cutoff > 0) {
			bool test1 ARG(Local);
			bool test2 ARG(Local);
			BoundingBox ctWorldBounds2 ARG(Rwb) (ctWorldBounds); 
			
			//ParallelArrayList<RLists2> aList2 = new ParallelArrayList<RLists2>();
        	//ParallelArrayList<RLists2> bList2 = new ParallelArrayList<RLists2>();
        	
        	IntersectInvoker leftFn ARG(Rwb, R_cT, R:Left, Rwb, Local) (
        			&collisionTree, left, &ctWorldBounds2, cutoff-1, &test1);
        	IntersectInvoker rightFn ARG(Rwb, R_cT, R:Right, Rwb, Local) (
        			&collisionTree, right, &ctWorldBounds2, cutoff-1, &test2);
        	tbb::parallel_invoke(leftFn, rightFn);
        	//test1 = collisionTree.intersect(*left, ctWorldBounds, cutoff-1);
        	//test2 = collisionTree.intersect(*right, ctWorldBounds, cutoff-1);
        	
			if (test2) {
				//aList.addAll(aList2);
				//bList.addAll(bList2);
				return true;
			} else if (test1) {
				return true;
			} else {
				return false;
			}
		} else {
			bool test = collisionTree.intersect(*left, ctWorldBounds, cutoff-1);
			test = collisionTree.intersect(*right, ctWorldBounds, cutoff-1) || test;
			return test;
		}
	}

	// This node is a leaf, but the testing tree node is not. Therefore,
	// continue processing the testing tree until we find its leaves.
	if (collisionTree.left != nullptr) {
		if (cutoff > 0) {
			bool test1 ARG(Local);
			bool test2 ARG(Local);
			
			//ParallelArrayList<RLists2> aList2 = new ParallelArrayList<RLists2>();
			//ParallelArrayList<RLists2> bList2 = new ParallelArrayList<RLists2>();
			
			IntersectInvoker leftFn ARG(Rwb, R, R_cT:Left, Rwb, Local) (
					this, collisionTree.left, &myWorldBounds, cutoff-1, &test1);
			IntersectInvoker rightFn ARG(Rwb, R, R_cT:Right, Rwb, Local) (
					this, collisionTree.right, &myWorldBounds, cutoff-1, &test2);
        	tbb::parallel_invoke(leftFn, rightFn);
			//test1 = this->intersect(*collisionTree.left, myWorldBounds, cutoff-1);
			//test2 = this->intersect(*collisionTree.right, myWorldBounds, cutoff-1);

			if (test2) {
				//aList.addAll(aList2);
				//bList.addAll(bList2);
				return true;
			} else if (test1) {
				return true;
			} else {
				return false;
			}
		} else {
			bool test = this->intersect(*collisionTree.left, myWorldBounds, cutoff-1);
			test = this->intersect(*collisionTree.right, myWorldBounds, cutoff-1) || test;
			return test;        		
		}
	}

	// both this node and the testing node are leaves. Therefore, we can
	// switch to checking the contained triangles with each other. Any
	// that are found to intersect are placed in the appropriate list.
	Quaternion &roti ARG(RMeshes) = mesh->worldRotation;
	Vector3f &scalei ARG(RMeshes) = mesh->worldScale;
	Vector3f &transi ARG(RMeshes) = mesh->worldTranslation;

	Quaternion &rotj ARG(RMeshes) = collisionTree.mesh->worldRotation;
	Vector3f &scalej ARG(RMeshes) = collisionTree.mesh->worldScale;
	Vector3f &transj ARG(RMeshes) = collisionTree.mesh->worldTranslation;

	bool test = false;

	// Temporaries to contain information for ray intersection
	// Converted from fields to provide thread-safety
	Vector3f tempVa ARG(Local);
	Vector3f tempVb ARG(Local);
	Vector3f tempVc ARG(Local);
	Vector3f tempVd ARG(Local);
	Vector3f tempVe ARG(Local);
	Vector3f tempVf ARG(Local);

	Vector3f verts ARG(Local) [3];
	Vector3f target ARG(Local) [3];

	// Temporaries for Intersection.intersection_r
	Vector3f e1 ARG(Local);
	Vector3f e2 ARG(Local);
	Vector3f n1 ARG(Local);
	Vector3f n2 ARG(Local);
	// XXX: Using "float * ... = new float[2]" for isect1/isect2 crashes the checker.
	float isect1 ARG(Local) [2];
	float isect2 ARG(Local) [2];

	for (int i = start; i < end; i++) {
		mesh->getTriangle(triIndex[i], verts);
		// XXX: Writing "roti.mult(..., tempVa).addLocal(...) gives a spurious
		// "writes RMesh" effect.  Method effect param inference error?
		roti.mult(tempVa.set(verts[0]).multLocal(scalei), tempVa); tempVa.addLocal(transi);
		roti.mult(tempVb.set(verts[1]).multLocal(scalei), tempVb); tempVb.addLocal(transi);
		roti.mult(tempVc.set(verts[2]).multLocal(scalei), tempVc); tempVc.addLocal(transi);
		for (int j = collisionTree.start; j < collisionTree.end; j++) {
			collisionTree.mesh->getTriangle(collisionTree.triIndex[j], target);
			rotj.mult(tempVd.set(target[0]).multLocal(scalej), tempVd); tempVd.addLocal(transj);
			rotj.mult(tempVe.set(target[1]).multLocal(scalej), tempVe); tempVd.addLocal(transj);
			rotj.mult(tempVf.set(target[2]).multLocal(scalej), tempVf); tempVd.addLocal(transj);
			if (Intersection::intersection_r(tempVa, tempVb, tempVc, tempVd, tempVe, tempVf, e1, e2, n1, n2, isect1, isect2)) {
				test = true;
				//aList.add(triIndex[i]);
				//bList.add(collisionTree.triIndex[j]);
			}
		}
	}

	return test;
}
            
