#ifndef INTERSECTION_H
#define INTERSECTION_H

#include "asap.h"
#include "matvec.h"
#line 7 "intersection.h"

namespace Intersection {

bool intersection_r PARAM(RVals, RTemps) 
WRITES(RVals, RTemps:*) (
		Vector3f &v0 ARG_(RVals),
		Vector3f &v1 ARG_(RVals),
		Vector3f &v2 ARG_(RVals),
		Vector3f &u0 ARG_(RVals),
		Vector3f &u1 ARG_(RVals),
		Vector3f &u2 ARG_(RVals),
		Vector3f &e1 ARG_(RTemps),
		Vector3f &e2 ARG_(RTemps),
		Vector3f &n1 ARG_(RTemps),
		Vector3f &n2 ARG_(RTemps),
		float *isect1 ARG_(RTemps),
		float *isect2 ARG_(RTemps));

}

#endif
