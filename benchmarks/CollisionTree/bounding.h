#ifndef BOUNDING_H
#define BOUNDING_H

#include "asap.h"
#include "matvec.h"
#line 7 "bounding.h"

class PARAM(R) BoundingBox {
protected:
	Vector3f center ARG(R);

public:
	float xExtent ARG(R), yExtent ARG(R), zExtent ARG(R);

public:
    BoundingBox() : center(), xExtent(0), yExtent(0), zExtent(0) {} 

    PARAM(P) READS(P)
	BoundingBox(const BoundingBox &b ARG(P))
				: center(b.center), xExtent(b.xExtent),
				  yExtent(b.yExtent), zExtent(b.zExtent) {} // expected-warning{{Inferred Effect Summary for BoundingBox: [reads(rpl([p52_P],[]))]}}

	BoundingBox &transform_r ARG(Rstore) 
	PARAM(Rrotate, Rtranslate, Rscale, Rstore) 
	READS(R, Rrotate, Rtranslate, Rscale) WRITES(Rstore) (
		Quaternion &rotate ARG_(Rrotate), 
		Vector3f &translate ARG_(Rtranslate), 
		Vector3f &scale ARG_(Rscale), 
		BoundingBox &store ARG_(Rstore));
		
	bool intersectsBoundingBox PARAM(Rbb) READS(R, Rbb) (
		BoundingBox &bb ARG_(Rbb));
};

#endif
