#include "trimesh-full.h"
#line 3 "trimesh-full.cpp"

void TriMesh::getTriangle //param(R) 
READS(RMesh) WRITES(R) (
	 	int i, Vector3f vertices ARG(R) [])
{  // expected-warning{{Inferred Effect Summary for getTriangle: [reads(rpl([p46_TriMesh],[])),writes(rpl([p47_R],[])),writes(rpl([rLOCAL],[]))]}}
	if (i < triangleCount && i >= 0) {
		for (int x = 0; x < 3; x++) {
			int vertexIndex = indexArray[i*3 + x];
			vertices[x].x = vertexArray[vertexIndex*3];
			vertices[x].y = vertexArray[vertexIndex*3 + 1];
			vertices[x].z = vertexArray[vertexIndex*3 + 2];
		}
	}
}
