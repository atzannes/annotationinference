#ifndef INTERSECTION_H
#define INTERSECTION_H

#include "asap.h"
#include "matvec.h"
#line 7 "intersection-full.h"

namespace Intersection {

bool intersection_r PARAM_(RVals, RTemps) 
WRITES(RVals, RTemps:*) (
		Vector3f &v0 ARG_(RVals),
		Vector3f &v1 ARG(RVals), // expected-warning{{Inferred region arguments: class Vector3f &, IN:<empty>, ArgV:[p55_RVals]:}}
		Vector3f &v2 ARG(RVals), // expected-warning{{Inferred region arguments: class Vector3f &, IN:<empty>, ArgV:[p55_RVals]:}}
		Vector3f &u0 ARG(RVals), // expected-warning{{Inferred region arguments: class Vector3f &, IN:<empty>, ArgV:[p55_RVals]:}}
		Vector3f &u1 ARG(RVals), // expected-warning{{Inferred region arguments: class Vector3f &, IN:<empty>, ArgV:[p55_RVals]:}}
		Vector3f &u2 ARG(RVals), // expected-warning{{Inferred region arguments: class Vector3f &, IN:<empty>, ArgV:[p55_RVals]:}}
		Vector3f &e1 ARG_(RTemps),
		Vector3f &e2 ARG(RTemps), // expected-warning{{Inferred region arguments: class Vector3f &, IN:<empty>, ArgV:[p55_RVals]:}} // CHECK
		Vector3f &n1 ARG(RTemps), // expected-warning{{Inferred region arguments: class Vector3f &, IN:<empty>, ArgV:[p55_RVals]:}} // CHECK
		Vector3f &n2 ARG(RTemps), // expected-warning{{Inferred region arguments: class Vector3f &, IN:<empty>, ArgV:[p55_RVals]:}} // CHECK
		float *isect1 ARG(RTemps), // expected-warning{{Inferred region arguments: float *, IN:Local, ArgV:[p55_RVals]:}}  // CHECK
		float *isect2 ARG(RTemps)); // expected-warning{{Inferred region arguments: float *, IN:Local, ArgV:[p55_RVals]:}} // CHECK

}

#endif
