#include "trimesh.h"
#line 3 "trimesh.cpp"

void TriMesh::getTriangle //PARAM(R) 
READS(RMesh) WRITES(R) (
	 	int i, Vector3f vertices ARG(R) [])
{  // expected-warning{{Inferred Effect Summary for getTriangle: [reads(rpl([p49_RMesh],[])),writes(rpl([p50_R],[])),writes(rpl([rLOCAL],[]))]}}
	if (i < triangleCount && i >= 0) {
		for (int x = 0; x < 3; x++) {
			int vertexIndex = indexArray[i*3 + x];
			vertices[x].x = vertexArray[vertexIndex*3];
			vertices[x].y = vertexArray[vertexIndex*3 + 1];
			vertices[x].z = vertexArray[vertexIndex*3 + 2];
		}
	}
}
