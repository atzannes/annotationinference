#ifndef BOUNDING_H
#define BOUNDING_H

#include "asap.h"
#include "matvec-full.h"
#line 7 "bounding-full.h"

class BoundingBox {
protected:
	Vector3f center ARG(R); // expected-warning{{Inferred region arguments: class Vector3f, IN:<empty>, ArgV:[p48_BoundingBox]:}}

public:
	float xExtent ARG(R), yExtent ARG(R), zExtent ARG(R); // expected-warning{{Inferred region arguments: float, IN:[p48_BoundingBox], ArgV::}} // expected-warning{{Inferred region arguments: float, IN:[p48_BoundingBox], ArgV::}} // expected-warning{{Inferred region arguments: float, IN:[p48_BoundingBox], ArgV::}}

public:
    BoundingBox() : center(), xExtent(0), yExtent(0), zExtent(0) {} 

    PARAM_(P) READS(P)
	BoundingBox(const BoundingBox &b ARG_(P)) 
				: center(b.center), xExtent(b.xExtent),
				  yExtent(b.yExtent), zExtent(b.zExtent) {} // expected-warning{{Inferred Effect Summary for BoundingBox: [reads(rpl([p49_P],[]))]}}

	BoundingBox &transform_r ARG(Rstore)  // expected-warning{{Inferred region arguments: class BoundingBox &(class Quaternion &, class Vector3f &, class Vector3f &, class BoundingBox &), IN:<empty>, ArgV:[p53_Rstore]:}}
	PARAM_(Rrotate, Rtranslate, Rscale, Rstore) 
	READS(R, Rrotate, Rtranslate, Rscale) WRITES(Rstore) (
		Quaternion &rotate ARG_(Rrotate), 
		Vector3f &translate ARG_(Rtranslate), 
		Vector3f &scale ARG_(Rscale), 
		BoundingBox &store ARG_(Rstore));
		
	bool intersectsBoundingBox PARAM_(Rbb) READS(R, Rbb) (
		BoundingBox &bb ARG_(Rbb));
};

#endif
