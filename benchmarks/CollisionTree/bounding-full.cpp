#include <cmath>
#include "bounding-full.h"
#line 4 "bounding-full.cpp"

// XXX If this appears before the call to it in CollisionTree::intersect 
// in the single-file version, it will cause spurious checker errors.
BoundingBox &BoundingBox::transform_r ARG(Rstore) // expected-warning{{Inferred region arguments: class BoundingBox &(class Quaternion &, class Vector3f &, class Vector3f &, class BoundingBox &), IN:<empty>, ArgV:[p53_Rstore]:}} 
//param(Rrotate, Rtranslate, Rscale, Rstore)
READS(R, Rrotate, Rtranslate, Rscale) WRITES(Rstore) (
		Quaternion &rotate ARG(Rrotate), 
		Vector3f &translate ARG(Rtranslate), 
		Vector3f &scale ARG(Rscale), 
		BoundingBox &store ARG(Rstore))
{   // expected-warning{{Inferred Effect Summary for transform_r: [reads(rpl([p48_BoundingBox],[])),reads(rpl([p50_Rrotate],[])),reads(rpl([p51_Rtranslate],[])),reads(rpl([p52_Rscale],[])),reads(rpl([p53_Rstore],[])),reads(rpl([rLOCAL],[])),writes(rpl([p53_Rstore],[])),writes(rpl([rLOCAL],[]))}}
		//Matrix3f _compMat;
	Vector3f _compVect1 ARG_(Local);
	Vector3f _compVect2 ARG_(Local);

        BoundingBox &box ARG(Rstore) = store;  // expected-warning{{Inferred region arguments: class BoundingBox &, IN:<empty>, ArgV:[p53_Rstore]:}}
        BoundingBox &boxVol ARG(Rstore) = box; // expected-warning{{Inferred region arguments: class BoundingBox &, IN:<empty>, ArgV:[p53_Rstore]:}}

        center.mult(scale, boxVol.center);
        rotate.mult(boxVol.center, boxVol.center);
        boxVol.center.addLocal(translate);

        Matrix3f transMatrix ARG_(Local);
        transMatrix.set(rotate);
        // Make the rotation matrix all positive to get the maximum x/y/z extent
        transMatrix.m00 = fabs(transMatrix.m00);
        transMatrix.m01 = fabs(transMatrix.m01);
        transMatrix.m02 = fabs(transMatrix.m02);
        transMatrix.m10 = fabs(transMatrix.m10);
        transMatrix.m11 = fabs(transMatrix.m11);
        transMatrix.m12 = fabs(transMatrix.m12);
        transMatrix.m20 = fabs(transMatrix.m20);
        transMatrix.m21 = fabs(transMatrix.m21);
        transMatrix.m22 = fabs(transMatrix.m22);

        _compVect1.set(xExtent * scale.x, yExtent * scale.y, zExtent * scale.z);
        transMatrix.mult(_compVect1, _compVect2);
        // Assign the biggest rotations after scales.
        box.xExtent = fabs(_compVect2.x);
        box.yExtent = fabs(_compVect2.y);
        box.zExtent = fabs(_compVect2.z);

        return box;
}

bool BoundingBox::intersectsBoundingBox /*param(Rbb)*/ 
READS(R, Rbb) (
		BoundingBox &bb ARG(Rbb))
{  // expected-warning{{Inferred Effect Summary for intersectsBoundingBox: [reads(rpl([p48_BoundingBox],[])),reads(rpl([p54_Rbb],[])),reads(rpl([rLOCAL],[]))]}}
		BoundingBox &bbVol ARG(Rbb) = bb; // expected-warning{{Inferred region arguments: class BoundingBox &, IN:<empty>, ArgV:[p54_Rbb]:}}
        if (!Vector3f::isValidVector(center) || !Vector3f::isValidVector(bbVol.center)) return false;
    
        if (center.x + xExtent < bbVol.center.x - bb.xExtent
                || center.x - xExtent > bbVol.center.x + bb.xExtent) {
            return false;
        }
        else if (center.y + yExtent < bbVol.center.y - bb.yExtent
                || center.y - yExtent > bbVol.center.y + bb.yExtent) {
            return false;
        }
        else if (center.z + zExtent < bbVol.center.z - bb.zExtent
                || center.z - zExtent > bbVol.center.z + bb.zExtent) {
            return false;
        }
        else
            return true;
}
