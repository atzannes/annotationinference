#include <cmath>
#include "intersection.h"
#line 4 "intersection.cpp"

#define EPSILON 1e-12

namespace Intersection {

static bool edgeEdgeTest PARAM(R)
READS(R) (
		float *v0 ARG_(R), float *u0 ARG_(R), float *u1 ARG_(R), 
		int i0, int i1, float aX, float Ay)
{  // expected-warning{{Inferred Effect Summary for edgeEdgeTest: [reads(rpl([p88_R],[])),reads(rpl([rLOCAL],[]))]}}
	float Bx = u0[i0] - u1[i0];
	float By = u0[i1] - u1[i1];
	float Cx = v0[i0] - u0[i0];
	float Cy = v0[i1] - u0[i1];
	float f = Ay * Bx - aX * By;
	float d = By * Cx - Bx * Cy;
	if ((f > 0 && d >= 0 && d <= f) || (f < 0 && d <= 0 && d >= f)) {
		float e = aX * Cy - Ay * Cx;
		if (f > 0) {
			if (e >= 0 && e <= f)
				return true;
		} else {
			if (e <= 0 && e >= f)
				return true;
		}
	}
	return false;
}


static bool edgeAgainstTriEdges PARAM(R)
READS(R) (
		float *v0 ARG_(R), float *v1 ARG_(R), 
		float *u0 ARG_(R), float *u1 ARG_(R), float *u2 ARG_(R), 
		int i0, int i1)
{  // expected-warning{{Inferred Effect Summary for edgeAgainstTriEdges: [reads(rpl([p89_R],[])),writes(rpl([rLOCAL],[]))]}}
	float aX, aY;
	aX = v1[i0] - v0[i0];
	aY = v1[i1] - v0[i1];
	/* test edge u0,u1 against v0,v1 */
	if (Intersection::edgeEdgeTest(v0, u0, u1, i0, i1, aX, aY)) {
		return true;
	}
	/* test edge u1,u2 against v0,v1 */
	if (Intersection::edgeEdgeTest(v0, u1, u2, i0, i1, aX, aY)) {
		return true;
	}
	/* test edge u2,u1 against v0,v1 */
	if (Intersection::edgeEdgeTest(v0, u2, u0, i0, i1, aX, aY)) {
		return true;
	}
	return false;
}


static bool pointInTri PARAM(R)
READS(R) (
		float *V0 ARG_(R), 
		float *U0 ARG_(R), float *U1 ARG_(R), float *U2 ARG_(R), 
		int i0, int i1)
{  // expected-warning{{Inferred Effect Summary for pointInTri: [reads(rpl([p90_R],[])),writes(rpl([rLOCAL],[]))]}}
	float a, b, c, d0, d1, d2;
	/* is T1 completly inside T2? */
	/* check if V0 is inside tri(U0,U1,U2) */
	a = U1[i1] - U0[i1];
	b = -(U1[i0] - U0[i0]);
	c = -a * U0[i0] - b * U0[i1];
	d0 = a * V0[i0] + b * V0[i1] + c;

	a = U2[i1] - U1[i1];
	b = -(U2[i0] - U1[i0]);
	c = -a * U1[i0] - b * U1[i1];
	d1 = a * V0[i0] + b * V0[i1] + c;

	a = U0[i1] - U2[i1];
	b = -(U0[i0] - U2[i0]);
	c = -a * U2[i0] - b * U2[i1];
	d2 = a * V0[i0] + b * V0[i1] + c;
	if (d0 * d1 > 0.0 && d0 * d2 > 0.0)
		return true;
	
	return false;
}


static bool coplanarTriTri PARAM(Rn, Rv)
READS(Rv) WRITES(Rn) (
		Vector3f &n ARG_(Rn),
		Vector3f &v0 ARG_(Rv), Vector3f &v1 ARG_(Rv), Vector3f &v2 ARG_(Rv), 
		Vector3f &u0 ARG_(Rv), Vector3f &u1 ARG_(Rv), Vector3f &u2 ARG_(Rv))
{  // expected-warning{{Inferred Effect Summary for coplanarTriTri: [reads(rpl([p92_Rv],[])),writes(rpl([p91_Rn],[])),writes(rpl([rLOCAL],[]))]}}
	Vector3f a ARG(Rn);
	short i0, i1;
	a.x = fabs(n.x);
	a.y = fabs(n.y);
	a.z = fabs(n.z);

	if (a.x > a.y) {
		if (a.x > a.z) {
			i0 = 1; /* a[0] is greatest */
			i1 = 2;
		} else {
			i0 = 0; /* a[2] is greatest */
			i1 = 1;
		}
	} else /* a[0] <=a[1] */{
		if (a.z > a.y) {
			i0 = 0; /* a[2] is greatest */
			i1 = 1;
		} else {
			i0 = 0; /* a[1] is greatest */
			i1 = 2;
		}
	}

	/* test all edges of triangle 1 against the edges of triangle 2 */
	float v0f ARG(Rn) [3];
	v0.toArray(v0f);
	float v1f ARG(Rn) [3];
	v1.toArray(v1f);
	float v2f ARG(Rn) [3];
	v2.toArray(v2f);
	float u0f ARG(Rn) [3];
	u0.toArray(u0f);
	float u1f ARG(Rn) [3];
	u1.toArray(u1f);
	float u2f ARG(Rn) [3];
	u2.toArray(u2f);
	if (Intersection::edgeAgainstTriEdges(v0f, v1f, u0f, u1f, u2f, i0, i1)) {
		return true;
	}

	if (Intersection::edgeAgainstTriEdges(v1f, v2f, u0f, u1f, u2f, i0, i1)) {
		return true;
	}

	if (Intersection::edgeAgainstTriEdges(v2f, v0f, u0f, u1f, u2f, i0, i1)) {
		return true;
	}

	/* finally, test if tri1 is totally contained in tri2 or vice versa */
	Intersection::pointInTri(v0f, u0f, u1f, u2f, i0, i1);
	Intersection::pointInTri(u0f, v0f, v1f, v2f, i0, i1);

	return false;
}

static bool newComputeIntervals PARAM(Rabc, Rx)
WRITES(Rabc, Rx) (
		float vv0, float vv1, float vv2,
		float d0, float d1, float d2, float d0d1, float d0d2, 
		Vector3f &abc ARG_(Rabc), Vector2f &x0x1 ARG_(Rx))
{  // expected-warning{{Inferred Effect Summary for newComputeIntervals: [reads(rpl([rLOCAL],[])),writes(rpl([p93_Rabc],[])),writes(rpl([p94_Rx],[]))]}}
	if (d0d1 > 0.0f) {
		/* here we know that d0d2 <=0.0 */
		/*
		 * that is d0, d1 are on the same side, d2 on the other or on the
		 * plane
		 */
		abc.x = vv2;
		abc.y = (vv0 - vv2) * d2;
		abc.z = (vv1 - vv2) * d2;
		x0x1.x = d2 - d0;
		x0x1.y = d2 - d1;
	} else if (d0d2 > 0.0f) {
		/* here we know that d0d1 <=0.0 */
		abc.x = vv1;
		abc.y = (vv0 - vv1) * d1;
		abc.z = (vv2 - vv1) * d1;
		x0x1.x = d1 - d0;
		x0x1.y = d1 - d2;
	} else if (d1 * d2 > 0.0f || d0 != 0.0f) {
		/* here we know that d0d1 <=0.0 or that d0!=0.0 */
		abc.x = vv0;
		abc.y = (vv1 - vv0) * d0;
		abc.z = (vv2 - vv0) * d0;
		x0x1.x = d0 - d1;
		x0x1.y = d0 - d2;
	} else if (d1 != 0.0f) {
		abc.x = vv1;
		abc.y = (vv0 - vv1) * d1;
		abc.z = (vv2 - vv1) * d1;
		x0x1.x = d1 - d0;
		x0x1.y = d1 - d2;
	} else if (d2 != 0.0f) {
		abc.x = vv2;
		abc.y = (vv0 - vv2) * d2;
		abc.z = (vv1 - vv2) * d2;
		x0x1.x = d2 - d0;
		x0x1.y = d2 - d1;
	} else {
		/* triangles are coplanar */
		return true;
	}
	return false;
}	


static void sort PARAM(RTemps)
WRITES(RTemps) (float *f ARG_(RTemps))
{  // expected-warning{{Inferred Effect Summary for sort: [reads(rpl([rLOCAL],[])),writes(rpl([p95_RTemps],[]))]}}
	if (f[0] > f[1]) {
		float c = f[0];
		f[0] = f[1];
		f[1] = c;
	}
}


bool intersection_r //PARAM(RVals, RTemps) 
WRITES(RVals, RTemps:*) (
		Vector3f &v0 ARG_(RVals),
		Vector3f &v1 ARG_(RVals),
		Vector3f &v2 ARG_(RVals),
		Vector3f &u0 ARG_(RVals),
		Vector3f &u1 ARG_(RVals),
		Vector3f &u2 ARG_(RVals),
		Vector3f &e1 ARG_(RTemps),
		Vector3f &e2 ARG_(RTemps),
		Vector3f &n1 ARG_(RTemps),
		Vector3f &n2 ARG_(RTemps),
		float *isect1 ARG_(RTemps),
		float *isect2 ARG_(RTemps))
{  // expected-warning{{Inferred Effect Summary for intersection_r: [reads(rpl([p58_RVals],[])),writes(rpl([p59_RTemps],[])),writes(rpl([rLOCAL],[]))]}}
	float d1, d2;
	float du0, du1, du2, dv0, dv1, dv2;
	Vector3f d ARG(RTemps);

	float du0du1, du0du2, dv0dv1, dv0dv2;
	short index;
	float vp0, vp1, vp2;
	float up0, up1, up2;
	float bb, cc, max;
	float xx, yy, xxyy, tmp;

	/* compute plane equation of triangle(v0,v1,v2) */
	v1.subtract(v0, e1);
	v2.subtract(v0, e2);
	e1.cross(e2, n1);
	d1 = -n1.dot(v0);
	/* plane equation 1: n1.X+d1=0 */

	/*
	 * put u0,u1,u2 into plane equation 1 to compute signed distances to the
	 * plane
	 */
	du0 = n1.dot(u0) + d1;
	du1 = n1.dot(u1) + d1;
	du2 = n1.dot(u2) + d1;

	/* coplanarity robustness check */
	if (fabs(du0) < EPSILON)
		du0 = 0.0f;
	if (fabs(du1) < EPSILON)
		du1 = 0.0f;
	if (fabs(du2) < EPSILON)
		du2 = 0.0f;
	du0du1 = du0 * du1;
	du0du2 = du0 * du2;

	if (du0du1 > 0.0f && du0du2 > 0.0f) {
		return false;
	}

	/* compute plane of triangle (u0,u1,u2) */
	u1.subtract(u0, e1);
	u2.subtract(u0, e2);
	e1.cross(e2, n2);
	d2 = -n2.dot(u0);
	/* plane equation 2: n2.X+d2=0 */

	/* put v0,v1,v2 into plane equation 2 */
	dv0 = n2.dot(v0) + d2;
	dv1 = n2.dot(v1) + d2;
	dv2 = n2.dot(v2) + d2;

	if (fabs(dv0) < EPSILON)
		dv0 = 0.0f;
	if (fabs(dv1) < EPSILON)
		dv1 = 0.0f;
	if (fabs(dv2) < EPSILON)
		dv2 = 0.0f;

	dv0dv1 = dv0 * dv1;
	dv0dv2 = dv0 * dv2;

	if (dv0dv1 > 0.0f && dv0dv2 > 0.0f) { /*
										   * same sign on all of them + not
										   * equal 0 ?
										   */
		return false; /* no intersection occurs */
	}

	/* compute direction of intersection line */
	n1.cross(n2, d);

	/* compute and index to the largest component of d */
	max = fabs(d.x);
	index = 0;
	bb = fabs(d.y);
	cc = fabs(d.z);
	if (bb > max) {
		max = bb;
		index = 1;
	}
	if (cc > max) {
		//max = cc;
		vp0 = v0.z;
		vp1 = v1.z;
		vp2 = v2.z;

		up0 = u0.z;
		up1 = u1.z;
		up2 = u2.z;

	} else if (index == 1) {
		vp0 = v0.y;
		vp1 = v1.y;
		vp2 = v2.y;

		up0 = u0.y;
		up1 = u1.y;
		up2 = u2.y;
	} else {
		vp0 = v0.x;
		vp1 = v1.x;
		vp2 = v2.x;

		up0 = u0.x;
		up1 = u1.x;
		up2 = u2.x;
	}

	/* compute interval for triangle 1 */
	Vector3f &abc ARG(RTemps) = e1;
	Vector2f x0x1 ARG(RTemps);
	if (Intersection::newComputeIntervals(vp0, vp1, vp2, dv0, dv1, dv2, dv0dv1, dv0dv2,
			abc, x0x1)) {
		return Intersection::coplanarTriTri(n1, v0, v1, v2, u0, u1, u2);
	}

	/* compute interval for triangle 2 */
	Vector3f &def ARG(RTemps) = e2;
	Vector2f y0y1 ARG(RTemps);
	if (Intersection::newComputeIntervals(up0, up1, up2, du0, du1, du2, du0du1, du0du2,
			def, y0y1)) {
		return Intersection::coplanarTriTri(n1, v0, v1, v2, u0, u1, u2);
	}

	xx = x0x1.x * x0x1.y;
	yy = y0y1.x * y0y1.y;
	xxyy = xx * yy;

	tmp = abc.x * xxyy;
	isect1[0] = tmp + abc.y * x0x1.y * yy;
	isect1[1] = tmp + abc.z * x0x1.x * yy;

	tmp = def.x * xxyy;
	isect2[0] = tmp + def.y * xx * y0y1.y;
	isect2[1] = tmp + def.z * xx * y0y1.x;

	Intersection::sort(isect1);
	Intersection::sort(isect2);

	if (isect1[1] < isect2[0] || isect2[1] < isect1[0]) {
		return false;
	} 
	
	return true;
}

}
