#ifndef COLLISIONTREE_H
#define COLLISIONTREE_H

#include "asap.h"
#include "bounding-full.h"
#include "trimesh-full.h"
#line 8 "collisiontree-full.h"

class REGION(Left, Right, RMeshes) PARAM_(R) CollisionTree {
// R is specific to this node.
// RMesh is for structures shared by all nodes in this tree (mesh & triIndex)
// Root nodes of collision trees should have R == RMesh
// For now, use RMeshes for the meshes of all trees, to avoid needing a second region parameter

protected:
	// children trees
	CollisionTree *left ARG(R:Left, R:Left); // expected-warning{{Inferred region arguments: class CollisionTree *, IN:[p57_R], ArgV:[p57_R,r0_Left]:}}
	CollisionTree *right ARG(R:Right, R:Right); // expected-warning{{Inferred region arguments: class CollisionTree *, IN:[p57_R], ArgV:[p57_R,r1_Right]:}}
	
	// bounding volumes that contain the triangles that the node is handling
	BoundingBox *bounds ARG(R, R); // expected-warning{{Inferred region arguments: class BoundingBox *, IN:[p57_R], ArgV:[p57_R]:}}

    // the list of triangle indices that compose the tree. This list
    // contains all the triangles of the mesh and is shared between
    // all nodes of this tree.
    int *triIndex ARG(RMeshes, RMeshes); // expected-warning{{Inferred region arguments: int *, IN:[p57_R], ArgV:[p57_R]:}}

    // Defines the pointers into the triIndex array that this node is
    // directly responsible for.
    int start ARG(R), end ARG(R); // expected-warning{{Inferred region arguments: int, IN:[p57_R], ArgV::}} // expected-warning{{Inferred region arguments: int, IN:[p57_R], ArgV::}}

    // Required Spatial information
    TriMesh *mesh ARG(RMeshes, RMeshes); // expected-warning{{Inferred region arguments: class TriMesh *, IN:[p57_R], ArgV:[p57_R]:}}
    
public:
	bool intersect PARAM_(R_cT, Rwb) //PARAM_(R_cT, RLists, Rwb)
	//REGION(R_cTWB, RTemps)
	READS(R:*, R_cT:*, Rwb, RMeshes) 
	/*WRITES(RLists, RTemps:*)*/ (
			CollisionTree &collisionTree ARG_(R_cT),
            //ParallelArrayList<RLists> aList, ParallelArrayList<RLists> bList, 
            BoundingBox &myWorldBounds ARG_(Rwb), int cutoff);
};

#endif
