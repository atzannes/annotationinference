#ifndef MATVEC_H
#define MATVEC_H

#include <cmath>
#include "asap.h"
#line 7 "matvec.h"

class PARAM(R) Vector2f;
class PARAM(R) Vector3f;
class PARAM(R) Quaternion;
class PARAM(R) Matrix3f;


class PARAM(R) Vector2f {
public:
    float x ARG(R), y ARG(R);
    Vector2f() : x(0), y(0) {}
};


class PARAM(R) Vector3f {
public:
    float x ARG(R), y ARG(R), z ARG(R);

	Vector3f (): x(0), y(0), z(0) {}
	
	PARAM(P) READS(P)
	Vector3f(const Vector3f &v ARG_(P)) : x(v.x), y(v.y), z(v.z) {} // expected-warning{{Inferred Effect Summary for Vector3f: [reads(rpl([p27_P],[]))]}}
    
    //Vector3f &multLocal ARG(R) WRITES(R) (float scalar);
    
    Vector3f &multLocal ARG(R) PARAM(Rvec)
    READS(Rvec) WRITES(R) (Vector3f &vec ARG_(Rvec));

    Vector3f &addLocal ARG(R) PARAM(Rvec)
    READS(Rvec) WRITES(R) (Vector3f &vec ARG_(Rvec));
    
    Vector3f &set ARG(R) PARAM(Rvect) 
    READS(Rvect) WRITES(R) (
    	const Vector3f &vect ARG_(Rvect));
    
    Vector3f &set ARG(R) WRITES(R) (float x, float y, float z);

	Vector3f &mult ARG(Rstore) PARAM(Rvec, Rstore)
	READS(R, Rvec) WRITES(Rstore) (
		Vector3f &vec ARG_(Rvec), Vector3f &store ARG_(Rstore));

    Vector3f &subtract ARG(Rresult) PARAM(Rvec, Rresult)
    READS(R, Rvec) WRITES(Rresult) (
    	Vector3f &vec ARG_(Rvec), Vector3f &result ARG_(Rresult));

	float dot PARAM(Rvec)
	READS(R, Rvec) (Vector3f &vec ARG_(Rvec));

	Vector3f &cross ARG(Rresult) PARAM(Rresult) 
	READS(R) WRITES(Rresult) (
		float otherX, float otherY, float otherZ, Vector3f &result ARG_(Rresult));

	Vector3f &cross ARG(Rresult) PARAM(Rv, Rresult)
	READS(R, Rv) WRITES(Rresult) (
		Vector3f &v ARG_(Rv), Vector3f &result ARG_(Rresult));

	static bool isValidVector PARAM(Rvector) 
	READS(Rvector) (Vector3f &vector ARG_(Rvector))
	{ // expected-warning{{Inferred Effect Summary for isValidVector: [reads(rpl([p39_Rvector],[])),reads(rpl([rLOCAL],[]))]}}
		if (std::isnan(vector.x) ||
			std::isnan(vector.y) ||
			std::isnan(vector.z)) return false;
		if (std::isinf(vector.x) ||
			std::isinf(vector.y) ||
			std::isinf(vector.z)) return false;
		return true;
	}

	float *toArray ARG(Rfloats) PARAM(Rfloats)
	READS(R) WRITES(Rfloats) (
		float *floats ARG_(Rfloats));
};


class PARAM(R) Quaternion {
public:
	float x ARG(R), y ARG(R), z ARG(R), w ARG(R);

	Vector3f &mult ARG(Rstore) PARAM(Rv, Rstore)
	READS(R, Rv) WRITES(Rstore) (
		Vector3f &v ARG_(Rv), Vector3f &store ARG_(Rstore));
    
    Matrix3f &toRotationMatrix ARG(Rresult) PARAM(Rresult)
    READS(R) WRITES(Rresult) (Matrix3f &result ARG_(Rresult));
    
    float norm READS(R) ();
};


class PARAM(R) Matrix3f {
public:
	float m00 ARG(R), m01 ARG(R), m02 ARG(R);
    float m10 ARG(R), m11 ARG(R), m12 ARG(R);
    float m20 ARG(R), m21 ARG(R), m22 ARG(R);
    
    void set PARAM(Rquaternion)
	READS(Rquaternion) WRITES(R) (
		Quaternion &quaternion ARG_(Rquaternion));
	
	Vector3f &mult ARG(Rproduct) PARAM(Rvec, Rproduct)
	READS(R, Rvec) WRITES(Rproduct) (
		Vector3f &vec ARG_(Rvec), Vector3f &product ARG_(Rproduct));
};

#endif
