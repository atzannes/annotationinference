#include "matvec.h"
#line 3 "matvec.cpp"

// XXX If various function definitions in this file appear before the calls to them
// in intersection.cpp in the single-file version, it will produce spurious errors.

Vector3f & Vector3f::multLocal ARG(R) //PARAM(Rvec)
READS(Rvec) WRITES(R) (Vector3f &vec ARG_(Rvec))
{  // expected-warning{{Inferred Effect Summary for multLocal: [reads(rpl([p28_Rvec],[])),writes(rpl([p26_R],[]))]}}
	x *= vec.x;
	y *= vec.y;
	z *= vec.z;
	return *this;
}

Vector3f & Vector3f::addLocal ARG(R) //PARAM(Rvec)
READS(Rvec) WRITES(R) (Vector3f &vec ARG_(Rvec))
{  // expected-warning{{Inferred Effect Summary for addLocal: [reads(rpl([p29_Rvec],[])),writes(rpl([p26_R],[]))]}}
	x += vec.x;
	y += vec.y;
	z += vec.z;
	return *this;
}

Vector3f & Vector3f::set ARG(R) //PARAM(Rvect) 
READS(Rvect) WRITES(R) (
    	const Vector3f &vect ARG_(Rvect))
{  // expected-warning{{Inferred Effect Summary for set: [reads(rpl([p30_Rvect],[])),writes(rpl([p26_R],[]))]}}
	x = vect.x;
	y = vect.y;
	z = vect.z;
	return *this;
}

Vector3f & Vector3f::set ARG(R) WRITES(R) (float x, float y, float z)
{  // expected-warning{{Inferred Effect Summary for set: [reads(rpl([rLOCAL],[])),writes(rpl([p26_R],[]))]}}
	this->x = x;
	this->y = y;
	this->z = z;
	return *this;
}

Vector3f & Vector3f::mult ARG(Rstore) //PARAM(Rvec, Rstore)
READS(R, Rvec) WRITES(Rstore) (
		Vector3f &vec ARG_(Rvec), Vector3f &store ARG_(Rstore))
{  // expected-warning{{Inferred Effect Summary for mult: [reads(rpl([p26_R],[])),reads(rpl([p31_Rvec],[])),reads(rpl([rLOCAL],[])),writes(rpl([p32_Rstore],[]))]}}
	return store.set(x * vec.x, y * vec.y, z * vec.z);
}

Vector3f & Vector3f::subtract ARG(Rresult) //PARAM(Rvec, Rresult)
READS(R, Rvec) WRITES(Rresult) (
    	Vector3f &vec ARG_(Rvec), Vector3f &result ARG_(Rresult))
{  // expected-warning{{Inferred Effect Summary for subtract: [reads(rpl([p26_R],[])),reads(rpl([p33_Rvec],[])),writes(rpl([p34_Rresult],[]))]}}
	result.x = x - vec.x;
	result.y = y - vec.y;
	result.z = z - vec.z;
	return result;
}

float Vector3f::dot //PARAM(Rvec)
READS(R, Rvec) (Vector3f &vec ARG_(Rvec))
{  // expected-warning{{Inferred Effect Summary for dot: [reads(rpl([p26_R],[])),reads(rpl([p35_Rvec],[]))]}}
	return x * vec.x + y * vec.y + z * vec.z;
}

Vector3f & Vector3f::cross ARG(Rresult) //PARAM(Rresult) 
READS(R) WRITES(Rresult) (
		float otherX, float otherY, float otherZ, Vector3f &result ARG_(Rresult))
{  // expected-warning{{Inferred Effect Summary for cross: [reads(rpl([p26_R],[])),reads(rpl([rLOCAL],[])),writes(rpl([p36_Rresult],[]))]}}
	float resX = ((y * otherZ) - (z * otherY)); 
	float resY = ((z * otherX) - (x * otherZ));
	float resZ = ((x * otherY) - (y * otherX));
	result.set(resX, resY, resZ);
	return result;
}

Vector3f & Vector3f::cross ARG(Rresult) //PARAM(Rv, Rresult)
READS(R, Rv) WRITES(Rresult) (
		Vector3f &v ARG_(Rv), Vector3f &result ARG_(Rresult))
{  // expected-warning{{Inferred Effect Summary for cross: [reads(rpl([p26_R],[])),reads(rpl([p37_Rv],[])),reads(rpl([rLOCAL],[])),writes(rpl([p38_Rresult],[]))]}}
	// XXX Below version gives (bogus?) checker error
	//return cross(v.x, v.y, v.z, result);
	
	float resX = ((y * v.z) - (z * v.y)); 
	float resY = ((z * v.x) - (x * v.z));
	float resZ = ((x * v.y) - (y * v.x));
	result.set(resX, resY, resZ);
	return result;
}

float * Vector3f::toArray ARG(Rfloats) //PARAM(Rfloats)
READS(R) WRITES(Rfloats) (
		float *floats ARG_(Rfloats))
{  // expected-warning{{Inferred Effect Summary for toArray: [reads(rpl([p26_R],[])),reads(rpl([rLOCAL],[])),writes(rpl([p40_Rfloats],[]))]}}
	floats[0] = x;
	floats[1] = y;
	floats[2] = z;
	return floats;
}



Matrix3f &Quaternion::toRotationMatrix ARG(Rresult) //PARAM(Rresult)
READS(R) WRITES(Rresult) (Matrix3f &result ARG_(Rresult))
{  // expected-warning{{Inferred Effect Summary for toRotationMatrix: [reads(rpl([p41_R],[])),reads(rpl([rLOCAL],[])),writes(rpl([p44_Rresult],[]))]}}
	float norm = this->norm();
	// we explicitly test norm against one here, saving a division
	// at the cost of a test and branch.  Is it worth it?
	float s = (norm==1.0f) ? 2.0f : (norm > 0.0f) ? 2.0f/norm : 0;
	
	// compute xs/ys/zs first to save 6 multiplications, since xs/ys/zs
	// will be used 2-4 times each.
	float xs      = x * s;
	float ys      = y * s;
	float zs      = z * s;
	float xx      = x * xs;
	float xy      = x * ys;
	float xz      = x * zs;
	float xw      = w * xs;
	float yy      = y * ys;
	float yz      = y * zs;
	float yw      = w * ys;
	float zz      = z * zs;
	float zw      = w * zs;

	// using s=2/norm (instead of 1/norm) saves 9 multiplications by 2 here
	result.m00  = 1 - ( yy + zz );
	result.m01  =     ( xy - zw );
	result.m02  =     ( xz + yw );
	result.m10  =     ( xy + zw );
	result.m11  = 1 - ( xx + zz );
	result.m12  =     ( yz - xw );
	result.m20  =     ( xz - yw );
	result.m21  =     ( yz + xw );
	result.m22  = 1 - ( xx + yy );

	return result;
}

Vector3f &Quaternion::mult ARG(Rstore) //PARAM(Rv, Rstore)
READS(R, Rv) WRITES(Rstore) (
		Vector3f &v ARG_(Rv), Vector3f &store ARG_(Rstore))
{  // expected-warning{{Inferred Effect Summary for mult: [reads(rpl([p41_R],[])),reads(rpl([p42_Rv],[])),reads(rpl([rLOCAL],[])),writes(rpl([p43_Rstore],[]))]}}
	if (v.x == 0 && v.y == 0 && v.z == 0) {
		store.x = store.y = store.z = 0;
	} else {
		float vx = v.x, vy = v.y, vz = v.z;
		store.x = w * w * vx + 2 * y * w * vz - 2 * z * w * vy + x * x
				* vx + 2 * y * x * vy + 2 * z * x * vz - z * z * vx - y
				* y * vx;
		store.y = 2 * x * y * vx + y * y * vy + 2 * z * y * vz + 2 * w
				* z * vx - z * z * vy + w * w * vy - 2 * x * w * vz - x
				* x * vy;
		store.z = 2 * x * z * vx + 2 * y * z * vy + z * z * vz - 2 * w
				* y * vx - y * y * vz + 2 * w * x * vy - x * x * vz + w
				* w * vz;
	}
	return store;
}

float Quaternion::norm READS(R) ()
{  // expected-warning{{Inferred Effect Summary for norm: [reads(rpl([p41_R],[]))]}}
	return w * w + x * x + y * y + z * z;
}


void Matrix3f::set //PARAM(Rquaternion)
READS(Rquaternion) WRITES(R) (
		Quaternion &quaternion ARG_(Rquaternion))
{  // expected-warning{{Inferred Effect Summary for set: [reads(rpl([p46_Rquaternion],[])),reads(rpl([rLOCAL],[])),writes(rpl([p45_R],[]))]}}
	// XXX This gives a (bogus?) checker error, so duplicate the code for now
	//quaternion.toRotationMatrix(*this);
	
	float norm = quaternion.norm();
	// we explicitly test norm against one here, saving a division
	// at the cost of a test and branch.  Is it worth it?
	float s = (norm==1.0f) ? 2.0f : (norm > 0.0f) ? 2.0f/norm : 0;
	
	// compute xs/ys/zs first to save 6 multiplications, since xs/ys/zs
	// will be used 2-4 times each.
	float xs      = quaternion.x * s;
	float ys      = quaternion.y * s;
	float zs      = quaternion.z * s;
	float xx      = quaternion.x * xs;
	float xy      = quaternion.x * ys;
	float xz      = quaternion.x * zs;
	float xw      = quaternion.w * xs;
	float yy      = quaternion.y * ys;
	float yz      = quaternion.y * zs;
	float yw      = quaternion.w * ys;
	float zz      = quaternion.z * zs;
	float zw      = quaternion.w * zs;

	// using s=2/norm (instead of 1/norm) saves 9 multiplications by 2 here
	m00  = 1 - ( yy + zz );
	m01  =     ( xy - zw );
	m02  =     ( xz + yw );
	m10  =     ( xy + zw );
	m11  = 1 - ( xx + zz );
	m12  =     ( yz - xw );
	m20  =     ( xz - yw );
	m21  =     ( yz + xw );
	m22  = 1 - ( xx + yy );
}

Vector3f &Matrix3f::mult ARG(Rproduct) //PARAM(Rvec, Rproduct)
READS(R, Rvec) WRITES(Rproduct) (
		Vector3f &vec ARG_(Rvec), Vector3f &product ARG_(Rproduct))
{  // expected-warning{{Inferred Effect Summary for mult: [reads(rpl([p45_R],[])),reads(rpl([p47_Rvec],[])),reads(rpl([rLOCAL],[])),writes(rpl([p48_Rproduct],[]))]}}
	float x = vec.x;
	float y = vec.y;
	float z = vec.z;

	product.x = m00 * x + m01 * y + m02 * z;
	product.y = m10 * x + m11 * y + m12 * z;
	product.z = m20 * x + m21 * y + m22 * z;
	return product;
}
