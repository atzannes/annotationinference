#ifndef TRIMESH_H
#define TRIMESH_H

#include "asap.h"
#include "matvec.h"
#line 7 "trimesh.h"

class PARAM(RMesh) TriMesh {
public:
	float * vertexArray ARG(RMesh, RMesh);
	int * indexArray ARG(RMesh, RMesh);
	int triangleCount ARG(RMesh);
	
	Quaternion worldRotation ARG(RMesh);
	Vector3f worldScale ARG(RMesh);
	Vector3f worldTranslation ARG(RMesh);
	
	void getTriangle PARAM(R) 
	READS(RMesh) WRITES(R:*) (
	 	int i, Vector3f vertices ARG_(R) []);
};

#endif
