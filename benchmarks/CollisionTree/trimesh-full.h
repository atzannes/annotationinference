#ifndef TRIMESH_H
#define TRIMESH_H

#include "asap.h"
#include "matvec-full.h"
#line 7 "trimesh-full.h"

class TriMesh { // expected-warning{{Inferred region arguments: class TriMesh &&, IN:<empty>, ArgV:[p46_TriMesh]:}} // expected-warning{{Inferred region arguments: class TriMesh &(class TriMesh &&), IN:<empty>, ArgV:[p46_TriMesh]}}
public:
	float * vertexArray ARG(RMesh, RMesh); // expected-warning{{Inferred region arguments: float *, IN:[p46_TriMesh], ArgV:[p46_TriMesh]:}}
	int * indexArray ARG(RMesh, RMesh); // expected-warning{{Inferred region arguments: int *, IN:[p46_TriMesh], ArgV:[p46_TriMesh]:}}
	int triangleCount ARG(RMesh); // expected-warning{{Inferred region arguments: int, IN:[p46_TriMesh], ArgV::}}
	
	Quaternion worldRotation ARG(RMesh); // expected-warning{{Inferred region arguments: class Quaternion, IN:<empty>, ArgV:[p46_TriMesh]:}}
	Vector3f worldScale ARG(RMesh); // expected-warning{{Inferred region arguments: class Vector3f, IN:<empty>, ArgV:[p46_TriMesh]:}}
	Vector3f worldTranslation ARG(RMesh); // expected-warning{{Inferred region arguments: class Vector3f, IN:<empty>, ArgV:[p46_TriMesh]:}}
	
	void getTriangle PARAM_(R) 
	READS(RMesh) WRITES(R:*) (
	 	int i, Vector3f vertices ARG_(R) []);
};

#endif
