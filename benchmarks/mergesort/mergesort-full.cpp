//#include <cstddef>
//#include <iostream>
//#include "../include/tbb.h"
#include "asap.h"

typedef unsigned int size_t;

namespace tbb {
    template<typename Func0, typename Func1>
    void parallel_invoke[[asap::param("P1,P2,P3,P4")]](const Func0 &F0 [[asap::arg("P1,P2")]], const Func1 &F1 [[asap::arg("P3,P4")]]);
}

using namespace std;

void sort PARAM(Rin,Rout) READS(Rin) WRITES(Rout:*) (
		int *in ARG_(Rin), int *out ARG_(Rout), size_t n);

class PARAM(RinC, RoutC) SortInvoker // expected-warning{{Inferred region arguments: class SortInvoker &&, IN:<empty>, ArgV:[p10_RinC], [p10_RinC]}} expected-warning{{Inferred region arguments: const class SortInvoker &, IN:<empty>, ArgV:[p10_RinC], [p10_RinC]}}
{
	int *in ARG(RinC, RinC); // expected-warning{{Inferred region arguments: int *, IN:[p10_RinC], ArgV:[p10_RinC]}}
	int *out ARG(RoutC, RoutC); // expected-warning{{Inferred region arguments: int *, IN:[p10_RinC], ArgV:[p11_RoutC]}}
	size_t n; // expected-warning{{Inferred region arguments: size_t, IN:[p10_RinC], ArgV::}}
	
public:
	SortInvoker(int *inCons ARG(RinC), int *outCons ARG(RoutC), size_t nCons) : in(inCons), out(outCons), n(nCons) {} // expected-warning{{Inferred region arguments: int *, IN:Local, ArgV:[p10_RinC]: }} expected-warning{{Inferred region arguments: int *, IN:Local, ArgV:[p11_RoutC]:}} expected-warning{{Inferred Effect Summary for SortInvoker: [reads(rpl([rLOCAL],[]))]}}
	
	void operator() READS(RinC) WRITES(RoutC:*) () const
	{ // expected-warning{{Inferred Effect Summary for operator(): [reads(rpl([p10_RinC],[])),writes(rpl([p11_RoutC,rSTAR],[])),writes(rpl([rLOCAL],[]))]:}}
		sort(in, out, n);
	}
}; // end class SortInvoker

void sort REGION(Ra, Rb) /*param(Rin,Rout) READS(Rin) WRITES(Rout:* )*/ (
		int *in ARG_(Rin), int *out ARG_(Rout), size_t n)
{ // expected-warning{{Inferred Effect Summary for sort: [reads(rpl([p8_Rin],[])),writes(rpl([p9_Rout],[])),writes(rpl([p9_Rout,r11_A,rSTAR],[])),writes(rpl([p9_Rout,r12_B,rSTAR],[])),writes(rpl([rLOCAL],[]))]}}
	if (n == 0) return;
	if (n == 1) {
		out[0] = in[0];
		return;
	}

	size_t aSize = n / 2;
	size_t bSize = n - aSize;
	int A ARG(Rout:Ra) [aSize]; // expected-warning{{Inferred region arguments: int [aSize], IN:<empty>, ArgV:[p9_Rout,r11_A]}}
	int B ARG(Rout:Rb) [bSize]; // expected-warning{{Inferred region arguments: int [bSize], IN:<empty>, ArgV:[p9_Rout,r12_B]}}

	SortInvoker aFn ARG(Rin, Rout:Ra) (in, A, aSize); // expected-warning{{Inferred region arguments: class SortInvoker, IN:<empty>, ArgV:[p8_Rin], [p9_Rout,r11_A]}}
	SortInvoker bFn ARG(Rin, Rout:Rb) (in + aSize, B, bSize); // expected-warning{{Inferred region arguments: class SortInvoker, IN:<empty>, ArgV:[p8_Rin], [p9_Rout,r12_B]}}
	tbb::parallel_invoke(aFn, bFn);

	// now merge
	size_t a = 0, b = 0;
	size_t k = 0;

	while (a < aSize && b < bSize) {
		if (A[a] < B[b]) {
			out[k++] = A[a++]; // reads Rout:Ra  writes Rout
		} else {
			out[k++] = B[b++]; // reads Rout:Rb  writes Rout
		}
	}

	while (a < aSize) {
		out[k++] = A[a++];
	}
	while (b < bSize) {
		out[k++] = B[b++];
	}
}

int main (int argc, char** argv) // expected-warning{{Inferred region arguments: char **, IN:Local, ArgV:[r17_argv], [r18_argv]}}
{ // expected-warning{{Inferred Effect Summary for main: [reads(rpl([r19_in],[])),writes(rpl([r20_out],[])),writes(rpl([r20_out,r11_A,rSTAR],[])),writes(rpl([r20_out,r12_B,rSTAR],[])),writes(rpl([rLOCAL],[]))]}}
	const size_t n = 13;

	int in[n] = {134,3453,3455,67,678,4356,345,4635,4536,348,564,345,3245}; // expected-warning{{Inferred region arguments: int [13], IN:<empty>, ArgV:[r19_in]}}
	int out[n]; // expected-warning{{Inferred region arguments: int [13], IN:<empty>, ArgV:[r20_out]}}
	sort(in, out, n);
//	for (size_t i = 0; i < n; i++) {
//		cout << out[i] << " ";
//	}
//	cout << endl;
	return 0;
}
