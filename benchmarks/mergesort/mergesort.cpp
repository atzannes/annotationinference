#include <cstddef>
//#include <iostream>
//#include "../include/tbb.h"
#include "asap.h"

namespace tbb {
    template<typename Func0, typename Func1>
    void parallel_invoke[[asap::param("P1,P2,P3,P4")]](const Func0 &F0 [[asap::arg("P1,P2")]], const Func1 &F1 [[asap::arg("P3,P4")]]);
}

using namespace std;

void sort PARAM(Rin,Rout) READS(Rin) WRITES(Rout:*) (
		int *in ARG_(Rin), int *out ARG_(Rout), size_t n);

class PARAM(RinC, RoutC) SortInvoker
{
	int *in ARG(RinC, RinC);
	int *out ARG(RoutC, RoutC);
	size_t n;
	
public:
	SortInvoker(int *inCons ARG(RinC), int *outCons ARG(RoutC), size_t nCons) : in(inCons), out(outCons), n(nCons) {} // expected-warning{{Inferred Effect Summary for SortInvoker: [reads(rpl([rLOCAL],[]))]}}
	
	void operator() READS(RinC) WRITES(RoutC:*) () const
	{ // expected-warning{{Inferred Effect Summary for operator(): [reads(rpl([p11_RinC],[])),writes(rpl([p12_RoutC,rSTAR],[])),writes(rpl([rLOCAL],[]))]}}
		sort(in, out, n);
	}
}; // end class SortInvoker

void sort REGION(Ra, Rb) /*PARAM(Rin,Rout) READS(Rin) WRITES(Rout:* )*/ (
		int *in ARG_(Rin), int *out ARG_(Rout), size_t n)
{ // expected-warning{{Inferred Effect Summary for sort: [reads(rpl([p9_Rin],[])),writes(rpl([p10_Rout],[])),writes(rpl([p10_Rout,r0_Ra,rSTAR],[])),writes(rpl([p10_Rout,r1_Rb,rSTAR],[])),writes(rpl([rLOCAL],[]))]:}}
	if (n == 0) return;
	if (n == 1) {
		out[0] = in[0];
		return;
	}

	size_t aSize = n / 2;
	size_t bSize = n - aSize;
	int A ARG(Rout:Ra) [aSize];
	int B ARG(Rout:Rb) [bSize];

	SortInvoker aFn ARG(Rin, Rout:Ra) (in, A, aSize);
	SortInvoker bFn ARG(Rin, Rout:Rb) (in + aSize, B, bSize);
	tbb::parallel_invoke(aFn, bFn);

	// now merge
	size_t a = 0, b = 0;
	size_t k = 0;

	while (a < aSize && b < bSize) {
		if (A[a] < B[b]) {
			out[k++] = A[a++]; // reads Rout:Ra  writes Rout
		} else {
			out[k++] = B[b++]; // reads Rout:Rb  writes Rout
		}
	}

	while (a < aSize) {
		out[k++] = A[a++];
	}
	while (b < bSize) {
		out[k++] = B[b++];
	}
}

int main (int argc, char** argv)
{ // expected-warning{{Inferred Effect Summary for main: [writes(rpl([rLOCAL],[])),writes(rpl([rLOCAL,r0_Ra,rSTAR],[])),writes(rpl([rLOCAL,r1_Rb,rSTAR],[]))]}}
	const size_t n = 13;

	int in[n] = {134,3453,3455,67,678,4356,345,4635,4536,348,564,345,3245};
	int out[n];
	sort(in, out, n);
//	for (size_t i = 0; i < n; i++) {
//		cout << out[i] << " ";
//	}
//	cout << endl;
	return 0;
}
