#ifndef ASP_BENCHMARK_QUADTREE_H
#define ASP_BENCHMARK_QUADTREE_H

#include "asap.h"
namespace asp {
namespace benchmark {
  static const int MAXCOORD = 1000000000;

  struct PARAM(P) Point {
    int x ARG(P);
    int y ARG(P);
    Point() : x(0), y(0) {}
    PARAM(S) Point (const Point &P ARG(S)) : x(P.x), y(P.y) {}
    Point &operator= PARAM(S2) (const Point &P ARG(S2)) noexcept {
      this->x = P.x;
      this->y = P.y;
      return *this;
    }

    Point(int x, int y) : x(x), y(y) {}
  };

  struct PARAM(Q) AABB {
    int x ARG(Q), y ARG(Q), w ARG(Q), h ARG(Q);
    AABB();
    PARAM(U) AABB (const AABB &AB ARG(U)) : x(AB.x), y(AB.y), w(AB.w), h(AB.h) {}
    AABB &operator= PARAM(V) (const AABB &AB ARG(V)) noexcept {
      this->x = AB.x;
      this->y = AB.y;
      this->w = AB.w;
      this->h = AB.h;
      return *this;
    }

    AABB(int x, int y, int w, int h);
    bool contains PARAM(T) (Point &p ARG(T));
  };

  class [[asap::param("Class"), asap::region("R1, R2, R3, R4")]] QuadTree {
    static const int CAPACITY = 4;
    Point points ARG(Class) [CAPACITY];
    AABB bounds ARG(Class);
    int size ARG(Class);

    QuadTree *nw ARG(Class, R1);
    QuadTree *ne ARG(Class, R2);
    QuadTree *sw ARG(Class, R3);
    QuadTree *se ARG(Class, R4);
    
     class InsertFunctor {
      QuadTree *mNode;
      Point mPoint;
    public:
     PARAM(T) InsertFunctor(QuadTree *node ARG(T), Point p) : mNode(node), mPoint(p){
      }
      void operator()() const {
        mNode->insert(mPoint);
      }
    };

  

   public:
    QuadTree (AABB bounds ARG(Class));

    bool insert(Point p ARG(Class));
    bool subDivide PARAM(Class) ();
  };
};
};

#endif

