#include "QuadTree.h"
#include <iostream>
#include <chrono>

using namespace asp::benchmark;

int main() {
  AABB box(0, 0, MAXCOORD, MAXCOORD);
  QuadTree qtree(box);

  std::cout << "Start executing parallel quadtree construciton." << std::endl;
  
  auto start_time = std::chrono::high_resolution_clock::now();

  const int maxPoints = 10000000;
  for (int i = 0; i < maxPoints; ++i)
    qtree.insert(Point(rand() % MAXCOORD, 
    rand() % MAXCOORD));
  
  auto end_time = std::chrono::high_resolution_clock::now();
  auto duration = std::chrono::duration_cast<std::chrono::seconds>(end_time - start_time).count();
  
  std::cout << "Construction took " << duration << " seconds." << std::endl;
}
