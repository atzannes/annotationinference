namespace tbb {
    template<typename Func0, typename Func1, typename Func2, typename Func3>
    void parallel_invoke [[asap::param("P0,P1,P2,P3")]]
                         (const Func0 &F0 [[asap::arg("P0")]], const Func1 &F1 [[asap::arg("P1")]],
                          const Func2 &F2 [[asap::arg("P2")]], const Func3 &F3 [[asap::arg("P3")]]);
}

#include "asap.h"

namespace asp {
namespace benchmark {
  static const int MAXCOORD = 1000000000;

  struct Point {
    int x ARG(P);  // expected-warning{{Inferred region arguments: int, IN:[p8_Point], ArgV::}}
    int y ARG(P);  // expected-warning{{Inferred region arguments: int, IN:[p8_Point], ArgV::}}
    Point() : x(0), y(0) {}
    PARAM(S) READS(S) Point (const Point &P ARG(S)) : x(P.x), y(P.y) {} // expected-warning{{Inferred region arguments: const struct asp::benchmark::Point &, IN:<empty>, ArgV:[p12_P]}} expected-warning{{Inferred Effect Summary for Point: [reads(rpl([p12_P],[]))]}}
    Point &operator= PARAM(S2) ARG(P) WRITES(P) READS(S2) (const Point &P ARG(S2)) noexcept { // expected-warning{{Inferred region arguments: const struct asp::benchmark::Point &, IN:<empty>, ArgV:[p13_P]}} expected-warning{{Inferred region arguments: struct asp::benchmark::Point &(const struct asp::benchmark::Point &) noexcept, IN:<empty>, ArgV:[p8_Point]:}} expected-warning{{Inferred Effect Summary for operator=: [reads(rpl([p13_P],[])),writes(rpl([p8_Point],[]))]}}
      this->x = P.x;
      this->y = P.y;
      return *this;
    }

    Point(int x, int y) : x(x), y(y) {} // expected-warning{{Inferred Effect Summary for Point: [reads(rpl([rLOCAL],[]))]}}
  };

  struct AABB {
    int x ARG(Q), // expected-warning{{Inferred region arguments: int, IN:[p9_AABB], ArgV::}}
        y ARG(Q), // expected-warning{{Inferred region arguments: int, IN:[p9_AABB], ArgV::}}
        w ARG(Q), // expected-warning{{Inferred region arguments: int, IN:[p9_AABB], ArgV::}}
        h ARG(Q); // expected-warning{{Inferred region arguments: int, IN:[p9_AABB], ArgV::}}

    AABB() : x(0), y(0), w(0), h(0) {}
    AABB(int x, int y, int w, int h) : x(x), y(y), w(w), h(h) {} // expected-warning{{Inferred Effect Summary for AABB: [reads(rpl([rLOCAL],[]))]}}

    PARAM(U) READS(U) AABB (const AABB &AB ARG(U)) : x(AB.x), y(AB.y), w(AB.w), h(AB.h) {} // expected-warning{{Inferred region arguments: const struct asp::benchmark::AABB &, IN:<empty>, ArgV:[p14_AB]}} expected-warning{{Inferred Effect Summary for AABB: [reads(rpl([p14_AB],[]))]}}
    AABB &operator= PARAM(V) ARG(Q) WRITES(Q) READS(V) (const AABB &AB ARG(V)) noexcept {  // expected-warning{{Inferred region arguments: const struct asp::benchmark::AABB &, IN:<empty>, ArgV:[p15_AB]}} expected-warning{{Inferred region arguments: struct asp::benchmark::AABB &(const struct asp::benchmark::AABB &) noexcept, IN:<empty>, ArgV:[p9_AABB]}} expected-warning{{Inferred Effect Summary for operator=: [reads(rpl([p15_AB],[])),writes(rpl([p9_AABB],[]))]}} 
      this->x = AB.x;
      this->y = AB.y;
      this->w = AB.w;
      this->h = AB.h;
      return *this;
    }

    bool contains PARAM(T) READS(Q,T) (Point &p ARG(T)) { // expected-warning{{Inferred region arguments: struct asp::benchmark::Point &, IN:<empty>, ArgV:[p16_p]:}} expected-warning{{Inferred Effect Summary for contains: [reads(rpl([p16_p],[])),reads(rpl([p9_AABB],[]))]}}
      return (p.x >= x && p.y >= y && p.x < x + w && p.y < y + h);
    }
  };

  class QuadTree { // expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree &&, IN:<empty>, ArgV:[p24_UnNamed]:}} expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree &&, IN:<empty>, ArgV:[p25_UnNamed]}} // expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree &(class asp::benchmark::QuadTree &&), IN:<empty>, ArgV:[p25_UnNamed]}} // expected-warning{{Inferred region arguments: const class asp::benchmark::QuadTree &, IN:<empty>, ArgV:[p26_UnNamed]}}
    static const int CAPACITY = 4;
    Point points ARG(Class) [CAPACITY]; // expected-warning{{Inferred region arguments: struct asp::benchmark::Point [4], IN:<empty>, ArgV:[p10_QuadTree,rGLOBAL]}}
    AABB bounds ARG(Class); // expected-warning{{Inferred region arguments: struct asp::benchmark::AABB, IN:<empty>, ArgV:[p10_QuadTree]}}
    int size ARG(Class); // expected-warning{{Inferred region arguments: int, IN:[p10_QuadTree,rGLOBAL], ArgV:}}

    QuadTree *nw ARG(Class, Class:R1); // expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree *, IN:[p10_QuadTree,rGLOBAL], ArgV:[p10_QuadTree,r17_nw]}}
    QuadTree *ne ARG(Class, Class:R2); // expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree *, IN:[p10_QuadTree,rGLOBAL], ArgV:[p10_QuadTree,r19_ne]}}
    QuadTree *sw ARG(Class, Class:R3); // expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree *, IN:[p10_QuadTree,rGLOBAL], ArgV:[p10_QuadTree,r21_sw]}}
    QuadTree *se ARG(Class, Class:R4); // expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree *, IN:[p10_QuadTree,rGLOBAL], ArgV:[p10_QuadTree,r23_se]}}

    class InsertFunctor { // expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree::InsertFunctor &&, IN:<empty>, ArgV:[p19_UnNamed]}} expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree::InsertFunctor &&, IN:<empty>, ArgV:[p20_UnNamed]}} // expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree::InsertFunctor &(class asp::benchmark::QuadTree::InsertFunctor &&), IN:<empty>, ArgV:[p20_UnNamed]}} // expected-warning{{Inferred region arguments: const class asp::benchmark::QuadTree::InsertFunctor &, IN:<empty>, ArgV:[p21_UnNamed]}}
      QuadTree *mNode ARG(Local, T); // expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree *, IN:[p11_InsertFunctor], ArgV:[p11_InsertFunctor]}}
      Point mPoint ARG(T); // expected-warning{{Inferred region arguments: struct asp::benchmark::Point, IN:<empty>, ArgV:[p11_InsertFunctor]}}
    public:
      InsertFunctor (QuadTree *node ARG(T), Point p ARG(T)) : mNode(node), mPoint(p){}  // expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree *, IN:Local, ArgV:[p11_InsertFunctor]}} expected-warning{{Inferred region arguments: struct asp::benchmark::Point, IN:<empty>, ArgV:[p17_node]}} // expected-warning{{Inferred Effect Summary for InsertFunctor: [reads(rpl([rLOCAL],[]))]}}//FIXME:reads p17_node

      void operator() WRITES(T:*) READS(Global) () const { // expected-warning{{Inferred Effect Summary for operator(): [reads(rpl([p11_InsertFunctor,rSTAR],[])),reads(rpl([rGLOBAL],[])),reads(rpl([rLOCAL],[])),writes(rpl([p11_InsertFunctor,rSTAR,rGLOBAL],[]))]}}
        mNode->insert(mPoint);
      }
    };



  public:
    PARAM(P) READS(P) WRITES(Class) QuadTree(AABB aabb ARG(P)) : size(0), nw(0), ne(0), sw(0), se(0) { // expected-warning{{Inferred region arguments: struct asp::benchmark::AABB, IN:<empty>, ArgV:[p22_aabb]}} expected-warning{{Inferred Effect Summary for QuadTree: [reads(rpl([p22_aabb],[])),writes(rpl([p10_QuadTree],[]))]}} 
      bounds = aabb;
    }

    bool subDivide WRITES(Class) (); 

    bool insert WRITES(Class:*) READS(Global) (Point p ARG(Class)) { //expected-warning{{Inferred region arguments: struct asp::benchmark::Point, IN:<empty>, ArgV:[p23_p]}} expected-warning{{Inferred Effect Summary for insert: [reads(rpl([p10_QuadTree],[])),reads(rpl([p10_QuadTree,r17_nw,rSTAR],[])),reads(rpl([p10_QuadTree,r19_ne,rSTAR],[])),reads(rpl([p10_QuadTree,r21_sw,rSTAR],[])),reads(rpl([p10_QuadTree,r23_se,rSTAR],[])),reads(rpl([p23_p],[])),reads(rpl([rGLOBAL],[])),reads(rpl([rLOCAL],[])),writes(rpl([p10_QuadTree,r17_nw,rSTAR,rGLOBAL],[])),writes(rpl([p10_QuadTree,r19_ne,rSTAR,rGLOBAL],[])),writes(rpl([p10_QuadTree,r21_sw,rSTAR,rGLOBAL],[])),writes(rpl([p10_QuadTree,r23_se,rSTAR,rGLOBAL],[])),writes(rpl([p10_QuadTree,rGLOBAL],[]))]:}}
      if (!bounds.contains(p))
        return false;
      if (size < CAPACITY) {
        points[size++] = p;
        return true;
      }

      if (!nw)
        subDivide();

      InsertFunctor fnw ARG(Class:R1) (nw, p),  // expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree::InsertFunctor, IN:<empty>, ArgV:[p10_QuadTree,r17_nw]}}
                    fne ARG(Class:R2) (ne, p),  // expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree::InsertFunctor, IN:<empty>, ArgV:[p10_QuadTree,r19_ne]}}
                    fsw ARG(Class:R3) (sw, p),  // expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree::InsertFunctor, IN:<empty>, ArgV:[p10_QuadTree,r21_sw]}}
                    fse ARG(Class:R4) (se, p);  // expected-warning{{Inferred region arguments: class asp::benchmark::QuadTree::InsertFunctor, IN:<empty>, ArgV:[p10_QuadTree,r23_se]}}
      tbb::parallel_invoke(fnw, fne, fsw, fse);
      return true;
    }
  };
};
};

using namespace std;
using namespace asp::benchmark;

//#define USE_LAMBDA

bool QuadTree::subDivide() {// expected-warning{{Inferred Effect Summary for subDivide: [reads(rpl([p10_QuadTree],[])),reads(rpl([p10_QuadTree,rGLOBAL],[])),writes(rpl([p10_QuadTree,rGLOBAL],[]))]}}
  if (ne != 0 || nw != 0 || sw != 0 || se != 0)
    return false;

  AABB *ne_aabb ARG(Local, Class) = new AABB(bounds.x + (bounds.w >> 1), bounds.y, bounds.w >> 1, bounds.h >> 1); // expected-warning{{Inferred region arguments: struct asp::benchmark::AABB *, IN:[p10_QuadTree], ArgV:[p10_QuadTree]}}
  ne = new QuadTree(*ne_aabb); // expected-warning{{Potential leak of memory pointed to by 'ne_aabb'}}
  AABB *se_aabb ARG(Local, Class) = new AABB(bounds.x + (bounds.w >> 1), bounds.y + (bounds.h >> 1), bounds.w >> 1, bounds.h >> 1); // expected-warning{{Inferred region arguments: struct asp::benchmark::AABB *, IN:[p10_QuadTree], ArgV:[p10_QuadTree]}}
  se = new QuadTree(*se_aabb); // expected-warning{{Potential leak of memory pointed to by 'se_aabb'}}
  AABB *nw_aabb ARG(Local, Class) = new AABB(bounds.x, bounds.y, bounds.w >> 1, bounds.h >> 1); // expected-warning{{Inferred region arguments: struct asp::benchmark::AABB *, IN:[p10_QuadTree], ArgV:[p10_QuadTree]}}
  nw = new QuadTree(*nw_aabb); // expected-warning{{Potential leak of memory pointed to by 'nw_aabb'}}
  AABB *sw_aabb ARG(Local, Class) = new AABB(bounds.x, bounds.y + (bounds.h >> 1), bounds.w >> 1, bounds.h >> 1); // expected-warning{{Inferred region arguments: struct asp::benchmark::AABB *, IN:[p10_QuadTree], ArgV:[p10_QuadTree]}}
  sw = new QuadTree(*sw_aabb); // expected-warning{{Potential leak of memory pointed to by 'sw_aabb'}}
  return true;
}

