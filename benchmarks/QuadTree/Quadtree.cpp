#include "QuadTree.h"
#include <cstdlib>
#include <array>
// Use our headers instead
//#include <tbb/tbb.h>
#include "asap.h"
#include "../include/tbb.h"

using namespace std;
using namespace asp::benchmark;

//#define USE_LAMBDA

AABB::AABB() : x(0), y(0), w(0), h(0) {
}

AABB::AABB(int x, int y, int w, int h) : x(x), y(y), w(w), h(h) {
}

bool AABB::contains(Point &p ) {
  return (p.x >= x && p.y >= y && p.x < x + w && p.y < y + h);
}

QuadTree::QuadTree(AABB aabb ARG(Class))
: size(0), nw(0), ne(0), sw(0), se(0) {
  bounds = aabb;
}

bool QuadTree::subDivide() {
  if (ne != 0 || nw != 0 || sw != 0 || se != 0)
    return false;

  AABB *ne_aabb = new AABB(bounds.x + (bounds.w >> 1), bounds.y, bounds.w >> 1, bounds.h >> 1);
  ne = new QuadTree(*ne_aabb);
  AABB *se_aabb = new AABB(bounds.x + (bounds.w >> 1), bounds.y + (bounds.h >> 1), bounds.w >> 1, bounds.h >> 1);
  se = new QuadTree(*se_aabb);
  AABB *nw_aabb = new AABB(bounds.x, bounds.y, bounds.w >> 1, bounds.h >> 1);
  nw = new QuadTree(*nw_aabb);
  AABB *sw_aabb = new AABB(bounds.x, bounds.y + (bounds.h >> 1), bounds.w >> 1, bounds.h >> 1);
  sw = new QuadTree(*sw_aabb);
  return true;
}

bool QuadTree::insert(Point p) {
  if (!bounds.contains(p))
    return false;

  if (size < CAPACITY) {
    points[size++] = p;
    return true;
  }

  if (!nw)
    subDivide();

#ifdef NOT_USE_TBB
  if (nw->insert(p) || ne->insert(p) || sw->insert(p) || se->insert(p))
    return true;
  return false;
#elif defined(USE_LAMBDA)
  array<QuadTree*, 4> quadrants = {{ne, se, nw, sw}};
  tbb::parallel_for(0, 4, 1, [&quadrants, &p] (int index) {
    quadrants.at(index)->insert(p);
  });
  return true;
#else
  InsertFunctor fnw(nw,p), fne(ne,p), fsw(sw, p), fse(se, p);
  tbb::parallel_invoke(fnw, fne, fsw, fse);
  return true;
#endif
}
