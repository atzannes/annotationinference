namespace tbb {
    template<typename Func0, typename Func1, typename Func2, typename Func3>
    void parallel_invoke [[asap::param("P0,P1,P2,P3")]]
                         (const Func0 &F0 [[asap::arg("P0")]], const Func1 &F1 [[asap::arg("P1")]],
                          const Func2 &F2 [[asap::arg("P2")]], const Func3 &F3 [[asap::arg("P3")]]);
}

#include "asap.h"

namespace asp {
namespace benchmark {
  static const int MAXCOORD = 1000000000;

  struct PARAM(P) Point {
    int x ARG(P);
    int y ARG(P);
    Point() : x(0), y(0) {}
    PARAM(S) READS(S) Point (const Point &P ARG_(S)) : x(P.x), y(P.y) {} // expected-warning{{Inferred Effect Summary for Point: [reads(rpl([p9_S],[]))]}}
    Point &operator= PARAM(S2) ARG(P) WRITES(P) READS(S2) (const Point &P ARG_(S2)) noexcept { // expected-warning{{Inferred Effect Summary for operator=: [reads(rpl([p10_S2],[])),writes(rpl([p8_P],[]))]}}
      this->x = P.x;
      this->y = P.y;
      return *this;
    }

    Point(int x, int y) : x(x), y(y) {} // expected-warning{{Inferred Effect Summary for Point: [reads(rpl([rLOCAL],[]))]}}
  };

  struct PARAM(Q) AABB {
    int x ARG(Q),
        y ARG(Q),
        w ARG(Q),
        h ARG(Q);

    AABB() : x(0), y(0), w(0), h(0) {}
    AABB(int x, int y, int w, int h) : x(x), y(y), w(w), h(h) {} // expected-warning{{Inferred Effect Summary for AABB: [reads(rpl([rLOCAL],[]))]}}

    PARAM(U) READS(U) AABB (const AABB &AB ARG_(U)) : x(AB.x), y(AB.y), w(AB.w), h(AB.h) {} // expected-warning{{Inferred Effect Summary for AABB: [reads(rpl([p12_U],[]))]}}
    AABB &operator= PARAM(V) ARG(Q) WRITES(Q) READS(V) (const AABB &AB ARG_(V)) noexcept { // expected-warning{{Inferred Effect Summary for operator=: [reads(rpl([p13_V],[])),writes(rpl([p11_Q],[]))]}}
      this->x = AB.x;
      this->y = AB.y;
      this->w = AB.w;
      this->h = AB.h;
      return *this;
    }

    bool contains PARAM(T) READS(Q,T) (Point &p ARG_(T)) { // expected-warning{{Inferred Effect Summary for contains: [reads(rpl([p11_Q],[])),reads(rpl([p14_T],[]))]}} 
      return (p.x >= x && p.y >= y && p.x < x + w && p.y < y + h);
    }
  };

  class [[asap::param("Class"), asap::region("R1, R2, R3, R4")]] QuadTree {
    static const int CAPACITY = 4;
    Point points ARG(Class) [CAPACITY];
    AABB bounds ARG(Class);
    int size ARG(Class);

    QuadTree *nw ARG(Class, Class:R1);
    QuadTree *ne ARG(Class, Class:R2);
    QuadTree *sw ARG(Class, Class:R3);
    QuadTree *se ARG(Class, Class:R4);

    class PARAM(T) InsertFunctor {
      QuadTree *mNode ARG(Local, T);
      Point mPoint ARG(T);
    public:
      InsertFunctor (QuadTree *node ARG(T), Point p ARG(T)) : mNode(node), mPoint(p){} // expected-warning{{Inferred Effect Summary for InsertFunctor: [reads(rpl([rLOCAL],[]))]}}

      void operator() WRITES(T:*) READS(Global) () const { // expected-warning{{Inferred Effect Summary for operator(): [reads(rpl([rGLOBAL],[])),reads(rpl([rLOCAL],[])),writes(rpl([p16_T,rSTAR],[]))]}} 
        mNode->insert(mPoint);
      }
    };



  public:
    PARAM(P) READS(P) WRITES(Class) QuadTree(AABB aabb ARG_(P)) : size(0), nw(0), ne(0), sw(0), se(0) { // expected-warning{{Inferred Effect Summary for QuadTree: [reads(rpl([p17_P],[])),writes(rpl([p15_Class],[]))]}}
      bounds = aabb;
    }

    bool subDivide WRITES(Class) (); 

    bool insert WRITES(Class:*) READS(Global) (Point p ARG(Class)) { // expected-warning{{Inferred Effect Summary for insert: [reads(rpl([rGLOBAL],[])),reads(rpl([rLOCAL],[])),writes(rpl([p15_Class],[])),writes(rpl([p15_Class,r0_R1,rSTAR],[])),writes(rpl([p15_Class,r1_R2,rSTAR],[])),writes(rpl([p15_Class,r2_R3,rSTAR],[])),writes(rpl([p15_Class,r3_R4,rSTAR],[]))]}}
      if (!bounds.contains(p))
        return false;
      if (size < CAPACITY) {
        points[size++] = p;
        return true;
      }

      if (!nw)
        subDivide();

      InsertFunctor fnw ARG(Class:R1) (nw, p), 
                    fne ARG(Class:R2) (ne, p), 
                    fsw ARG(Class:R3) (sw, p), 
                    fse ARG(Class:R4) (se, p);
      tbb::parallel_invoke(fnw, fne, fsw, fse);
      return true;
    }
  };
};
};

using namespace std;
using namespace asp::benchmark;

//#define USE_LAMBDA

bool QuadTree::subDivide() { // expected-warning{{Inferred Effect Summary for subDivide: [reads(rpl([rLOCAL],[])),writes(rpl([p15_Class],[]))]}}
  if (ne != 0 || nw != 0 || sw != 0 || se != 0)
    return false;

  AABB *ne_aabb ARG(Local, Class) = new AABB(bounds.x + (bounds.w >> 1), bounds.y, bounds.w >> 1, bounds.h >> 1);
  ne = new QuadTree(*ne_aabb); // expected-warning{{Potential leak of memory pointed to by 'ne_aabb'}}
  AABB *se_aabb ARG(Local, Class) = new AABB(bounds.x + (bounds.w >> 1), bounds.y + (bounds.h >> 1), bounds.w >> 1, bounds.h >> 1);
  se = new QuadTree(*se_aabb); // expected-warning{{Potential leak of memory pointed to by 'se_aabb'}}
  AABB *nw_aabb ARG(Local, Class) = new AABB(bounds.x, bounds.y, bounds.w >> 1, bounds.h >> 1);
  nw = new QuadTree(*nw_aabb); // expected-warning{{Potential leak of memory pointed to by 'nw_aabb'}}
  AABB *sw_aabb ARG(Local, Class) = new AABB(bounds.x, bounds.y + (bounds.h >> 1), bounds.w >> 1, bounds.h >> 1);
  sw = new QuadTree(*sw_aabb); // expected-warning{{Potential leak of memory pointed to by 'sw_aabb'}}
  return true;
}

