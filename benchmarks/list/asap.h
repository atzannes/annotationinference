#line 2 "asap.h"
#ifndef ASAP_H
#define ASAP_H

#ifndef ASAP_INFER_EFFECTS
#  define READS(...) [[asap::reads(#__VA_ARGS__)]]
#  define WRITES(...) [[asap::writes(#__VA_ARGS__)]]
#else
#  define READS(...)
#  define WRITES(...)
#endif

#ifndef ASAP_INFER_ARGS
#  define ARG(...) [[asap::arg(#__VA_ARGS__)]]
#  define REGION(...) [[asap::region(#__VA_ARGS__)]]
#else
#  define ARG(...)
#  define REGION(...)
#endif

#ifndef ASAP_CHECK_INFERRED
#  define IPARAM(...)
#  define IREADS(...)
#  define IWRITES(...)
#  define IARG(...)
#  define IREGION(...)
#  define PARAM(...) [[asap::param(#__VA_ARGS__)]]
#else
#  define PARAM(...)
#  define READS(...)
#  define WRITES(...)
#  define ARG(...)
#  define REGION(...)
#  define IPARAM(...) [[asap::param(#__VA_ARGS__)]]
#  define IREADS(...) [[asap::reads(#__VA_ARGS__)]]
#  define IWRITES(...) [[asap::writes(#__VA_ARGS__)]]
#  define IARG(...) [[asap::arg(#__VA_ARGS__)]]
#  define IREGION(...) [[asap::region(#__VA_ARGS__)]]
#endif

#endif
