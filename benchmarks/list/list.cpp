namespace tbb {
    template<typename Func0, typename Func1>
    void parallel_invoke [[asap::param("P0, P1")]]
                         (const Func0 &F0 [[asap::arg("P0")]], const Func1 &F1 [[asap::arg("P1")]]);

} // end namespace tbb

#include "asap.h"

IREGION(r0_n, r2_v, r6_n, r8_v, r13_next, r14_next);

class ListNode;

class PARAM(P_t) IPARAM(p4_P_t) SetThisFunctor {
  ListNode *n ARG(P_t, P_t) IARG(r0_n, p4_P_t);
  int v ARG(P_t) IARG(r2_v);
public:
  SetThisFunctor(ListNode *n ARG(P_t) IARG(p4_P_t), int v) : n(n), v(v) {} // expected-warning{{Inferred Effect Summary for SetThisFunctor: [reads(rpl([rLOCAL],[]))]}}

  void operator () READS(P_t) WRITES(P_t:ListNode::Value) IREADS(r0_n, r2_v) IWRITES(p4_P_t)
                   () const;
}; // end class SetThisFunctor

class PARAM(P_r) IPARAM(p5_P_r)
      SetRestFunctor {
  ListNode *n ARG(P_r, P_r) IARG(r6_n, p5_P_r);
  int v ARG(P_r) IARG(r8_v);
public:
  SetRestFunctor(ListNode *n ARG(P_r) IARG(p5_P_r), int v) : n(n), v(v) {} // expected-warning{{Inferred Effect Summary for SetRestFunctor: [reads(rpl([rLOCAL],[]))]}}
  void operator () READS(P_r, P_r:*:ListNode::Next, P_r:*:ListNode::Link) IREADS(r0_n, r13_next, r2_v, r6_n, r8_v, Local)
                   WRITES(P_r:ListNode::Next:*:ListNode::Value) IWRITES(p5_P_r:r14_next:*)
                   () const;
}; // end class SetRestFunctor

/////////////////////////////////////////////////////////////////////////////
class PARAM(P) IPARAM(p6_P) REGION(Link, Next, Value)
      ListNode {
  friend class SetThisFunctor;
  friend class SetRestFunctor;

  int value ARG(P:Value) IARG(p6_P);
  ListNode *next ARG(P:Link, P:Next) IARG(r13_next, p6_P:r14_next);
public:
  void setAllTo READS(P, P:*:Next, P:*:Link) IREADS(r0_n, r13_next, r2_v, r6_n, r8_v, Local)
                WRITES(P:*:Value) IWRITES(p6_P:*) (int x) { // expected-warning{{Inferred Effect Summary for setAllTo: [reads(rpl([p6_P,rSTAR],[])),reads(rpl([rLOCAL],[])),writes(rpl([p6_P,rSTAR,r2_Value],[]))]}}
    SetThisFunctor SetThis ARG(P) IARG(p6_P) (this, x);
    SetRestFunctor SetRest ARG(P) IARG(p6_P) (this, x);
    tbb::parallel_invoke(SetThis, SetRest);
  }
}; // end class List<region P>

void SetThisFunctor::operator () IREADS(r0_n, r2_v) IWRITES(p4_P_t) () const { // expected-warning{{Inferred Effect Summary for operator(): [reads(rpl([p4_P_t],[])),writes(rpl([p4_P_t,r2_Value],[]))]}}
  n->value = v;
}

void SetRestFunctor::operator () IREADS(r0_n, r13_next, r2_v, r6_n, r8_v, Local) IWRITES(p5_P_r:r14_next:*) () const { // expected-warning{{Inferred Effect Summary for operator(): [reads(rpl([p5_P_r],[])),reads(rpl([p5_P_r,r0_Link],[])),reads(rpl([p5_P_r,r1_Next,rSTAR],[])),reads(rpl([rLOCAL],[])),writes(rpl([p5_P_r,r1_Next,rSTAR,r2_Value],[]))]}}
  if (n->next)
    n->next->setAllTo(v);
}
