namespace tbb {
    template<typename Func0, typename Func1>
    void parallel_invoke [[asap::param("P0, P1")]]
                         (const Func0 &F0 [[asap::arg("P0")]], const Func1 &F1 [[asap::arg("P1")]]);

} // end namespace tbb

#include "asap.h"

IREGION(r0_n, r2_v, r6_n, r8_v, r13_next, r14_next);

class ListNode;

class IPARAM(p4_SetThisFunctor) SetThisFunctor { // expected-warning{{Inferred region arguments: class SetThisFunctor &&, IN:<empty>, ArgV:[p4_SetThisFunctor]}} expected-warning{{Inferred region arguments: const class SetThisFunctor &, IN:<empty>, ArgV:[p4_SetThisFunctor]}}
  ListNode *n; // expected-warning{{IN:[p4_SetThisFunctor], ArgV:[p4_SetThisFunctor]: Inferred region arguments}}
  int v; // expected-warning{{int, IN:[p4_SetThisFunctor], ArgV:: Inferred region arguments}}
public:
  SetThisFunctor(ListNode *n, int v) : n(n), v(v) {} // expected-warning{{class ListNode *, IN:Local, ArgV:[p4_SetThisFunctor]: Inferred region arguments}} expected-warning{{Inferred Effect Summary for SetThisFunctor: [reads(rpl([rLOCAL],[]))]}}

  void operator () // IREADS(r0_n, r2_v) IWRITES(p4_SetThisFunctor)
                   () const;
}; // end class SetThisFunctor

class IPARAM(p5_SetRestFunctor)
      SetRestFunctor { // expected-warning{{Inferred region arguments: class SetRestFunctor &&, IN:<empty>, ArgV:[p5_SetRestFunctor]}} expected-warning{{Inferred region arguments: const class SetRestFunctor &, IN:<empty>, ArgV:[p5_SetRestFunctor]}}
  ListNode *n; // expected-warning{{class ListNode *, IN:[p5_SetRestFunctor,r6_n], ArgV:[p5_SetRestFunctor]: Inferred region arguments}}
  int v; // expected-warning{{int, IN:[p5_SetRestFunctor,r8_v], ArgV:: Inferred region arguments}}
public:
  SetRestFunctor(ListNode *n, int v) : n(n), v(v) {} // expected-warning{{class ListNode *, IN:Local, ArgV:[p5_SetRestFunctor]: Inferred region arguments}} expected-warning{{Inferred Effect Summary for SetRestFunctor: [reads(rpl([rLOCAL],[]))]}}
  void operator () // IREADS(r0_n, r13_next, r2_v, r6_n, r8_v, Local)
                   // IWRITES(p5_SetRestFunctor:r14_next:*)
                   () const;
}; // end class SetRestFunctor

/////////////////////////////////////////////////////////////////////////////
class IPARAM(p6_ListNode) 
      ListNode {
  friend class SetThisFunctor;
  friend class SetRestFunctor;

  int value; // expected-warning{{int, IN:[p6_ListNode], ArgV:: Inferred region arguments}}
  ListNode *next; // expected-warning{{class ListNode *, IN:[p6_ListNode,r13_next], ArgV:[p6_ListNode,r14_next]: Inferred region arguments}}
public:
  void setAllTo READS(P, P:*:Next, P:*:Link) IREADS(r0_n, r13_next, r2_v, r6_n, r8_v, Local)
                WRITES(P:*:Value) IWRITES(p6_ListNode:*) (int x) { // expected-warning{{Inferred Effect Summary for setAllTo: [reads(rpl([rLOCAL],[])),writes(rpl([p6_ListNode,rSTAR],[]))]}}
    SetThisFunctor SetThis IARG(p6_ListNode) (this, x); // expected-warning{{Inferred region arguments: class SetThisFunctor, IN:<empty>, ArgV:[p6_ListNode]}}
    SetRestFunctor SetRest ARG(P) IARG(p6_ListNode) (this, x); // expected-warning{{Inferred region arguments: class SetRestFunctor, IN:<empty>, ArgV:[p6_ListNode]}}
    tbb::parallel_invoke(SetThis, SetRest); 
  }
}; // end class List<region P>

void SetThisFunctor::operator () () const { // expected-warning{{Inferred Effect Summary for operator(): [reads(rpl([p4_SetThisFunctor],[])),writes(rpl([p4_SetThisFunctor],[]))]}}
  n->value = v;
}

void SetRestFunctor::operator () () const { // expected-warning{{Inferred Effect Summary for operator(): [reads(rpl([p5_SetRestFunctor,r13_next],[])),reads(rpl([p5_SetRestFunctor,r6_n],[])),reads(rpl([p5_SetRestFunctor,r8_v],[])),reads(rpl([rLOCAL],[])),writes(rpl([p5_SetRestFunctor,r14_next,rSTAR],[]))]}}
  if (n->next)
    n->next->setAllTo(v);
}
