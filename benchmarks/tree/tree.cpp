// RUN: %clang_cc1 -std=c++11 -analyze -analyzer-checker=alpha.SafeParallelismChecker -analyzer-config -asap-default-scheme=param %s -verify

#define NULL 0
int atoi(char*);

int printf ( const char * format, ... );

namespace tbb {
template<typename Func0, typename Func1>
    void parallel_invoke [[asap::param("P0, P1")]]
                         (const Func0 &F0 [[asap::arg("P0")]], const Func1 &F1 [[asap::arg("P1")]]);
}
#include "asap.h"

#ifndef TIMES
#define TIMES 1
#endif

class TreeNode;
REGION(ReadOnly);

class PARAM(P_gtl) GrowTreeLeft {
  TreeNode *Node_gtl ARG(ReadOnly, P_gtl);
  int Depth_gtl ARG(ReadOnly);

public:
  GrowTreeLeft(TreeNode *n_gtl ARG(P_gtl), int depth) : Node_gtl(n_gtl), Depth_gtl(depth) {} // expected-warning{{Inferred Effect Summary for GrowTreeLeft: [reads(rpl([rLOCAL],[]))]}}

  void operator() READS(ReadOnly, P_gtl:*:TreeNode::V)
                  WRITES(P_gtl:TreeNode::L,
                         P_gtl:TreeNode::L:*:TreeNode::L,
                         P_gtl:TreeNode::L:*:TreeNode::R)
                  () const;

}; // end class GrowTreeLeft

class PARAM(P_gtr) GrowTreeRight {
  TreeNode *Node_gtr ARG(ReadOnly, P_gtr);
  int Depth_gtr ARG(ReadOnly);

public:
  GrowTreeRight(TreeNode *n_gtr ARG(P_gtr), int depth) : Node_gtr(n_gtr), Depth_gtr(depth) {} // expected-warning{{Inferred Effect Summary for GrowTreeRight: [reads(rpl([rLOCAL],[]))]}}

  void operator() READS(ReadOnly, P_gtr:*:TreeNode::V)
                  WRITES(P_gtr:TreeNode::R,
                         P_gtr:TreeNode::R:*:TreeNode::L,
                         P_gtr:TreeNode::R:*:TreeNode::R)
                  () const;

}; // end class GrowTreeRight

class PARAM(P) REGION(L, R, V, Links) TreeNode {
  friend class GrowTreeLeft;
  friend class GrowTreeRight;

  TreeNode *left ARG(P:L, P:L);  // left child
  TreeNode *right ARG(P:R, P:R); // right child
  int value ARG(P:V);

public:
  TreeNode(int v = 0) : left(NULL), right(NULL), value(v) {} // expected-warning{{Inferred Effect Summary for TreeNode: [reads(rpl([rLOCAL],[]))]}}

  void addLeftChild WRITES(P:L) 
                    (TreeNode *N_l ARG(P:L)) { // expected-warning{{Inferred Effect Summary for addLeftChild: [reads(rpl([rLOCAL],[])),writes(rpl([p6_P,r1_L],[]))]}}
    left = N_l;
  }

  void addRightChild WRITES(P:R)
                     (TreeNode *N_r ARG(P:R)) {// expected-warning{{Inferred Effect Summary for addRightChild: [reads(rpl([rLOCAL],[])),writes(rpl([p6_P,r2_R],[]))]}}
    right = N_r;
  }

  void growTree READS(P:*:V, ReadOnly) WRITES(P:*:L, P:*:R)
                (int depth) {// expected-warning{{Inferred Effect Summary for growTree: [reads(rpl([p6_P,rSTAR,r3_V],[])),reads(rpl([r0_ReadOnly],[])),reads(rpl([rLOCAL],[])),writes(rpl([p6_P,rSTAR,r1_L],[])),writes(rpl([p6_P,rSTAR,r2_R],[]))]}}
    if (depth<=0) return;
#ifdef SEQUENTIAL
    // INVARIANT: depth >= 1
    if (left==NULL) {
      left = new TreeNode(value+1);
      left->growTree(depth-1);
    }
    if (right==NULL) {
      int newValue = value + (1<<(depth));
      right = new TreeNode(newValue);
      right->growTree(depth-1);
    }
#else
    GrowTreeLeft Left ARG(P) (this, depth);
    GrowTreeRight Right ARG(P) (this, depth);
    tbb::parallel_invoke(Left, 
                         Right);
#endif      
  }

  void printTree READS(P:*) () { // expected-warning{{Inferred Effect Summary for printTree: [reads(rpl([p6_P,rSTAR,r1_L],[])),reads(rpl([p6_P,rSTAR,r2_R],[])),reads(rpl([p6_P,rSTAR,r3_V],[])),reads(rpl([rLOCAL],[]))]}}
    printf("%d, ", value);
    if (left)
      left->printTree();
    if (right)
      right->printTree();
  }

}; // end class TreeNode

void GrowTreeLeft::operator() () const { // expected-warning{{Inferred Effect Summary for operator(): [reads(rpl([p4_P_gtl,r1_L,rSTAR,r3_V],[])),reads(rpl([p4_P_gtl,r3_V],[])),reads(rpl([r0_ReadOnly],[])),reads(rpl([rLOCAL],[])),writes(rpl([p4_P_gtl,r1_L],[])),writes(rpl([p4_P_gtl,r1_L,rSTAR,r1_L],[])),writes(rpl([p4_P_gtl,r1_L,rSTAR,r2_R],[]))]}}
  if (Node_gtl->left==NULL) {
    Node_gtl->left = new TreeNode(Node_gtl->value+1);
    Node_gtl->left->growTree(Depth_gtl-1);
  }
}

void GrowTreeRight::operator() () const { // expected-warning{{Inferred Effect Summary for operator(): [reads(rpl([p5_P_gtr,r2_R,rSTAR,r3_V],[])),reads(rpl([p5_P_gtr,r3_V],[])),reads(rpl([r0_ReadOnly],[])),reads(rpl([rLOCAL],[])),writes(rpl([p5_P_gtr,r2_R],[])),writes(rpl([p5_P_gtr,r2_R,rSTAR,r1_L],[])),writes(rpl([p5_P_gtr,r2_R,rSTAR,r2_R],[]))]}}
  if (Node_gtr->right==NULL) {
    int newValue = Node_gtr->value + (1<<(Depth_gtr));
    Node_gtr->right = new TreeNode(newValue);
    Node_gtr->right->growTree(Depth_gtr-1);
  }
}

