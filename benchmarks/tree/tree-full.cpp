// RUN: %clang_cc1 -std=c++11 -analyze -analyzer-checker=alpha.SafeParallelismChecker -analyzer-config -asap-default-scheme=param %s -verify

#define NULL 0
int atoi(char*); // expected-warning{{Inferred region arguments: char *, IN:Local, ArgV:[r0_UnNamed]}}

int printf ( const char * format, ... ); // expected-warning{{Inferred region arguments: const char *, IN:Local, ArgV:[r1_format]}}

namespace tbb {
template<typename Func0, typename Func1>
    void parallel_invoke [[asap::param("P0, P1")]]
                         (const Func0 &F0 [[asap::arg("P0")]], const Func1 &F1 [[asap::arg("P1")]]);
}
#include "asap.h"

#ifndef TIMES
#define TIMES 1
#endif

class TreeNode;

class GrowTreeLeft { // expected-warning{{Inferred region arguments: class GrowTreeLeft &&, IN:<empty>, ArgV:[p4_GrowTreeLeft]}}  expected-warning{{Inferred region arguments: const class GrowTreeLeft &, IN:<empty>, ArgV:[p4_GrowTreeLeft]}}
  TreeNode *Node_gtl ARG(ReadOnly, P_gtl); // expected-warning{{Inferred region arguments: class TreeNode *, IN:[p4_GrowTreeLeft], ArgV:[p4_GrowTreeLeft]}}
  int Depth_gtl ARG(ReadOnly); // expected-warning{{Inferred region arguments: int, IN:[p4_GrowTreeLeft], ArgV:}}

public:
  GrowTreeLeft(TreeNode *n_gtl ARG(P_gtl), int depth) : Node_gtl(n_gtl), Depth_gtl(depth) {} // expected-warning{{Inferred region arguments: class TreeNode *, IN:Local, ArgV:[p4_GrowTreeLeft]}} // expected-warning{{Inferred Effect Summary for GrowTreeLeft: [reads(rpl([rLOCAL],[]))]}}

  void operator() READS(ReadOnly, P_gtl:*:TreeNode::V)
                  WRITES(P_gtl:TreeNode::L,
                         P_gtl:TreeNode::L:*:TreeNode::L,
                         P_gtl:TreeNode::L:*:TreeNode::R)
                  () const;

}; // end class GrowTreeLeft

class GrowTreeRight { // expected-warning{{Inferred region arguments: class GrowTreeRight &&, IN:<empty>, ArgV:[p5_GrowTreeRight]}} // expected-warning{{Inferred region arguments: const class GrowTreeRight &, IN:<empty>, ArgV:[p5_GrowTreeRight]}}
  TreeNode *Node_gtr ARG(ReadOnly, P_gtr); // expected-warning{{Inferred region arguments: class TreeNode *, IN:[p5_GrowTreeRight,r8_Node_gtr], ArgV:[p5_GrowTreeRight]}}
  int Depth_gtr ARG(ReadOnly); // expected-warning{{Inferred region arguments: int, IN:[p5_GrowTreeRight,r10_Depth_gtr], ArgV:}}

public:
  GrowTreeRight(TreeNode *n_gtr ARG(P_gtr), int depth) : Node_gtr(n_gtr), Depth_gtr(depth) {} // expected-warning{{Inferred Effect Summary for GrowTreeRight: [reads(rpl([rLOCAL],[]))]}} // expected-warning{{Inferred region arguments: class TreeNode *, IN:Local, ArgV:[p5_GrowTreeRight]}}

  void operator() READS(ReadOnly, P_gtr:*:TreeNode::V)
                  WRITES(P_gtr:TreeNode::R,
                         P_gtr:TreeNode::R:*:TreeNode::L,
                         P_gtr:TreeNode::R:*:TreeNode::R)
                  () const;

}; // end class GrowTreeRight

class TreeNode { // expected-warning{{Inferred region arguments: class TreeNode &&, IN:<empty>, ArgV:[p6_TreeNode]}} // expected-warning{{Inferred region arguments: const class TreeNode &, IN:<empty>, ArgV:[p6_TreeNode]}}
  friend class GrowTreeLeft;
  friend class GrowTreeRight;

  TreeNode *left ARG(P:L, P:L);  // left child // expected-warning{{Inferred region arguments: class TreeNode *, IN:[p6_TreeNode], ArgV:[p6_TreeNode,r15_left]}}
  TreeNode *right ARG(P:R, P:R); // right child // expected-warning{{Inferred region arguments: class TreeNode *, IN:[p6_TreeNode,r16_right], ArgV:[p6_TreeNode,r17_right]}}
  int value ARG(P:V); // expected-warning{{Inferred region arguments: int, IN:[p6_TreeNode,r18_value], ArgV:}}

public:
  TreeNode(int v = 0) : left(NULL), right(NULL), value(v) {} // expected-warning{{Inferred Effect Summary for TreeNode: [reads(rpl([rLOCAL],[]))]}}

  void addLeftChild WRITES(P:L) 
                    (TreeNode *N_l ARG(P:L)) { // expected-warning{{Inferred Effect Summary for addLeftChild: [reads(rpl([rLOCAL],[])),writes(rpl([p6_TreeNode],[]))]}} // expected-warning{{Inferred region arguments: class TreeNode *, IN:Local, ArgV:[p6_TreeNode,r15_left]}}
    left = N_l;
  }

  void addRightChild WRITES(P:R)
                     (TreeNode *N_r ARG(P:R)) {// expected-warning{{Inferred Effect Summary for addRightChild: [reads(rpl([rLOCAL],[])),writes(rpl([p6_TreeNode,r16_right],[]))]}} // expected-warning{{Inferred region arguments: class TreeNode *, IN:Local, ArgV:[p6_TreeNode,r17_right]}}
    right = N_r;
  }

  void growTree READS(P:*:V, ReadOnly) WRITES(P:*:L, P:*:R)
                (int depth) {// expected-warning{{Inferred Effect Summary for growTree: [reads(rpl([rLOCAL],[])),writes(rpl([p6_TreeNode,rSTAR],[]))]}}
    if (depth<=0) return;
#ifdef SEQUENTIAL
    // INVARIANT: depth >= 1
    if (left==NULL) {
      left = new TreeNode(value+1);
      left->growTree(depth-1);
    }
    if (right==NULL) {
      int newValue = value + (1<<(depth));
      right = new TreeNode(newValue);
      right->growTree(depth-1);
    }
#else
    GrowTreeLeft Left ARG(P) (this, depth); // expected-warning{{Inferred region arguments: class GrowTreeLeft, IN:<empty>, ArgV:[p6_TreeNode]}}
    GrowTreeRight Right ARG(P) (this, depth); // expected-warning{{Inferred region arguments: class GrowTreeRight, IN:<empty>, ArgV:[p6_TreeNode]}}
    tbb::parallel_invoke(Left, 
                         Right);
#endif      
  }

  void printTree READS(P:*) () { // expected-warning{{Inferred Effect Summary for printTree: [reads(rpl([p6_TreeNode,rSTAR],[])),reads(rpl([rLOCAL],[]))]}}
    printf("%d, ", value);
    if (left)
      left->printTree();
    if (right)
      right->printTree();
  }

}; // end class TreeNode

void GrowTreeLeft::operator() () const { // expected-warning{{Inferred Effect Summary for operator(): [reads(rpl([p4_GrowTreeLeft,r18_value],[])),reads(rpl([rLOCAL],[])),writes(rpl([p4_GrowTreeLeft],[])),writes(rpl([p4_GrowTreeLeft,r15_left,rSTAR],[]))]}}
  if (Node_gtl->left==NULL) {
    Node_gtl->left = new TreeNode(Node_gtl->value+1);
    Node_gtl->left->growTree(Depth_gtl-1);
  }
}

void GrowTreeRight::operator() () const { // expected-warning{{Inferred Effect Summary for operator(): [reads(rpl([p5_GrowTreeRight,r10_Depth_gtr],[])),reads(rpl([p5_GrowTreeRight,r18_value],[])),reads(rpl([p5_GrowTreeRight,r8_Node_gtr],[])),reads(rpl([rLOCAL],[])),writes(rpl([p5_GrowTreeRight,r16_right],[])),writes(rpl([p5_GrowTreeRight,r17_right,rSTAR],[]))]}}
  if (Node_gtr->right==NULL) {
    int newValue = Node_gtr->value + (1<<(Depth_gtr));
    Node_gtr->right = new TreeNode(newValue);
    Node_gtr->right->growTree(Depth_gtr-1);
  }
}

