namespace tbb {
    template<typename Func0, typename Func1>
    void parallel_invoke(const Func0 &F0, const Func1 &F1);

    template<typename Func0, typename Func1, typename Func2>
    void parallel_invoke(const Func0 &F0, const Func1 &F1, const Func2 &F2);

    template<typename Func0, typename Func1, typename Func2, typename Func3>
    void parallel_invoke [[asap::param("P1, P2, P3, P4")]]
                         (const Func0 &F0 [[asap::arg("P1")]], const Func1 &F1 [[asap::arg("P2")]], 
                          const Func2 &F2 [[asap::arg("P3")]], const Func3 &F3 [[asap::arg("P4")]]);

    template<typename Func0, typename Func1, typename Func2, typename Func3, typename Func4>
    void parallel_invoke(const Func0 &F0, const Func1 &F1, const Func2 &F2, const Func3 &F3, const Func4 &F4);

    template<typename Func0, typename Func1, typename Func2, typename Func3, 
             typename Func4, typename Func5>
    void parallel_invoke(const Func0 &F0, const Func1 &F1, const Func2 &F2, const Func3 &F3, 
                         const Func4 &F4, const Func5 &F5);

    template<typename Func0, typename Func1, typename Func2, typename Func3, 
             typename Func4, typename Func5, typename Func6>
    void parallel_invoke(const Func0 &F0, const Func1 &F1, const Func2 &F2, const Func3 &F3, 
                         const Func4 &F4, const Func5 &F5, const Func6 &F6);

    template<typename Func0, typename Func1, typename Func2, typename Func3, 
             typename Func4, typename Func5, typename Func6, typename Func7>
    void parallel_invoke(const Func0 &F0, const Func1 &F1, const Func2 &F2, const Func3 &F3, 
                         const Func4 &F4, const Func5 &F5, const Func6 &F6, const Func7 &F7);

} // end namespace

