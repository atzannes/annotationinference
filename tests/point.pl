% class Point {
%     int<Rx> x;
%     int<Ry> y;
%     void setX(int _x) { x = _x; }
%     void setY(int _y) { y = _y; }
%     void setXY(int _x, int _y)
% };

rgn_name(rx).
rgn_name(ry).
effect_var(ev1).
effect_var(ev2).
effect_var(ev3).



%% esi(+EsiID:id, +Method:id, +LHS:effect_l, +RHS:effect_l)
%
esi_constraint(esi1, point_setX_int_void, [writes(rpl([rx],[]))], effect_var(ev1)).
esi_constraint(esi2, point_setY_int_void, [writes(rpl([ry],[]))], effect_var(ev2)).
esi_constraint(esi3, point_setXY_int_int_void, [invokes(point_setX_int_void,[]),
                                                invokes(point_setY_int_void,[])], effect_var(ev3)).

solve(ES1, ES2, ES3) :-
    esi_constraint(esi1, Name1, LHS1, effect_var(EV1)),
    esi_collect(EV1, [Name1], LHS1, ES1),
    assertz(asap:has_value(EV1,ES1)),
    esi_constraint(esi2, Name2, LHS2, effect_var(EV2)),
    esi_collect(EV2, [Name2], LHS2, ES2),
    assertz(asap:has_value(EV2,ES2)),
    esi_constraint(esi3, Name3, LHS3, effect_var(EV3)),
    esi_collect(EV3, [Name3], LHS3, ES3),
    assertz(asap:has_value(EV3,ES3)),!.

solve3(ES3) :-
    esi_constraint(esi3, Name3, LHS3, effect_var(EV3)),
    esi_collect(EV3, Name3, LHS3, ES3),
    assertz(asap:has_value(EV3,ES3)),!.

