% class Point<region P> {
%     int<P:Rx> x;
%     int<P:Ry> y;
%     void setX(int _x) { x = _x; }
%     void setY(int _y) { y = _y; }
%     void setXY(int _x, int _y)
% };
%
% void foo() {
%     Point<Rfoo> *p1 = new Point();
%     p1->setX(4);
% }

rgn_name(rx).
rgn_name(ry).
rgn_name(r_foo).
rgn_param(p).
effect_var(ev1).
effect_var(ev2).
effect_var(ev3).
effect_var(ev4).

% has_value(ev1, effect_summary)
:- dynamic has_value/2.

%% esi(+EsiID:id, +Method:id, +LHS:effect_l, +RHS:effect_l)
%
esi_constraint(esi1, point_setX_int_void, [writes(rpl([p,rx],[]))], effect_var(ev1)).
esi_constraint(esi2, point_setY_int_void, [writes(rpl([p,ry],[]))], effect_var(ev2)).
esi_constraint(esi3, point_setXY_int_int_void, [invokes(point_setX_int_void,[]),
                                                invokes(point_setY_int_void,[])], effect_var(ev3)).
esi_constraint(esi4, foo_void, [invokes(point_setX_int_void, [param_sub(p,[r_foo])])], effect_var(ev4)).

%effect_summary(point_setX_int_void, effect_var(ev1)).
effect_summary(none,[]).

solve(RHS1, ES1, ES2, ES3,ES4) :-
    esi_constraint(esi1, Name1, LHS1, effect_var(EV1)),
    RHS1 = effect_var(EV1),
    esi_collect(EV1, Name1, LHS1, ES1),
    assertz(has_value(EV1,ES1)),
    esi_constraint(esi2, Name2, LHS2, effect_var(EV2)),
    esi_collect(EV2, Name2, LHS2, ES2),
    assertz(has_value(EV2,ES2)),
    esi_constraint(esi3, Name3, LHS3, effect_var(EV3)),
    esi_collect(EV3, Name3, LHS3, ES3),
    assertz(has_value(EV3,ES3)),
    esi_constraint(esi4, Name4, LHS4, effect_var(EV4)),
    esi_collect(EV4, Name4, LHS4, ES4),
    assertz(has_value(EV4,ES4)).



