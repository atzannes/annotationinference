% class FunctorIncr<region P1> {
%  int<P1:V> &value;
%  void operator() () writes P1:V { value++; }
% };
%
% class FunctorRecurse<region P2> {
%  ListNode<P2:Next> *next in P2:Link;
%  void operator() () reads P2:*:Link, writes P2:Next:*:V {
%    if (next) next->incrAll();
%  }
% };
%
% class ListNode<region P> {
%  int<P:V> value;
%  ListNode<P:Next> *next in P:Link;
%  void incrAll() reads P:*:Link, writes P:*:V {
%    FunctorIncr<P> FI(value);
%    FunctorRecurse<P> FR(next);
%    cobegin {
%      FI();
%      FR();
%    }
%  }
% }; // end class ListNode

rgn_name(rV).
rgn_name(rNext).
rgn_name(rLink).

rgn_param(p1).
rgn_param(p2).
rgn_param(p).

effect_var(ev1).

has_effect_summary(none,effect_summary([],[])).

%effect_summary(operator_paren_void_void_functor_incr,
%               effect_summary([writes(rpl([p1,rV],[]))],[])).
%effect_summary(operator_paren_void_void_functor_recurse,
%               effect_summary([reads(rpl([p2,star,rLink],[])),
%                               writes(rpl([p2,rNext,star,rV],[]))], []).
esi_constraint(esi2, operator_paren_void_void_functor_incr, [writes(rpl([p1,rV],[]))], effect_var(ev2)).
esi_constraint(esi3, operator_paren_void_void_functor_recurse,
               [reads(rpl([p2,rLink],[])),
                invokes(incrAll_void_void,[param_sub(p,[p2,rNext])])], effect_var(ev3)).
esi_constraint(esi1, incrAll_void_void,
               [invokes(operator_paren_void_void_functor_incr,   [param_sub(p1,[p])]),
                invokes(operator_paren_void_void_functor_recurse,[param_sub(p2,[p])])], effect_var(ev1)).

solve(ES1) :-
    esi_constraint(esi1, Name1, LHS1, effect_var(EV1)),
    esi_collect(EV1, Name1, LHS1, ES1).
