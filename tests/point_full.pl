% class Point {
%     int<Rx> x;
%     int<Ry> y;
%     void setX(int _x) { x = _x; }
%     void setY(int _y) { y = _y; }
%     void setXY(int _x, int _y) {
%        setX(_x) || setY(_y);
%     }
% };

rgn_name(rx).
rgn_name(ry).

rpl_domain(dom1, [p], [rx], null_dom).
rpl_domain(dom2, [p], [ry, rx], null_dom).

head_rpl_var(rv1, dom1).
head_rpl_var(rv2, dom2).

effect_var(ev1).
effect_var(ev2).
effect_var(ev3).


has_effect_summary(point_setX_int_void, effect_var(ev1)).
has_effect_summary(point_setY_int_void, effect_var(ev2)).
has_effect_summary(point_setXY_int_int_void, effect_var(ev3)).

%% esi(+EsiID:id, +Method:id, +LHS:effect_l, +RHS:effect_l)
%
esi_constraint(esi1, point_setX_int_void, [writes(rpl([rv1],[]))], effect_var(ev1)).
esi_constraint(esi2, point_setY_int_void, [writes(rpl([rv2],[]))], effect_var(ev2)).
esi_constraint(esi3, point_setXY_int_int_void, [invokes(point_setX_int_void,[]),
                                                invokes(point_setY_int_void,[])], effect_var(ev3)).

eni_constraint(eni1, [invokes(point_setX_int_void, [])],
                     [invokes(point_setY_int_void, [])]).

