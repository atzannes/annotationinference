% class ListNode<region P> {
%  int<P:V> value;
%  ListNode<P:Next> *next in P:Link;
%  void incrAll() reads P:*:Link, writes P:*:V {
%    cobegin {
%      value++;
%      if (next) next->incrAll();
%    }
%  }
% }; // end class ListNode

rgn_name(rV).
rgn_name(rNext).
rgn_name(rLink).

rgn_param(p).

effect_var(ev1).


esi_constraint(esi1, incrAll_void_void,
               [reads(rpl([p,rLink],[])),
                writes(rpl([p,rV],[])),
                invokes(incrAll_void_void,[param_sub(p,[p,rNext])])], effect_var(ev1)).


solve(ES1) :-
    esi_constraint(esi1, Name1, LHS1, effect_var(EV1)),
    esi_collect(EV1, Name1, LHS1, ES1),
    assertz(asap:has_value(EV1,ES1)).

