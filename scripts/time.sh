#!/bin/bash
subjects=(QuadTree list)
table=table.tex
values=values.csv

TIMEFORMAT='%R'

dir=`pwd`
cd ../benchmarks

echo -e "Program,Time(s)" > $dir/$values

echo "\begin{table}[h]" > $dir/$table
echo "\begin{tabular}{l|l}">> $dir/$table
echo "Program & Time(s) \\\\" >> $dir/$table
echo "\hline" >> $dir/$table
for s in ${subjects[*]}
do
    cd $s
    seconds=$({ time make inferfull > /dev/null 2>&1; } 2>&1)
    echo -e "${s},${seconds}" >> $dir/$values
    echo "${s} & ${seconds} \\\\" >> $dir/$table
    cd ..
done
echo "\end{tabular}">> $dir/$table
echo "\end{table}" >> $dir/$table