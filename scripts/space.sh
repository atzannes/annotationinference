#!/bin/bash
subjects=(list tree QuadTree kmeans mergesort CollisionTree)
table=space.tex

dir=`pwd`
cd ../benchmarks

echo "\begin{table}[h]" > $dir/$table
echo "\begin{tabular}{l|l|l|l}">> $dir/$table
echo "Program & Space & After Thm1 & After Thm2 \\\\" >> $dir/$table
echo "\hline" >> $dir/$table
for s in ${subjects[*]}
do
    cd $s
    ( cmdpid=$BASHPID; (sleep 5; kill $cmdpid) & exec make inferfull 2&>1 > output )
    original=`cat output | grep "After RPL Inclusion Constraint Simplification" | cut -f2 -d'='`
    original=`printf "%1.2e" $original | sed 's/e+/\\\cdot10^{/'`
    original=`echo \$ "${original}}\$"`

    th1=`cat output | grep "After Theorem 1 Simplification" | cut -f2 -d'='`
    th1=`printf "%1.2e" $th1  | sed 's/e+/\\\cdot10^{/'`
    th1=`echo \$ "${th1}}\$"`

    th2=`cat output | grep "After Theorem 2 Simplification" | cut -f2 -d'='`
    th2=`printf "%1.2e" $th2 | sed 's/e+/\\\cdot10^{/'`
    th2=`echo \$ "${th2}}\$"`

    echo "${s} & ${original} & ${th1} & ${th2} \\\\" >> $dir/$table
    cd ..
done
echo "\end{tabular}">> $dir/$table
echo "\caption{Solution Space for benchmarks} " >> $dir/$table
echo "\label{table:space} " >> $dir/$table
echo "\end{table}" >> $dir/$table